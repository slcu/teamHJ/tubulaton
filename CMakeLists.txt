cmake_minimum_required(VERSION 2.8)

project(microtubule_simulations)

if(CMAKE_COMPILER_IS_GNUCXX)
  set(CMAKE_CXX_FLAGS "-Wall")
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS}")
  set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS}")
endif()

set(CMAKE_CXX_STANDARD 11) 
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR})
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib/)
include_directories(${PROJECT_SOURCE_DIR}/src/)

set(CMAKE_BUILD_TYPE "Release" CACHE STRING "build type")
option(BUILD_SHARED_LIBS "Build the libraries shared." ON)

set(VTK_MIN_VERSION "6.0")
find_package(VTK REQUIRED NO_MODULE)
include(${VTK_USE_FILE})

if(${VTK_FOUND})
	if(${VTK_VERSION} VERSION_GREATER "7.1")
		add_definitions(-DVTK_VERSION_7_1PLUS)
	endif()
endif()
#get_cmake_property(_variableNames VARIABLES)
#foreach (_variableName ${_variableNames})
#    message(STATUS "${_variableName}=${${_variableName}}")
#endforeach()

add_subdirectory(src)

#set(Boost_USE_STATIC_LIBS OFF) 
#set(Boost_USE_MULTITHREADED ON)  
#set(Boost_USE_STATIC_RUNTIME OFF) 
find_package(Boost REQUIRED COMPONENTS system filesystem serialization) 
#include_directories(${BOOST_INCLUDE_DIRS}) 
include_directories(${Boost_INCLUDE_DIRS})
link_directories(${Boost_LIBRARY_DIRS})
