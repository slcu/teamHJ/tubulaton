#!/usr/bin/python
# -*- coding: utf-8 -*- 
# NOTE: This code is written in Python 3. 
import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
from scipy import stats
import math
from mayavi import mlab
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from math import log10, floor

# function for setting the colors of the box plots pairs
def setBoxColors(bp,num):
    #Set colours of the boxplots
    plt.setp(bp['boxes'][0], color='blue')
    plt.setp(bp['caps'][0], color='blue')
    plt.setp(bp['caps'][1], color='blue')
    plt.setp(bp['whiskers'][0], color='blue')
    plt.setp(bp['whiskers'][1], color='blue')
    plt.setp(bp['fliers'][0], color='blue')
    plt.setp(bp['medians'][0], color='blue')
    
    if num>=2:
        plt.setp(bp['boxes'][1], color='red')
        plt.setp(bp['caps'][2], color='red')
        plt.setp(bp['caps'][3], color='red')
        plt.setp(bp['whiskers'][2], color='red')
        plt.setp(bp['whiskers'][3], color='red')
        plt.setp(bp['fliers'][1], color='red')
        plt.setp(bp['medians'][1], color='red')
    
    if num>=3:
        plt.setp(bp['boxes'][2], color='green')
        plt.setp(bp['caps'][3], color='green')
        plt.setp(bp['caps'][4], color='green')
        plt.setp(bp['whiskers'][4], color='green')
        plt.setp(bp['whiskers'][5], color='green')
        plt.setp(bp['fliers'][2], color='green')
        plt.setp(bp['medians'][2], color='green')

    if num>4:
        plt.setp(bp['boxes'][3], color='yellow')
        plt.setp(bp['caps'][4], color='yellow')
        plt.setp(bp['caps'][5], color='yellow')
        plt.setp(bp['whiskers'][6], color='yellow')
        plt.setp(bp['whiskers'][7], color='yellow')
        plt.setp(bp['fliers'][3], color='yellow')
        plt.setp(bp['medians'][3], color='yellow')

def main():
    """Four subplots plotting changes in anisotropy as one parameter is varied. For each graph, the control is manually set and the t-test done to determine if the mean is statistically different from this control; p-values are then output to the terminal.
    """
    #Read in variables
    plt.figure(1)
    ax = plt.axes()
    
    FileLocation='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/CuttingMultiRepetions_2021101214591634047191.txt'
    f=open(FileLocation,"r")
    lines=f.readlines()

    Nuc=[0.000001,0.00001,0.0001,0.001,0.01]
    Reps=10
    
    val_aniso=[[] for i in Nuc]
    val_direc=[[] for i in Nuc]
    
    Count=0
    NucIndex=0
    for x in lines:
        Count=Count+1
        x=x.split(";")
        val_aniso[NucIndex].append(float(x[0]))
        val_direc[NucIndex].append(float(x[1]))
        
        if Count==10:
            Count=0
            NucIndex=NucIndex+1

    for j in range(1,len(Nuc)+1):
        for k in range(Reps):
            plt.plot(j,val_aniso[j-1][k],'+k', zorder=2)
            
    bp=plt.boxplot(val_aniso, patch_artist=True, zorder=1)
    plt.ylabel(r'$S_2$')
    plt.xlabel('Cortical Nucleation Rate')
    ax.set_xticklabels(Nuc) 
    for box in bp['boxes']:
        box.set(color='black')
        box.set(facecolor='white')
    for item in ['whiskers', 'fliers', 'medians', 'caps']:
        for j in bp[item]:
            j.set(color='black')
            j.set(markeredgecolor='black')
    for item in ['whiskers', 'fliers', 'medians', 'caps']:    
        if item=='whiskers' or item=='caps':
            box=bp[item]
        elif item=='fliers' :
            box=bp[item]
        else :
            box=bp[item]
    plt.show()

    bp=plt.boxplot(val_direc, patch_artist=True, zorder=1)
    plt.ylabel('Angle Alignment')
    plt.xlabel('Cortical Nucleation Rate')
    ax.set_xticklabels(Nuc) 
    for box in bp['boxes']:
        box.set(color='black')
        box.set(facecolor='white')
    for item in ['whiskers', 'fliers', 'medians', 'caps']:
        for j in bp[item]:
            j.set(color='black')
            j.set(markeredgecolor='black')
    for item in ['whiskers', 'fliers', 'medians', 'caps']:    
        if item=='whiskers' or item=='caps':
            box=bp[item]
        elif item=='fliers' :
            box=bp[item]
        else :
            box=bp[item]
    for j in range(1,len(Nuc)+1):
        for k in range(Reps):
            plt.plot(j,val_direc[j-1][k],'+k', zorder=2)
            
    plt.show()
       
if __name__ == '__main__':
    main()
