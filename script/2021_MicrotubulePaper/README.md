
List of files in this directory and what they are used to generate:

BaseRun_Cylinder -- Supplementary Fig. 1 Cortical nucleation only with increasing cylinder size

BaseRun_Cylinder_NucleusNuc -- Supplementary Fig. 1 Nucleus nucleating microtubules only with increasing cylinder size

BaseRun_NucRateCortex -- Supplementary Fig. Cortical nucleation with increaseing nucleation rate. 
