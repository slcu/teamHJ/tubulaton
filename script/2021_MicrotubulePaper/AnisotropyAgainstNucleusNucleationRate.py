import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
from scipy import stats
import math
from mayavi import mlab
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from math import log10, floor

# function for setting the colors of the box plots pairs
def setBoxColors(bp,num):
    #Set colours of the boxplots
    plt.setp(bp['boxes'][0], color='blue')
    plt.setp(bp['caps'][0], color='blue')
    plt.setp(bp['caps'][1], color='blue')
    plt.setp(bp['whiskers'][0], color='blue')
    plt.setp(bp['whiskers'][1], color='blue')
    plt.setp(bp['fliers'][0], color='blue')
    plt.setp(bp['medians'][0], color='blue')
    
    if num>=2:
        plt.setp(bp['boxes'][1], color='red')
        plt.setp(bp['caps'][2], color='red')
        plt.setp(bp['caps'][3], color='red')
        plt.setp(bp['whiskers'][2], color='red')
        plt.setp(bp['whiskers'][3], color='red')
        plt.setp(bp['fliers'][1], color='red')
        plt.setp(bp['medians'][1], color='red')
    
    if num>=3:
        plt.setp(bp['boxes'][2], color='green')
        plt.setp(bp['caps'][3], color='green')
        plt.setp(bp['caps'][4], color='green')
        plt.setp(bp['whiskers'][4], color='green')
        plt.setp(bp['whiskers'][5], color='green')
        plt.setp(bp['fliers'][2], color='green')
        plt.setp(bp['medians'][2], color='green')

    if num>4:
        plt.setp(bp['boxes'][3], color='yellow')
        plt.setp(bp['caps'][4], color='yellow')
        plt.setp(bp['caps'][5], color='yellow')
        plt.setp(bp['whiskers'][6], color='yellow')
        plt.setp(bp['whiskers'][7], color='yellow')
        plt.setp(bp['fliers'][3], color='yellow')
        plt.setp(bp['medians'][3], color='yellow')

def main():
    """Four subplots plotting changes in anisotropy as one parameter is varied. For each graph, the control is manually set and the t-test done to determine if the mean is statistically different from this control; p-values are then output to the terminal.
    """
    #Read in variables
    plt.figure(1)
    ax = plt.axes()
    
    FileLocation='/home2/tamsin/Active/Tubulaton_Output/AnisotropyTextFiles/ReducedArea_2021110811461636371967.txt'
    #FileLocation='/home2/tamsin/Active/Tubulaton_Output/AnisotropyTextFiles/2021110512051636113910.txt'
    f=open(FileLocation,"r")
    lines=f.readlines()

    Nuc=[0.0,0.000001,0.00001,0.0001,0.001,0.01]
    Direction=[0,1]
    Reps=10

    val_aniso0=[[] for i in Nuc]
    val_direc0=[[] for i in Nuc]    
    val_aniso1=[[] for i in Nuc]
    val_direc1=[[] for i in Nuc]
    
    Count=0
    NucIndex=0
    for x in lines:
        Count=Count+1
        x=x.split(";")
        
        Nam=x[6]
        Nam=Nam.split("_")
        if Nam[3]!='Direction':
            NucOne="0."+Nam[3]
            Direc=Nam[5]
        else:
            NucOne="0."+Nam[2]
            Direc=Nam[4]
        NucOne=float(NucOne)
        NucIndex=Nuc.index(NucOne)
        if float(Direc)==0:        
            val_aniso0[NucIndex].append(float(x[0]))
            val_direc0[NucIndex].append(float(x[1]))
        else:
            val_aniso1[NucIndex].append(float(x[0]))
            val_direc1[NucIndex].append(float(x[1]))
    
    print(val_aniso0)
    print(val_aniso1)

    bars0=[1,5,9,13,17,21]
    bars1=[2,6,10,14,18,22]
    bars_xticks=[1.5,5.5,9.5,13.5,17.5,21.5]
    for j in range(1,len(Nuc)+1):
        for k in range(len(val_aniso0[j-1])):
            plt.plot(bars0[j-1],val_aniso0[j-1][k],'+k', zorder=2)
    for j in range(1,len(Nuc)+1):
        for k in range(len(val_aniso1[j-1])):
            plt.plot(bars1[j-1],val_aniso1[j-1][k],'+b', zorder=2)
    bp=plt.boxplot(val_aniso0,positions=bars0, patch_artist=True, zorder=1)
    bp1=plt.boxplot(val_aniso1,positions=bars1, patch_artist=True, zorder=1)
    plt.ylabel(r'$S_2$')
    plt.xlabel('Cortical Nucleation Rate')
    ax.set_xticks(bars_xticks)
    ax.set_xticklabels(Nuc) 
    ax.set_ylim([0,1])
    for box in bp['boxes']:
        box.set(color='black')
        box.set(facecolor='white')
    for box in bp1['boxes']:
        box.set(color='blue')
        box.set(facecolor='white')
    for item in ['whiskers', 'fliers', 'medians', 'caps']:
        for j in bp[item]:
            j.set(color='black')
            j.set(markeredgecolor='black')
        for j in bp1[item]:
            j.set(color='blue')
            j.set(markeredgecolor='blue')
    #for item in ['whiskers', 'fliers', 'medians', 'caps']:    
    #    if item=='whiskers' or item=='caps':
    #        box=bp[item]
    #        box=bp1[item]
    #    elif item=='fliers' :
    #        box=bp[item]
    #        box=bp1[item]
    #    else :
    #        box=bp[item]
    #        box=bp1[item]
    plt.show()




    bp=plt.boxplot(val_direc0,positions=bars0, patch_artist=True, zorder=1)
    bp1=plt.boxplot(val_direc1,positions=bars1, patch_artist=True, zorder=1)
    plt.ylabel('Angle Alignment')
    plt.xlabel('Cortical Nucleation Rate')
    for box in bp['boxes']:
        box.set(color='black')
        box.set(facecolor='white')
    for box in bp1['boxes']:
        box.set(color='blue')
        box.set(facecolor='white')
    for item in ['whiskers', 'fliers', 'medians', 'caps']:
        for j in bp[item]:
            j.set(color='black')
            j.set(markeredgecolor='black')
        for j in bp1[item]:
            j.set(color='blue')
            j.set(markeredgecolor='blue')
    for j in range(1,len(Nuc)+1):
        for k in range(len(val_direc0[j-1])):
            plt.plot(bars0[j-1],val_direc0[j-1][k],'+k', zorder=2)
    for j in range(1,len(Nuc)+1):
        for k in range(len(val_direc1[j-1])):
            plt.plot(bars1[j-1],val_direc1[j-1][k],'+b', zorder=2)        
    ax = plt.axes()
    ax.set_xticks(bars_xticks)
    ax.set_xticklabels(Nuc)     
    ax.set_ylim([-math.pi,math.pi])
    plt.show()
       
if __name__ == '__main__':
    main()
