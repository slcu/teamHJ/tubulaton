#!/usr/bin/python
# -*- coding: utf-8 -*-
import vtk
import math
import numpy as np
from tvtk.api import tvtk
import matplotlib.pyplot as plt
import os, sys
from scipy.stats import norm  
from scipy import stats
from os.path import exists
  
  
def lecture(rep,nom):
    """Read in data from the .vtk file output from the microtubule simulations. Copied from CalculateAnisotropy.
       Inputs: nom is the name of the file. rep is the file location, if none provided assume in current directory. 
    Output: p the read in data   """
    r=tvtk.PolyDataReader()
    r.file_name=rep+nom
    #print (r.file_name)
    r.update()
    p=r.output
    return p

def extraction(p,verbose=0):
    """Copied from CalculateAnisotropy
	This function extracts data from p. It creates a numpy array with the following data in each position:
    0, 1, 2, 3 , 4,  5,  6,        7
    X, Y, Z, VX, VY, VZ, identite, type
    
    c'est ici qu'il faut que je rajoute la coque si elle n'y est pas spontanément.
    """
    x=np.array([X[0] for X in p.points])
    y=np.array([X[1] for X in p.points])
    z=np.array([X[2] for X in p.points])
    vx=np.array([X[0] for X in p.point_data.get_array(0)])
    vy=np.array([X[1] for X in p.point_data.get_array(0)])
    vz=np.array([X[2] for X in p.point_data.get_array(0)])
    identite=np.array([i for i in p.point_data.get_array(1)])
    #if verbose:print(p.point_data.get_array_name(1))
    typ=np.array([i for i in p.point_data.get_array(2)])
    d=np.array((x,y,z,vx,vy,vz,identite,typ)).T
    return d

def CalculateDistribution(FileLocation,File1, Date,LenBar,WidthBar,Smooth=1,FileFormat=1):
        """ Calculates the distribution of MTs along the cell (bars and barsprop) and the proportion of those MTs which are right moving (BarsDirecX). If smooth==2 add 0.3 to each adjacent bar as a contribution"""
        #BarXLabel=[-(i+0.5) for i in range(400)] #to plot the opposite way around with end of root hair at other end. 
        TotalBars=math.ceil(LenBar/WidthBar);
        BarXLabel=[(WidthBar*(i+0.5)) for i in range(TotalBars)]
        Bars=[0]*TotalBars #Total of MTs in each section of a 1 micrometer region
        BarsDirecX=[0]*TotalBars #Similar to Bars but proportion of MTs in that region which are right moving
        BarsVeryX=[0]*TotalBars
        BarsVeryMX=[0]*TotalBars

        FullFileName=FileLocation+File1
        p=lecture(FileLocation,File1)
        if not exists(FileLocation+File1): 
            BarsProp=float('NaN')
            BarXLabel=float('NaN')
            BarsDirecX=float('NaN')
            BarsVeryX=float('NaN')
            BarsVeryMX=float('NaN')
            Bars=float('NaN')
            All_X=float('NaN')
        else:
            dprovi=extraction(p)
            All_X=[]
            for i in dprovi:
                All_X.append(i[0]*8/1000)
                Temp=i[0]*8/1000/WidthBar  #Positoin in Microtmeters divided by bar width to put it in the right bar       
                Bars[math.floor(Temp)]=Bars[math.floor(Temp)]+1
                if Smooth!=1:
                    Bars[math.floor(Temp)-1]=Bars[math.floor(Temp)-1]+0.6
                    Bars[math.floor(Temp)+1]=Bars[math.floor(Temp)+1]+0.6
                    Bars[math.floor(Temp)-2]=Bars[math.floor(Temp)-2]+0.3
                    Bars[math.floor(Temp)+2]=Bars[math.floor(Temp)+2]+0.3
                if i[3]>0:
                    BarsDirecX[math.floor(Temp)]=BarsDirecX[math.floor(Temp)]+1
                    if Smooth!=1:
                        BarsDirecX[math.floor(Temp)+1]=BarsDirecX[math.floor(Temp)+1]+0.5
                        BarsDirecX[math.floor(Temp)-1]=BarsDirecX[math.floor(Temp)-1]+0.5
                TempDirec=math.atan(np.sqrt(i[4]**2+i[5]**2)/i[3])
                if TempDirec>0:#0.785:
                    BarsVeryX[math.floor(Temp)]=BarsVeryX[math.floor(Temp)]+1
                    if Smooth!=1:
                        BarsVeryX[math.floor(Temp)+1]=BarsVeryX[math.floor(Temp)+1]+0.5
                        BarsVeryX[math.floor(Temp)-1]=BarsVeryX[math.floor(Temp)-1]+0.5
                if TempDirec<0:#-0.785:
                    BarsVeryMX[math.floor(Temp)]=BarsVeryMX[math.floor(Temp)]+1
                    if Smooth!=1:
                        BarsVeryMX[math.floor(Temp)+1]=BarsVeryMX[math.floor(Temp)+1]+0.5
                        BarsVeryMX[math.floor(Temp)-1]=BarsVeryMX[math.floor(Temp)-1]+0.5
                    
            for i in range(len(BarsDirecX)):
                if Bars[i]!=0:
                    BarsDirecX[i]=BarsDirecX[i]/Bars[i]
                    BarsVeryX[i]=BarsVeryX[i]/Bars[i]
                    BarsVeryMX[i]=BarsVeryMX[i]/Bars[i]     
            

            Tot=np.sum(Bars)
            BarsProp=[i/Tot for i in Bars]
        return(BarsProp,BarXLabel,BarsDirecX,BarsVeryX,BarsVeryMX,Bars,All_X)

def PlotHistogram(All_X, BarXLabel,Bars,WidthBar,ylabel='MTs',Plotting=2,Xinit=22425,MovSpeed=0.3,IntBet=1,SaveFile=''):
    """Plot a histogram of the MT intensity data"""
    if Plotting<=1:
        fig,ax=plt.subplots()
        Bars=Bars/WidthBar #Needed to get histogram similar to gaussian fit when having non-zero WidthBar
        plt.bar(BarXLabel,Bars,width=WidthBar)
        plt.xlabel('Distance (micromemters)')
        plt.ylabel(ylabel)
        
        mu, std = norm.fit(All_X)
        p = norm.pdf(BarXLabel, mu, std)
        print(max(All_X))
        print(min(All_X))
        print(mu,std)
        plt.plot(BarXLabel, p, 'r', linewidth=2)
        
        if Plotting==0: #If zero look at nucleus position. 
            Xpos=(Xinit+MovSpeed*10000)*8/1000; #Estimate of nucleus position
            plt.plot([Xpos,Xpos],[0,max(Bars)],'--r') #Pot nucleus position
            if IntBet==0:
                IntensityBetweenNucMT(Bars,BarXLabel,Xpos) #Calcaulte estimate of MT intensity between nucleus and Tip.
        if Plotting==-1:
            plt.savefig(SaveFile)
        else:
            plt.show()

def PlotReducedHistogram(BarXLabel,Bars,WidthBar,lim,ylabel='MTs',Plotting=2):
    """Plot a histogram of the MT intensity data but only in x>lim"""
    if Plotting<=1:
        fig,ax=plt.subplots()
        m=[i for i in range(len(BarXLabel)) if BarXLabel[i]>lim]
        BarXLabelReduced=[BarXLabel[i] for i in m]
        BarsReduced=[Bars[i] for i in m]
        plt.bar(BarXLabelReduced,BarsReduced,width=WidthBar)
        plt.xlabel('Distance (micromemters)')
        plt.ylabel(ylabel)
        plt.show()


def PlotLinePropEachDirect(BarXLabel,BarsVeryX,BarsVeryMX,Plotting=2):  
    """Plot proportion of right and left moving MTs."""
    if Plotting==1:
        fig3,ax3=plt.subplots()
        plt.plot(BarXLabel,BarsVeryX,'-b',label='right')
        plt.plot(BarXLabel,BarsVeryMX,'-r',label='left')    
        plt.xlabel('Distance(micrometers)')
        plt.ylabel('Proportion of MTs')    
        plt.legend()
        plt.show()


def SmoothData(Bars):
    """Method to smooth MT data"""
    from scipy.signal import lfilter
    n = 15  # the larger n is, the smoother curve will be
    b = [1.0 / n] * n
    a = 1
    BarsSmoother = lfilter(b,a,Bars)
    return(BarsSmoother)

def NucleationRange(Reps,FileLocation,Date,LenBar,WidthBar,VTK_Num):
    for i in ['0_00001','0_00005','0_0001','0_0005','0_0004','0_0003','0_0002']:
        for j in ['0_0001', '0_0005', '0_001', '0_002', '0_003', '0_004', '0_005']:
            File=str(Date)+'_NucProba1_'+i+'_NucProb2_'+j+'_repeat_'
            [Bars,BarXLabel,BarsDirecX,BarsVeryX,BarsVeryMX,BarsTotals]=CalculateDistribution(Reps, FileLocation,Date,LenBar,WidthBar,VTK_Num,2,File,1) #If 1 use repeat format
            SaveFile=str(Date)+'_NucProba1_'+i+'_NucProb2_'+j+'.png'
            PlotHistogram(BarXLabel,BarsTotals,WidthBar,'All MTs',-1,0,0,0,SaveFile)

def ReadAnisostropy(File,pos):
    f2=open(File,"r") 
    lines=f2.readlines()
    Aniso=[[] for _ in pos]
    for x in lines:
        x=x.split(";")
        Nam=x[6]
        Nam=Nam.split("_")
        Nam=Nam[1]
        Nam=int(Nam)
        PosNum=math.floor(Nam/30)
        Aniso[PosNum].append(float(x[0]))
    return Aniso

def AnisotropyTotal(Date,pos):
    """Calcualte Anisotorpy for increasing position for a single dataset probably throughout the root hair """
    File='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/'+str(Date)+'.txt'
    Aniso=ReadAnisostropy(File,pos)
    PosMu=[j*8/1000 for j in pos]
    fig,ax1=plt.subplots(1,1)
    ax1.boxplot(Aniso,positions=PosMu)
    ax1.set_ylabel('Anisotropy')
    ax1.set_xticks([0,20,40,60,80,100,120,140])
    ax1.set_xticklabels([0,20,40,60,80,100,120,140])
    print(Aniso)
    plt.show()

def AnisotropyMidCortical(Date,pos):
    """Read line for the anisotropy text file in each region either mid or at the cortex."""
    File='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/'+str(Date)+'_Mid.txt'
    AnisoMid=ReadAnisostropy(File,pos)

    File='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/'+str(Date)+'_Cortex.txt'
    AnisoCor=ReadAnisostropy(File,pos)

    #Ttest to compare if both sets have the same mean
    #for j in range(len(pos)): 
    #    p=stats.ttest_ind(AnisoMid[j], AnisoCor[j])
    #    print(p)
    #    print(np.mean(AnisoMid[j])) 
    #    print(np.mean(AnisoCor[j]))  

    #Plot Boxplot
    PosMu=[j*8/1000 for j in pos]
    PosMu2=[i+0.3 for i in PosMu]
    PosMu1=[i-0.3 for i in PosMu]
    c1='red'
    fig,ax1=plt.subplots(1,1)
    ax1.boxplot(AnisoMid,positions=PosMu1,capprops=dict(color=c1), whiskerprops=dict(color=c1), flierprops=dict(color=c1, markeredgecolor=c1),medianprops=dict(color=c1))
    ax1.boxplot(AnisoCor,positions=PosMu2)
    ax1.set_ylabel('Anisotropy')
    ax1.set_xticks([0,20,40,60,80,100,120,140])
    ax1.set_xticklabels([0,20,40,60,80,100,120,140])
    plt.show()

def PlotMTProfiles(FileLocation,Date,LenBar,WidthBar,PosMu,CylPos):

    File=str(Date)+'_20_10000.vtk'
    [BarsProp,BarXLabel,BarsDirecX,BarsVeryX,BarsVeryMX,BarsTotals,All_X]=CalculateDistribution(FileLocation,File,Date,LenBar,WidthBar,2,1) #If 1 use repeat format otherwise you filename in function
    BarXLabel=[i-CylPos for i in BarXLabel]
    h4=plt.plot(BarXLabel,BarsTotals,'k',label=r'0.4 $\mu$m')

    File=str(Date)+'_140_10000.vtk'
    [BarsProp,BarXLabel,BarsDirecX,BarsVeryX,BarsVeryMX,BarsTotals,All_X]=CalculateDistribution(FileLocation,File,Date,LenBar,WidthBar,2,1) #If 1 use repeat format otherwise you filename in function
    BarXLabel=[i-CylPos for i in BarXLabel]
    h1=plt.plot(BarXLabel,BarsTotals,'--b',label=r'2 $\mu$m')

    File=str(Date)+'_260_10000.vtk'
    [BarsProp,BarXLabel,BarsDirecX,BarsVeryX,BarsVeryMX,BarsTotals,All_X]=CalculateDistribution(FileLocation,File,Date,LenBar,WidthBar,2,1) #If 1 use repeat format otherwise you filename in function
    BarXLabel=[i-CylPos for i in BarXLabel]
    h2=plt.plot(BarXLabel,BarsTotals,'-.g',label=r'3.6 $\mu$m')

    File=str(Date)+'_331_10000.vtk'
    [BarsProp,BarXLabel,BarsDirecX,BarsVeryX,BarsVeryMX,BarsTotals,All_X]=CalculateDistribution(FileLocation,File,Date,LenBar,WidthBar,2,1) #If 1 use repeat format otherwise you filename in function
    if (type(BarsProp) is list):
        BarXLabel=[i-CylPos for i in BarXLabel]
        h3=plt.plot(BarXLabel,BarsTotals,':r',label=r'4.8 $\mu$m')

    plt.xlim([50,250])
    #plt.ylim([0,0.05])
    plt.xlabel(r'Distance from Tip ($\mu$m)')
    plt.ylabel('Microtubule Detections')
    
    pos=[1035*8/1000,1390*8/1000,1989*8/1000,738*8/1000]
    
    
    plt.legend()
    plt.savefig('Size_Distributions.png')


def main():
    """
    Calculates the distribution of microtubules in the x direction. 
    """

    LenBar=400; #Micrometer total length of region MT density considered within 
    WidthBar=1; #Micrometer width of region considered

    Date='2021121716141639757667' #Updated to ellipse data
    FileLocation='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/Data/VTK/'+str(Date)+'/' #folder of data 

    TopNum=360

    Size=[50,100,150,200,250,300,350,400,450,500,550,600]

    LongAxis=[j*8/1000 for j in Size]
    Mean=[[] for _ in Size]
    SD=[[] for _ in Size]
    CylPos=800*8/1000 #Root starts 800 units in from the edge    
    #for j in [1,600]:
    #    PosNum=math.floor(j/30);
    #    print(pos[PosNum])
#
    #Calculate Anisotropy (either throughout or near cortex/central region)

    PlotMTProfiles(FileLocation,Date,LenBar,WidthBar,LongAxis,CylPos)
    #AnisotropyTotal(Date,pos)



    #PlotHistogram(All_X,BarXLabel,BarsProp,WidthBar,'All MTs',1) #Plot histogram of total detections. if Plotting second input<=1

    for j in range(0,TopNum):
        PosNum=math.floor(j/30);
        #if PosNum!=11:
        File=str(Date)+'_'+str(j)+'_10000.vtk'
        [BarsProp,BarXLabel,BarsDirecX,BarsVeryX,BarsVeryMX,BarsTotals,All_X]=CalculateDistribution(FileLocation,File,Date,LenBar,WidthBar,2,1) #If 1 use repeat format otherwise you filename in function
        if (type(BarsProp) is list):
            mu, std = norm.fit(All_X)
            Mean[PosNum].append(mu)
            SD[PosNum].append(std)
            

    AverageMean=[np.mean(j) for j in Mean]
    AverageSD=[np.mean(j) for j in SD]
    fig,ax2=plt.subplots(1,1)
    #ax1.plot(PosMu,AverageMean,'+')
    #ax1.set_ylabel('Mean position')
    #ax2.plot(PosMu,AverageSD,'+')
    #ax1.boxplot(Mean,positions=LongAxis)
    #ax1.set_ylabel('Mean position')
    #ax1.set_xticks([0,2,4,6,8,10,12,14,16,18,20])
    #ax1.set_xticklabels([0,2,4,6,8,10,12,14,16,18,20])
    ax2.boxplot(SD,positions=LongAxis,widths=0.1) 
    ax2.set_ylabel('Mean standard deviation')
    ax2.set_xlabel(r'Radii ($\mu$m)')
    ax2.set_xticks([0,1,2,3,4,5,6])
    ax2.set_xticklabels([0,1,2,3,4,5,6])
    ax2.set_ylim([0,25])
    
    plt.savefig('Size_Boxplot.png')
        
        
if __name__ == '__main__':
    main()
