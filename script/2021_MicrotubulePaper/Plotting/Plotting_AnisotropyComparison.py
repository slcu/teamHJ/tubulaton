#!/usr/bin/python
# -*- coding: utf-8 -*-
import vtk
import math
import numpy as np
#from tvtk.api import tvtk
import matplotlib.pyplot as plt
import os, sys
import scipy.stats as sta
from scipy.stats import norm  
from scipy import stats
sys.path.insert(1, '/home/tamsin/Active/Documents/SegmentNuclei')
import Hanna_AnisotropyDifference as HAD
  
def lecture(rep,nom):
    """Read in data from the .vtk file output from the microtubule simulations. Copied from CalculateAnisotropy.
       Inputs: nom is the name of the file. rep is the file location, if none provided assume in current directory. 
    Output: p the read in data   """
    r=tvtk.PolyDataReader()
    r.file_name=rep+nom
    #print (r.file_name)
    r.update()
    p=r.output
    return p

def extraction(p,verbose=0):
    """Copied from CalculateAnisotropy
	This function extracts data from p. It creates a numpy array with the following data in each position:
    0, 1, 2, 3 , 4,  5,  6,        7
    X, Y, Z, VX, VY, VZ, identite, type
    
    c'est ici qu'il faut que je rajoute la coque si elle n'y est pas spontanément.
    """
    x=np.array([X[0] for X in p.points])
    y=np.array([X[1] for X in p.points])
    z=np.array([X[2] for X in p.points])
    vx=np.array([X[0] for X in p.point_data.get_array(0)])
    vy=np.array([X[1] for X in p.point_data.get_array(0)])
    vz=np.array([X[2] for X in p.point_data.get_array(0)])
    identite=np.array([i for i in p.point_data.get_array(1)])
    #if verbose:print(p.point_data.get_array_name(1))
    typ=np.array([i for i in p.point_data.get_array(2)])
    d=np.array((x,y,z,vx,vy,vz,identite,typ)).T
    return d

def CalculateDistribution(FileLocation,File1, Date,LenBar,WidthBar,Smooth=1,FileFormat=1):
        """ Calculates the distribution of MTs along the cell (bars and barsprop) and the proportion of those MTs which are right moving (BarsDirecX). If smooth==2 add 0.3 to each adjacent bar as a contribution"""
        #BarXLabel=[-(i+0.5) for i in range(400)] #to plot the opposite way around with end of root hair at other end. 
        TotalBars=math.ceil(LenBar/WidthBar);
        BarXLabel=[(WidthBar*(i+0.5)) for i in range(TotalBars)]
        Bars=[0]*TotalBars #Total of MTs in each section of a 1 micrometer region
        BarsDirecX=[0]*TotalBars #Similar to Bars but proportion of MTs in that region which are right moving
        BarsVeryX=[0]*TotalBars
        BarsVeryMX=[0]*TotalBars

        FullFileName=FileLocation+File1
        p=lecture(FileLocation,File1)
        dprovi=extraction(p)
        All_X=[]
        for i in dprovi:
            All_X.append(i[0]*8/1000)
            Temp=i[0]*8/1000/WidthBar  #Positoin in Microtmeters divided by bar width to put it in the right bar       
            Bars[math.floor(Temp)]=Bars[math.floor(Temp)]+1
            if Smooth!=1:
                Bars[math.floor(Temp)-1]=Bars[math.floor(Temp)-1]+0.6
                Bars[math.floor(Temp)+1]=Bars[math.floor(Temp)+1]+0.6
                Bars[math.floor(Temp)-2]=Bars[math.floor(Temp)-2]+0.3
                Bars[math.floor(Temp)+2]=Bars[math.floor(Temp)+2]+0.3
            if i[3]>0:
                BarsDirecX[math.floor(Temp)]=BarsDirecX[math.floor(Temp)]+1
                if Smooth!=1:
                    BarsDirecX[math.floor(Temp)+1]=BarsDirecX[math.floor(Temp)+1]+0.5
                    BarsDirecX[math.floor(Temp)-1]=BarsDirecX[math.floor(Temp)-1]+0.5
            TempDirec=math.atan(np.sqrt(i[4]**2+i[5]**2)/i[3])
            if TempDirec>0:#0.785:
                BarsVeryX[math.floor(Temp)]=BarsVeryX[math.floor(Temp)]+1
                if Smooth!=1:
                    BarsVeryX[math.floor(Temp)+1]=BarsVeryX[math.floor(Temp)+1]+0.5
                    BarsVeryX[math.floor(Temp)-1]=BarsVeryX[math.floor(Temp)-1]+0.5
            if TempDirec<0:#-0.785:
                BarsVeryMX[math.floor(Temp)]=BarsVeryMX[math.floor(Temp)]+1
                if Smooth!=1:
                    BarsVeryMX[math.floor(Temp)+1]=BarsVeryMX[math.floor(Temp)+1]+0.5
                    BarsVeryMX[math.floor(Temp)-1]=BarsVeryMX[math.floor(Temp)-1]+0.5
                    
        for i in range(len(BarsDirecX)):
            if Bars[i]!=0:
                BarsDirecX[i]=BarsDirecX[i]/Bars[i]
                BarsVeryX[i]=BarsVeryX[i]/Bars[i]
                BarsVeryMX[i]=BarsVeryMX[i]/Bars[i]     
            

        Tot=np.sum(Bars)
        BarsProp=[i/Tot for i in Bars]
        return(BarsProp,BarXLabel,BarsDirecX,BarsVeryX,BarsVeryMX,Bars,All_X)

def ReadAnisostropy(File):
    f2=open(File,"r") 
    lines=f2.readlines()
    Aniso=[]
    for x in lines:
        x=x.split(";")
        Nam=x[6]
        Nam=Nam.split("_")
        Nam=Nam[1]
        Nam=int(Nam)
        PosNum=math.floor(Nam/30)
        if float(x[5])==10000:
            Aniso.append(float(x[0]))
    return Aniso

def AnisotropyTotal(Date_Spat,Date_Tip,Aniso_Experiment):
    """Calcualte the total anisotropy """
    File='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/'+str(Date_Spat)+'.txt'
    Aniso_Spat=ReadAnisostropy(File)

    File='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/'+str(Date_Tip)+'.txt'
    Aniso_Tip=ReadAnisostropy(File)    


    fig,ax1=plt.subplots(1,1)
    ax1.boxplot([Aniso_Spat,Aniso_Tip,Aniso_Experiment])
    ax1.set_ylabel('Anisotropy')
    ax1.set_xticks([1,2,3])
    ax1.set_xticklabels(['Stabilisation','Tip Nucleation','Experiments'])
    ax1.set_ylim([0,0.5])
    plt.show()

def AnisotropyFrontBack(Date_Spat,Date_Tip,Aniso_Experiment):
    """Calcualte the anisotropy difference in front and behind the nucleus - CHECK THIS """
    
    #Spatial Nucleation
    File='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/'+str(Date_Spat)+'_FrontNuc.txt'
    Aniso_Spat_Front=ReadAnisostropy(File)
    File='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/'+str(Date_Spat)+'_BackNuc.txt'
    Aniso_Spat_Back=ReadAnisostropy(File)
    NucDifAnisoSpat=[]
    for i,j in zip(Aniso_Spat_Front,Aniso_Spat_Back):
        NucDifAnisoSpat.append(i-j)
    print("Ttest Tip Nucleation: ",sta.ttest_1samp(NucDifAnisoSpat,0))

    #Tip Stabilisation 
    File='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/'+str(Date_Tip)+'_FrontNuc.txt'
    Aniso_Tip_Front=ReadAnisostropy(File)
    File='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/'+str(Date_Tip)+'_BackNuc.txt'
    Aniso_Tip_Back=ReadAnisostropy(File)
    NucDifAnisoTip=[]
    for i,j in zip(Aniso_Tip_Front,Aniso_Tip_Back):
        NucDifAnisoTip.append(i-j)

    print("Ttest Tip stabilisation: ",sta.ttest_1samp(NucDifAnisoTip,0))

    print("Aniso experiments")
    print(Aniso_Experiment)

    print("Experiments: ",sta.ttest_1samp(Aniso_Experiment,0))   

    fig,ax1=plt.subplots(1,1)
    ax1.boxplot([NucDifAnisoSpat,NucDifAnisoTip,Aniso_Experiment])
    ax1.set_ylabel('Anisotropy Difference')
    ax1.set_xticks([1,2,3])
    ax1.set_xticklabels(['Tip Nucleation','Stabilisation','Experiments'])
    ax1.set_ylim([-0.3,0.3])
    plt.plot([0,4],[0,0],'--k')
    x_ran=[1 for i in NucDifAnisoSpat]
    plt.plot(x_ran,NucDifAnisoSpat,'+k')
    x_ran=[2 for i in NucDifAnisoTip]
    plt.plot(x_ran,NucDifAnisoTip,'+k')
    x_ran=[3 for i in Aniso_Experiment]
    plt.plot(x_ran,Aniso_Experiment,'+k')
    plt.show()

def AniosotropyNec(Date_NecNuc,FileLocation_NecNuc,Aniso_Experiment):
    """Calculate the anisotropy from Neck nucleation  """
    #Neck Nucleation
    File='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/'+str(Date_NecNuc)+'_FrontNuc.txt'
    Aniso_Spat_Front=ReadAnisostropy(File)
    File='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/'+str(Date_NecNuc)+'_BackNuc.txt'
    Aniso_Spat_Back=ReadAnisostropy(File)
    NucDifAnisoNec=[]
    for i,j in zip(Aniso_Spat_Front,Aniso_Spat_Back):
        NucDifAnisoNec.append(i-j)
    print("Ttest Neck Nucleation: ",sta.ttest_1samp(NucDifAnisoNec,0))
    
    fig,ax1=plt.subplots(1,1)
    ax1.boxplot([NucDifAnisoNec])
    ax1.set_ylabel('Anisotropy Difference')
    ax1.set_xticks([1])
    ax1.set_xticklabels(['Neck Nucleation'])
    ax1.set_ylim([-0.3,0.3])
    plt.plot([0,2],[0,0],'--k')
    x_ran=[1 for i in NucDifAnisoNec]
    plt.plot(x_ran,NucDifAnisoNec,'+k')
    
    fig2,ax2=plt.subplots(1,1)   
    ax2.boxplot([NucDifAnisoNec,Aniso_Experiment])
    ax2.set_ylabel('Anisotropy Difference')
    ax2.set_xticks([1,2])
    ax2.set_xticklabels(['Theory','Experiments'])
    ax2.set_ylim([-0.2,0.1])
    plt.plot([0,3],[0,0],'--k')
    x_ran=[1 for i in NucDifAnisoNec]
    plt.plot(x_ran,NucDifAnisoNec,'+k')
    x_ran=[2 for i in Aniso_Experiment]
    plt.plot(x_ran,Aniso_Experiment,'+k') 
    fig2.savefig('NeckExperiments.png', format='png', dpi=600)
    plt.show()


def main():
    """
    Calculate different measures comparing the simulations
    """
    #Experimental Data
    FileName,Res = HAD.ExperimentalData()
    print(FileName)
    AllDiff=[]  ##List of anisotropy difference
    TotAniso=[]
    MidAniso=[]
    TopAniso=[]
    for j,u in zip(FileName,Res):
        j='../../../'+j
        (Dif,Aniso,TopA,MidA,TotA)=HAD.CalcFromFile(j,u)
        AllDiff.append(Dif)
        TotAniso.append(TotA)
        TopAniso.append(TopA)
        MidAniso.append(MidA)

    #Simulation Data
    Date_Tip='2022033017411648658496' #Tip stabilisation
    FileLocation_Stab='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/Data/VTK/'+str(Date_Tip)+'/' #folder of data 

    Date_Spat='2022033018291648661342' #Tip and nucleus nucleation
    FileLocation_Spatial='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/Data/VTK/'+str(Date_Spat)+'/' #folder of data 
    
    Date_NecNuc='2022100710001665133236'
    FileLocation_NecNuc='/home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/Data/VTK/'+str(Date_Spat)+'/' #folder of data     

    #AnisotropyTotal(Date_Spat,Date_Tip,TotAniso)

    #AnisotropyFrontBack(Date_Spat,Date_Tip,AllDiff)

    AniosotropyNec(Date_NecNuc,FileLocation_NecNuc,AllDiff)

if __name__ == '__main__':
    main()



