import vtk

def read_vtk(vtk_path):
    """Read in the data of the vtk mesh"""
    reader = vtk.vtkPolyDataReader()
    reader.SetFileName(vtk_path)
    reader.ReadAllVectorsOn()
    reader.ReadAllScalarsOn()
    reader.Update()
    data = reader.GetOutput()
    return data

def main():
    """ Read in the vtk point data and check the contact values as a check. """
    #tx='/home/tamsin/Active/Documents/tubulaton/Output/test_0_1_1_500.vtk'
    #tx='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/NucleusInside/Test_repeat_30_990.vtk'
    #data=read_vtk(tx)
    #Contacts=data.GetPointData().GetArray("structure_num")
    #LenNum=data.GetNumberOfPoints()
    #Cont=[]
    #for j in range(0,LenNum):
    #    Cont.append(Contacts.GetValue(j))
    #print(set(Cont))

    #tx='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020080415171596550668/Test_repeat_30__Cont_2_990.vtk'
    #tx='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020080411251596536705/2020080411251596536705_149__Cont_2_10000.vtk'
    tx='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2020080415171596550668/2020080415171596550668_149__Cont_2_10000.vtk'
    #tx='/home/tamsin/Active/Documents/SegmentNuclei/Plant_1_single_root_hair_without_OZNucNum_1_Time_0.vtk'

    data=read_vtk(tx)
    ForceX=data.GetPointData().GetArray("ForcesLengthX")
    LenNum=data.GetNumberOfPoints()
    Cont=[]
    for j in range(0,LenNum):
        Cont.append(ForceX.GetValue(j))
    #print(Cont)
    Cont_Mi=[]
    for i in Cont:
        if i!=0:
            Cont_Mi.append(i)  
    print(Cont_Mi)  
    
    
if __name__ == '__main__':
    main()