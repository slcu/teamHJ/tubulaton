#!/usr/bin/python
# -*- coding: utf-8 -*- 
# NOTE: This code is written in Python 3. 
import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
import scipy.stats
import math
from mayavi import mlab
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

# function for setting the colors of the box plots pairs
def setBoxColors(bp,num):
    #Set colours of the boxplots
    plt.setp(bp['boxes'][0], color='blue')
    plt.setp(bp['caps'][0], color='blue')
    plt.setp(bp['caps'][1], color='blue')
    plt.setp(bp['whiskers'][0], color='blue')
    plt.setp(bp['whiskers'][1], color='blue')
    plt.setp(bp['fliers'][0], color='blue')
    plt.setp(bp['medians'][0], color='blue')
    
    if num>=2:
        plt.setp(bp['boxes'][1], color='red')
        plt.setp(bp['caps'][2], color='red')
        plt.setp(bp['caps'][3], color='red')
        plt.setp(bp['whiskers'][2], color='red')
        plt.setp(bp['whiskers'][3], color='red')
        plt.setp(bp['fliers'][1], color='red')
        plt.setp(bp['medians'][1], color='red')
    
    if num>=3:
        plt.setp(bp['boxes'][2], color='green')
        plt.setp(bp['caps'][3], color='green')
        plt.setp(bp['caps'][4], color='green')
        plt.setp(bp['whiskers'][4], color='green')
        plt.setp(bp['whiskers'][5], color='green')
        plt.setp(bp['fliers'][2], color='green')
        plt.setp(bp['medians'][2], color='green')

    if num>4:
        plt.setp(bp['boxes'][3], color='yellow')
        plt.setp(bp['caps'][4], color='yellow')
        plt.setp(bp['caps'][5], color='yellow')
        plt.setp(bp['whiskers'][6], color='yellow')
        plt.setp(bp['whiskers'][7], color='yellow')
        plt.setp(bp['fliers'][3], color='yellow')
        plt.setp(bp['medians'][3], color='yellow')
        
def mean_confidence_interval(data):
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    #h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1) #Equivalent to line below if take m+h, m-h with confidence as 0.9 or 0.95 not sure which
    #[cfb,cft]=scipy.stats.t.interval(alpha=0.95, df=n-1, loc=m, scale=se) ##Calculates confidence intervals using t-test 
    cfb,cft=scipy.stats.norm.interval(alpha=0.95, loc=m, scale=se)
    return m, cfb, cft

def Quartiles(data):
    """Calculate medium (q2) and first and third quartiles for input data list 'data' """
    a = 1.0 * np.array(data)
    q1=np.quantile(a, 0.25, axis = None)
    q2=np.quantile(a, 0.5, axis = None)
    q3=np.quantile(a, 0.75, axis = None)
    return q1, q2, q3
    
def PositionRange():
    """Plot for a range of elipse elongations 
    """
    ##########Set variables
    #args = sys.argv[1:]
    #FileLocation="/home/tamsin/Active/mount/hpc_slcu/Output/PythonGeneratedOutput/ForceTextFiles/2020080417081596557319.txt" #Highest nucleation rate
    #FileLocation="/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/ForceTextFiles/2020080417081596557319.txt" #Highest nucleation rate
    #NucPos=[1425,2425,3425,4425,5425,6425,7425,8425,9425,10425,11425,12425,13425,14425,15425,16425,17425,18425]

    FileLocation="/home/tamsin/Active/mount/hpc_slcu/Output/PythonGeneratedOutput/ForceTextFiles/2020080517281596644884.txt"
    NucPos=[1425,1625,1825,2025,2225,2425,2625,2825,3425,4425,5425,6425,7425,8425,9425,10425,11425]

    #FileLocation="/home/tamsin/Active/mount/hpc_slcu/Output/PythonGeneratedOutput/ForceTextFiles/2020080712211596799269.txt"
    #NucPos=[1425,1625,1825,2025,2225,2425,2625,2825,3425,4425,5425,6425,7425,8425,9425,10425,11425]
    
    #########Read File Location
    f=open(FileLocation,"r")
    lines=f.readlines()


    ###########Extract total force on object from text file
    ForceLength=[[] for i in NucPos]
    ForcePushing=[[] for i in NucPos]
    ForceLengthX=[[] for i in NucPos]
    ForcePushingX=[[] for i in NucPos]
    for x in lines: 
        x=x.split(";")
        T=x[1]
        T=T.split("_")
        T=T[4]
        T=T[4:]
        T=int(T)
        if T in NucPos:
            ind=NucPos.index(T)
            ForceLength[ind].append(float(x[36])) #First entry of force length
            ForcePushing[ind].append(float(x[46])) #First entry of force length
            ForceLengthX[ind].append(float(x[37])) #First entry of force length
            ForcePushingX[ind].append(float(x[47])) #First entry of force length
            
    NucPos_Mum=[(i-800)*8/1000 for i in NucPos]
    #fig, ax1 = plt.subplots()
    #bp=ax1.boxplot(ForceLength, widths = 0.6)
    #plt.xlabel('Distance (\mu m)')
    #plt.ylabel('Force')    
    #ax1.set_xticklabels(NucPos_Mum[:len(ForceLength)])
    #plt.show()
    
    ############### Plot total number of Interactions with the Nucleus
    #fig2, ax2 = plt.subplots()
    #AllMeans=[]
    #for j in range(0,len(NucPos)):
    #    m,bci,tci=mean_confidence_interval(ForcePushing[j])
    #    #bci,m,tci=Quartiles(ForceLength[j])
    #    plt.plot([NucPos_Mum[j],NucPos_Mum[j]],[bci,tci],'bo-')
    #    #plt.plot(NucPos_Mum[j],m,'b+')
    #    AllMeans.append(m)
    #plt.plot(NucPos_Mum,AllMeans,'b+-')
    #plt.ylabel(r'Overll number of MTs pushing on nucleus')
    #plt.xlabel(r'Distance from cell end ($\mu$m)')
    #plt.show()
    
    ############## Plot net length in x direction 
    fig3, ax3 = plt.subplots()
    AllMeans=[]
    for j in range(0,len(NucPos)):
        m,bci,tci=mean_confidence_interval(ForcePushingX[j])
        #bci,m,tci=Quartiles(ForceLength[j])
        plt.plot([NucPos_Mum[j],NucPos_Mum[j]],[bci,tci],'bo-')
        #plt.plot(NucPos_Mum[j],m,'b+')
        AllMeans.append(m)
    plt.plot(NucPos_Mum,AllMeans,'b+-')
    plt.ylabel(r'Net MT length pulling nucleus in X direction')
    plt.xlabel(r'Distance from cell end ($\mu$m)')
    plt.plot([0,NucPos_Mum[-1]],[0,0],'k--')
    ax3.set_xlim([0,NucPos_Mum[-1]+5])
    plt.show()

def ElipseRange2():
    """Plot mean and medium for pushing and length pulling forces
    """
    ##########Set variables
    #args = sys.argv[1:]
    #FileLocation="/home/tamsin/Active/mount/hpc_slcu/Output/PythonGeneratedOutput/ForceTextFiles/2020062613261593174418.txt" #Highest nucleation rate
    #NucRad=[400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600]   
    
    FileLocation="/home/tamsin/Active/mount/hpc_slcu/Output/PythonGeneratedOutput/ForceTextFiles/2020080417131596557619.txt"
    NucRad=[2500,2217,1989,1801,1643,1507,1390,1286,1194,1101,1035,965,900,840,783,728,676,625]
    rad_all=[200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625]  
        
    #########Read File Location
    f=open(FileLocation,"r")
    lines=f.readlines()


    ###########Extract total force on object from text file
    ForceLength=[[] for i in NucRad]
    ForcePushing=[[] for i in NucRad]
    ForceLengthX=[[] for i in NucRad]
    ForcePushingX=[[] for i in NucRad]
    for x in lines: 
        x=x.split(";")
        T=x[1]
        T=T.split("_")
        T=T[1]
        T=T[3:]
        T=int(T)
        if T in NucRad:
            ind=NucRad.index(T)
            ForceLength[ind].append(float(x[36])) #First entry of force length
            ForcePushing[ind].append(float(x[46])) #First entry of force length
            ForceLengthX[ind].append(float(x[37])) #First entry of force length
            ForcePushingX[ind].append(float(x[47])) #First entry of force length
            
    ElipAspect=[NucRad[i]/rad_all[i] for i in range(len(NucRad))]    
    ############## Plot force against elongation 
    fig3, ax3 = plt.subplots()
    AllMeans=[]
    AllMeans1=[]
    for j in range(0,len(NucRad)):
        m,bci,tci=mean_confidence_interval(ForceLengthX[j])
        #bci,m,tci=Quartiles(ForceLength[j])
        plt.plot([ElipAspect[j],ElipAspect[j]],[bci,tci],'bo-')
        AllMeans.append(m)
        #plt.plot(NucPos_Mum[j],m,'b+')
        m,bci,tci=mean_confidence_interval(ForceLength[j])
        #bci,m,tci=Quartiles(ForceLength[j])
        plt.plot([ElipAspect[j],ElipAspect[j]],[bci,tci],'ro-')
        AllMeans1.append(m)
    plt.plot(ElipAspect,AllMeans,'b+-')
    plt.plot(ElipAspect,AllMeans1,'r+-')
    plt.ylabel(r'Net MT length pulling nucleus in X direction')
    plt.xlabel(r'Aspect Ratio')
    plt.plot([1,ElipAspect[0]],[0,0],'k--')
    #ax3.set_xlim([0,ElipAspect[-1]+5])
    plt.show()

    #fname="RadiusIncrease".png"
    #print(fname)
    #plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format=None, transparent=False, bbox_inches=None, pad_inches=0.1, frameon=None, metadata=None)
    plt.show()

def ElipseRange():
    """Plot for a range of elipse elongations 
    """
    ##########Set variables
    #args = sys.argv[1:]
    #FileLocation="/home/tamsin/Active/mount/hpc_slcu/Output/PythonGeneratedOutput/ForceTextFiles/2020062613261593174418.txt" #Highest nucleation rate
    #NucRad=[400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600]
    #NucRad2=[625,781,938,1094,1250,1406,1563,1719,1875,2031,2188,2344,2500]
    
    FileLocation="/home/tamsin/Active/mount/hpc_slcu/tubulaton/Output/PythonGeneratedOutput/ForceTextFiles/2020070618011594054866.txt" #Highest nucleation rate
    NucRad=[2500,2217,1989,1801,1643,1507,1390,1286,1194,1101,1035,965,900,840,783,728,676,625]
    rad_all=[200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625]
    NucRad2=[]  
        
    #########Read File Location
    f=open(FileLocation,"r")
    lines=f.readlines()


    ###########Extract total force on object from text file
    ForceTot=[[] for i in NucRad]
    ForceTot2=[[] for i in NucRad2]
    for x in lines: 
        x=x.split(";")
        T=x[1]
        T=T.split("_")
        T=T[1]
        T=T[3:]
        T=int(T)
        if T in NucRad:
            ind=NucRad.index(T)
            ForceTot[ind].append(int(x[5]))
        if T in NucRad2:
            ind=NucRad2.index(T)
            ForceTot2[ind].append(int(x[5]))
    
    ############ Boxplot showing increasing forces
    Nuc=[j/625 for j in NucRad]
    fig, ax1 = plt.subplots()
    bp=ax1.boxplot(ForceTot, widths = 0.6)
    ax1.boxplot(ForceTot2, widths = 0.6)
    plt.xlabel('Aspect Ratio')
    plt.ylabel('Interactions')    
    ax1.set_xticklabels(Nuc[:len(ForceTot)])
    ############ Line plot of radius against mean
    MeanNum=[]
    for j in ForceTot:
        MeanNum.append(np.mean(j))
    x=list(range(1,(len(ForceTot)+1))) 
    MeanNum2=[]
    for j in ForceTot2:
        MeanNum2.append(np.mean(j))
    x2=list(range(1,(len(ForceTot2)+1))) 
    plt.plot(x,MeanNum,'-+')
    plt.plot(x2,MeanNum2,'-+')
    
    ##Estimate surface area
    x=[]
    rad=400*8/1000
    p=1.6075
    for j in NucRad:
        elrad=j*8/1000
        temp=(rad**p)*(rad**p)+(rad**p)*(elrad**p)+(rad**p)*(elrad**p)
        S=4*math.pi*((temp/3)**(1/p))
        x.append(S)
    x2=[]
    rad=625*8/1000
    p=1.6075
    for j in NucRad2:
        elrad=j*8/1000
        temp=(rad**p)*(rad**p)+(rad**p)*(elrad**p)+(rad**p)*(elrad**p)
        S=4*math.pi*((temp/3)**(1/p))
        x2.append(S)

    x=[]
    p=1.6075
    Count=0
    for j in NucRad:
        rad=rad_all[Count]*8/1000
        elrad=j*8/1000
        temp=(rad**p)*(rad**p)+(rad**p)*(elrad**p)+(rad**p)*(elrad**p)
        S=4*math.pi*((temp/3)**(1/p))
        x.append(S)
        Count=Count+1

    fig2, ax2 = plt.subplots()
    plt.plot(x,MeanNum,'-+')
    plt.plot(x2,MeanNum2,'-+')
    plt.xlabel('Surface area (micrometers^2)')
    plt.ylabel('Interactions')    

    #fname="RadiusIncrease".png"
    #print(fname)
    #plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format=None, transparent=False, bbox_inches=None, pad_inches=0.1, frameon=None, metadata=None)
    plt.show()
    
def SizeRange():
    """Plot for a range of bundles which I enter as a list manually. Similarly for the cut list. Similarly for a range of bundling and cuts which I enter manually.
    """
    ##########Set variables
    #args = sys.argv[1:]
    #FileLocation="/home/tamsin/Active/mount/hpc_slcu/tubulaton/Output/PythonGeneratedOutput/ForceTextFiles/2020061516291592234943.txt" #Highest nucleation rate
    #FileLocation="/home/tamsin/Active/mount/hpc_slcu/tubulaton/Output/PythonGeneratedOutput/ForceTextFiles/2020062316181592925508.txt" #Middle nucleation rate
    FileLocation="/home/tamsin/Active/mount/hpc_slcu/tubulaton/Output/PythonGeneratedOutput/ForceTextFiles/2020062315571592924270.txt" #Low nucleation rate
    #FileLocation="/home/tamsin/Active/mount/hpc_slcu/tubulaton/Output/PythonGeneratedOutput/ForceTextFiles/2020070716581594137483.txt" #Larger cylinder
    NucRad=[25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625]
    Rep=30;
    
    
    #########Read File Location
    f=open(FileLocation,"r")
    lines=f.readlines()


    ###########Extract total force on object from text file
    ForceTot=[[] for i in NucRad]
    for x in lines: 
        x=x.split(";")
        T=x[1]
        T=T.split("_")
        T=T[1]
        T=T[3:]
        T=int(T)
        ind=NucRad.index(T)
        ForceTot[ind].append(int(x[5]))
    
    ############ Boxplot showing increasing forces
    Nuc=[j*8/1000 for j in NucRad]
    #Nuc=[x**2 for x in Nuc]
    fig, ax1 = plt.subplots()
    bp=ax1.boxplot(ForceTot, widths = 0.6)
    ax1.set_xticklabels(Nuc[:len(ForceTot)])
    plt.xlabel('Radius (micrometers)')
    plt.ylabel('Interactions')    
    ############ Line plot of radius against mean
    #fig2, ax2 = plt.subplots()
    MeanNum=[]
    for j in ForceTot:
        MeanNum.append(np.mean(j))
    #plt.plot(Nuc[:len(ForceTot)],MeanNum,'-+')
    x=list(range(1,(len(ForceTot)+1)))  
    #print(x)  
    plt.plot(x,MeanNum,'-+')
    #plt.xlabel('Radius^2 (micrometers^2)')
    #plt.ylabel('Interactions')

    #fname="RadiusIncrease".png"
    #print(fname)
    #plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format=None, transparent=False, bbox_inches=None, pad_inches=0.1, frameon=None, metadata=None)
    plt.show()

def main():
    #SizeRange()
    #ElipseRange()
    #PositionRange()
    ElipseRange2()
    
if __name__ == '__main__':
    main()
