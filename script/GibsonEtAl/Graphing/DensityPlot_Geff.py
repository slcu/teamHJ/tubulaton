#!/usr/bin/python
# -*- coding: utf-8 -*- 
# NOTE: This code is written in Python 3. 
import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
import math
from mayavi import mlab
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.ticker import FormatStrFormatter


def Inputs():
    """ Output: Two lists Inp2 and Inp of file names output by tubulaton
    
    In the function ADD the names of the text files that density data should be read in for. There is one density file per (rc, rn) pair
    Inp and Inp2 are two Python lists (see examples below). Each list should contain all the files in order corresponding to the
    lower density first and the higher density. The boxplot will plot in the order of the files so it is advised to go from lowest to 
    highest rc value. x-labels of rc values are plotted based in Inp rc values so it is recommened to have the same rc values for both 
    sets of data. """
    
    


    # Fig 7 no-zip and zip, small sphere
    Inp=['Zip_Den_2023041909491681894158.txt',
    'Zip_Den_2023041909491681894167.txt',
    'Zip_Den_2023041822301681853426.txt',
    'Zip_Den_2023041822301681853424.txt',
    'Zip_Den_2023041822301681853420.txt',
    'Zip_Den_2023041822301681853416.txt',
    'Zip_Den_2023041822301681853413.txt',
    'Zip_Den_2023041822301681853410.txt']
    
    
    Inp2=['NoZip_Den_2023041909371681893441.txt',
    'Density_SmallSphere_Den_2023041111361681209374.txt',
    'Density_SmallSphere_Den_2023041111361681209372.txt',
    'Density_SmallSphere_Den_2023041111361681209367.txt',
    'Density_SmallSphere_Den_2023041111361681209364.txt',
    'Density_SmallSphere_Den_2023041111361681209362.txt',
    'Density_SmallSphere_Den_2023041111351681209359.txt',
    'Density_SmallSphere_Den_2023041111351681209357.txt']   
    
    #InpAll=[Inp2,Inp]
    InpAll=[Inp]    #Change this line for the zip or no-zippering plot you want

    return(InpAll)


def main():
    """Similar to DensityPlot but on the x-axis plots G_eff rather than rc. (This script plots the density of microtubules from two sets of data Inp and Inp2 as a boxplot.)
    """
    #Real in variables
    FileLocation='../../../Output/PythonGeneratedOutput/AnisotropyTextFiles/'  #Location of text files
    Inp_All=Inputs()  #Text files



    #For loop over all lines in each text file to extract density, rc and rn
    Count_Dataset=1 #change this to 0 or 1 depending on the desired color of the output
    for Inp in Inp_All:
        fig, ax = plt.subplots()
        Density=[[] for _ in Inp]
        Density_Average=[]
        Anisotropy=[[] for _ in Inp]
        Anisotropy_Average=[]
        rc=[]
        rn=[]
        for i,j in enumerate(Inp):
            File=FileLocation+j
            f=open(File,"r")
            lines=f.readlines()

            Count=0
            for x in lines:
                x=x.split(";")
                if Count==0:         
                    rc.append(round(float(x[9]),5))
                    rn.append(float(x[10]))
                    Count=Count+1
                if abs(float(x[6])-10000)<0.00001:
                    Density[i].append(float(x[13]))
                    Anisotropy[i].append(abs(float(x[14])))

            Density_Average.append(np.mean(Density[i]))
            Anisotropy_Average.append(np.mean(Anisotropy[i]))
        sublist=[max(i) for i in Density]
        MaxY=max(sublist)+1000
 

 
        #Boxplot
        sns.set_theme(style='whitegrid')
        if Count_Dataset==0:
            col2=(0.98, 0.5, 0.5)
            col="red"
            Count_Dataset=Count_Dataset+1
        else:
            col2=(0.6, 1, 1)
            col="blue"
        pal2 = {col2 for _ in Density}
        pal = {col for _ in Density}
        hello=sns.boxenplot(Density,showfliers=False , palette=pal2)
        hello=sns.stripplot(Density, size=4, palette=pal)

        print("Density:", Density_Average)
        print("Average:",np.mean(Density_Average))
        print("Anisotropy:",Anisotropy_Average)
        print("rn: ",rn)
        print("rc: ",rc)
        
        v=1
        d_m=49/8
        G_eff=[]
        G=[]
        g=[]
        rn_temp=[]
        l_0=[]
        LenRn=len(rn)
        for j in range (LenRn):
            rn_temp.append(rn[j]/(4/3*math.pi*700**3))
            l_0.append((2*math.pi*v/rn_temp[j])**(1/4))
            g.append((-rc[j]/v))
            G.append(g[j]*l_0[j])
            G_eff.append(-G[j]*(d_m/l_0[j])**(-1/3))
    
        G_eff_short = [ '%.2f' % elem for elem in G_eff ]
 
        ## Plotting formating (commented lines can be changed for better format in other case) 
        plt.ylabel('Total Microtubule Segments', fontsize=24)
        ax.set_xticklabels(G_eff_short, fontsize=24)
        plt.xlim([-1,8])
        #plt.xlim([-1,10])
        ax.set_yticks([0,10000,20000,30000])
        ax.set_yticklabels([0,10000,20000,30000], fontsize=24)
        plt.xlabel(r"$G_{\rm{eff}}$", fontsize=24)
        #plt.ylim([0,MaxY])
        plt.ylim([0,30000])

    
        plt.show()
     
    

    


if __name__ == '__main__':
    main()
