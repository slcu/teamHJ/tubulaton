#!/usr/bin/python
# -*- coding: utf-8 -*- 
# NOTE: This code is written in Python 3. 
import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
import math
from mayavi import mlab
import matplotlib.pyplot as plt
import seaborn as sns


def main():
    """Plot multiple lines of convergence on sam graph
    """
    #Real in variables
    FileLocation='../../../Output/PythonGeneratedOutput/AnisotropyTextFiles/Fig7_NoZip/'
    #FileLocation='../../../Output/PythonGeneratedOutput/AnisotropyTextFiles/'
    
    ### PUT THE LIST OF OUTPUT FILES IN THIS VARIABLE  ####
    FileAll=['DensitySphere_Single_2023031310371678703851.txt',
    'DensitySphere_Single_2023031311511678708316.txt',
    'DensitySphere_Single_2023031312551678712141.txt',
    'DensitySphere_Single_2023031313531678715619.txt',
    'DensitySphere_Single_2023031314391678718353.txt',
    'DensitySphere_Single_2023031315161678720594.txt',
    'DensitySphere_SingleA_2023031415441678808659.txt']  #Update these lines to reflect names of output from DensityData_Single #Smaller density

    #FileAll=['DensitySphere_Single_2023031310571678705062.txt',
    #'DensitySphere_Single_2023031313351678714508.txt',
    #'DensitySphere_Single_2023031315471678722462.txt', 
    #'DensitySphere_Single_2023031317421678729362.txt',
    #'DensitySphere_Single_2023031418111678817497.txt',
    #'DensitySphere_Single_2023031416081678810125.txt',
    #'DensitySphere_Single_2023031517121678900376.txt']  #Update these lines to reflect names of output from DensityData_Single #Large density

    FileAll=['Fig7_NoZip_Single_2023031621231679001815.txt',
    'Fig7_NoZip_Single_2023031620511678999919.txt',
    'Fig7_NoZip_Single_2023031620221678998177.txt',
    'Fig7_NoZip_Single_2023031619561678996597.txt',
    'Fig7_NoZip_Single_2023031619311678995066.txt',
    'Fig7_NoZip_Single_2023031619081678993734.txt',
    'Fig7_NoZip_Single_2023032115271679412461.txt',
    'Fig7_NoZip_Single_2023032213541679493293.txt',
    'Fig7_NoZip_Single_2023032215101679497818.txt',
    'Fig7_NoZip__2023032716021679929334.txt'] #NoZip
    #'Fig7_NoZip_Single_2023032115271679412461.txt',
    #'Fig7_NoZip_Single_2023032115561679414207.txt',
    #'Fig7_NoZip_Single_2023032116181679415494.txt',
    #'Fig7_NoZip_Single_2023031617091678986560.txt',
    #'Fig7_NoZip_Single_2023031616561678985772.txt',
    #'Fig7_NoZip_Single_2023031618061678989960.txt',
    #'Fig7_NoZip_Single_2023031618431678992212.txt',
    #'Fig7_NoZip_Single_2023032210441679481872.txt',
    #'Fig7_NoZip_Single_2023032214161679494564.txt',
    
    #FileAll=['Fig7_Zip_Single_2023031621021679000545.txt',
    #'Fig7_Zip_Single_2023031620311678998699.txt',
    #'Fig7_Zip_Single_2023031620021678996932.txt',
    #'Fig7_Zip_Single_2023031619351678995349.txt',
    #'Fig7_Zip_Single_2023031619101678993857.txt',
    #'Fig7_Zip_Single_2023031618491678992595.txt',
    #'Fig7_Zip_Single_2023031618331678991613.txt'] #Zip

    ##Density data for small sphere with origional density values
    #FileAll=['Density_SmallSphere_Single_2023032917081680106099.txt',
    #'Density_SmallSphere_Single_2023032918131680109981.txt',
    #'Density_SmallSphere_Single_2023032919101680113413.txt',
    #'Density_SmallSphere_Single_2023032919401680115254.txt',
    #'Density_SmallSphere_Single_2023032920021680116561.txt',
    #'Density_SmallSphere_Single_2023032920331680118381.txt',
    #'Density_SmallSphere_Single_2023032921011680120063.txt']
    
    ##Small sphere VERY higher rn
    #FileAll=['DensitySphere_SingleA_2023033011381680172694.txt',
    #'DensitySphere_SingleA_2023033019481680202129.txt',
    #'DensitySphere_SingleA_2023033101411680223286.txt',
    #'DensitySphere_SingleA_2023040111261680344803.txt',
    #'DensitySphere_SingleA_2023040117541680368099.txt',
    #'DensitySphere_SingleA_2023040205261680409614.txt']
    #'DensitySphere_SingleA_2023033111311680258667.txt', 0.0015 origional
    #'DensitySphere_SingleA_2023033107231680243830.txt', 0.002 origional
    #'DensitySphere_SingleA_2023033117221680279756.txt',
    
    #Fig7 values
    #FileAll=['Fig7_NoZip_SphereSmall_2023033023311680215485.txt',
    #'Fig7_NoZip_SphereSmall_2023033023261680215197.txt',
    #'Fig7_NoZip_SphereSmall_2023033023211680214914.txt',
    #'Fig7_NoZip_SphereSmall_2023033023171680214634.txt',
    #'Fig7_NoZip_SphereSmall_2023033023121680214345.txt',
    #'Fig7_NoZip_SphereSmall_2023033023041680213854.txt',
    #'Fig7_NoZip_SphereSmall_2023033021351680208514.txt',
    #'Fig7_NoZip_SphereSmall_2023033015311680186694.txt',
    #'Fig7_NoZip_SphereSmall_2023032917161680106619.txt',
    #'Fig7_NoZip_SphereSmall_2023033001311680136263.txt']  ##problem at two lower densities but otherwise OK. 

    ######## Fig 7 values again (Small sphere) full range of values
    #FileAll=['NoZip_Den_2023042018161682011009.txt',
    #'NoZip_Den_2023042018161682011013.txt',
    #'NoZip_Den_2023042018161682011018.txt',
    #'NoZip_Den_2023042018171682011025.txt',
    #'NoZip_Den_2023042018171682011033.txt',
    #'NoZip_Den_2023042018171682011036.txt',
    #'NoZip_Den_2023042018181682011084.txt',
    #'NoZip_Den_2023042018171682011046.txt',
    #'NoZip_Den_2023042018171682011043.txt',
    #'NoZip_Den_2023042312061682248007.txt']
    #'NoZip_Den_2023042222261682198773.txt',, 0.0006 but only 20000 time steps

    #Fig 7 values (Small sphere)
    #FileAll=[
    #'Zip_Den_2023042213221682166159.txt',
    #'Zip_Den_2023042213221682166155.txt',
    #'Zip_Den_2023042213221682166152.txt',
    #'Zip_Den_2023042213221682166149.txt',
    #'Zip_Den_2023042213221682166146.txt',
    #'Zip_Den_2023042213221682166143.txt',
    #'Zip_Den_2023042016271682004439.txt',
    #'Zip_Den_2023042016271682004441.txt',
    #'Zip_Den_2023042016271682004444.txt',
    #'Zip_Den_2023042016271682004447.txt',]
    #0.00006, 0.0000125, 0.000025, 0.00005

    reps=1 #Number of repitions
    rc=[]

    fig, ax = plt.subplots()

    for File in FileAll:
        File=FileLocation+File
        f=open(File,"r")
        lines=f.readlines()

        Count=0

        Summary=[]
        Scalar=[]
        Time=[]
        Count=[]
        Avord=0
        CountLoop=0
        for x in lines:
            x=x.split(";")
            if CountLoop==0:
               rc.append(float(x[9]))
            Summary.append([float(x[12])])
            Scalar.append([abs(float(x[13]))])
            Time.append([float(x[6])])
            Count.append(float(x[13]))
            CountLoop=CountLoop+1

        Count=np.array(Count)
        Time=np.array(Time)
        t=int(len(Count)/reps)
        data = np.zeros((t,reps))
        datah=np.zeros((t,reps))
        for i in range(t):
            for j in range(reps):
                i=int(i)
                j=int(j)
                a=int(j*t+i)
                data[i,j]=Count[a]
                datah[i,j]=Time[a]


        hello=plt.plot(datah[0:t,0:5],data[0:t,0:5],label=r'$r_c=$ %.6f'  %(rc[-1]))
    #plt.xlim([0,20000])
    #plt.ylim([0,25000])
    plt.xticks([0,10000,20000,30000], fontsize=18)
    plt.yticks([0,10000,20000,30000,40000], fontsize=18)
    plt.xlabel('Time', fontsize=18)
    plt.ylabel('Total Microtubule Segments', fontsize=18)
    plt.xlim([0,30000])
    plt.ylim([0,40000])
    plt.legend(fontsize=10)
    plt.show()


if __name__ == '__main__':
    main()
