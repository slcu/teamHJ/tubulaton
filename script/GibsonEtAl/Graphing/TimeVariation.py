#!/usr/bin/python
# -*- coding: utf-8 -*- 
# NOTE: This code is written in Python 3. 
import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
import math
from mayavi import mlab
import matplotlib.pyplot as plt
import seaborn as sns


def main():
    """
    """
    #Real in variables
    FileLocation='../../../Output/PythonGeneratedOutput/AnisotropyTextFiles/'
    #FileLocation='home/tamsin/home3/Active/tubulaton/Output/PythonGeneratedOutput/Data/VTK_CameronOutput' #location of some of the data
    #File='Fig7_NoZip_cube_2023030921401678398024.txt'   #Change this line to desired file. 
    #File='Fig7_NoZip_2023030923541678406062.txt'
    #File='Fig7_NoZip_2023030922431678401790.txt'
    #File='DensityCube_Big_2023031010101678443048.txt'
    #File='Fig7_Zip_2023031015021678460553.txt'
    #File='DensitySphere_Single_2023031311511678708316.txt'
    #File='DensitySphere_SingleA_2023031310571678705065.txt'
    #File='DensitySphere_Single_2023031310371678703851.txt' #0.0035, 1.8
    #File='DensitySphere_Single_2023031311511678708316.txt' #0.003, 1.25
    #File='DensitySphere_Single_2023031310571678705062.txt' #0.0035, 3.6
    #File='DensitySphere_Single_2023031312551678712141.txt' #0.0025, 0.9
    #File='DensitySphere_Single_2023031313531678715619.txt'  #0.002, 0.55
    #File='DensitySphere_Single_2023031314391678718353.txt' #0.0015, 0.32
    #File='DensitySphere_Single_2023031315161678720594.txt'  #0.001, 0.1
    #File='DensitySphere_Single_2023031313351678714508.txt' #0.003, 2.5
    #File='DensitySphere_Single_2023031315351678721721.txt' #0.0005, 0.044 
    #File='DensitySphere_Single_2023031315471678722462.txt' #0.0025, 1.8
    #File='DensitySphere_Single_2023031317421678729362.txt' #0.002, 1.1
    #File='DensitySphere_Single_2023031319131678734815.txt' #0.0015, 0.64
    #File='DensitySphere_SingleA_2023031310571678705065.txt' #0.0035, 1.8
    #File='DensitySphere_SingleA_2023031312121678709574.txt' #0.003, 1.25
    #File='DensitySphere_SingleA_2023031314151678716910.txt' #0.002, 0.55
    #File='DensitySphere_SingleA_2023031315011678719694.txt' #0.0015, 0.32
    #File='DensitySphere_SingleA_2023031315391678721987.txt' #0.001, 0.15
    #File='DensitySphere_SingleA_2023031316081678723718.txt' #0.0005, 0.044
    #File='DensitySphere_SingleA_2023031415441678808659.txt'
    #File='DensitySphere_Single_2023031415441678808655.txt' #0.005, 0.088
    File='DensitySphere_Single_2023031418111678817497.txt'  #0.0015, 0.64
    File='DensitySphere_Single_2023031416081678810125.txt' #0.001, 0.3
    File='DensitySphere_SingleA_2023031416261678811187.txt' #0.001, 0.15 
    File='DensitySphere_SingleA_2023031415441678808659.txt' #0.0005, 0.044
    File='DensitySphere_Single_2023031415441678808655.txt'
    File='DensitySphere_Single_2023031515021678892575.txt'
    File='DensitySphere_Single_2023031517121678900376.txt' #0.005, 0.088
    File='Fig7_Zip_Single_2023031717401679074804.txt'
    File='Fig7_NoZip_Single_2023032112161679400971.txt'
    File='Fig7_NoZip_Single_2023032112231679401431.txt'
    File='Fig7_NoZip_Single_2023032112501679403007.txt'
    File='Fig7_NoZip_Single_2023032114491679410180.txt'
    File='Fig7_NoZip_Single_2023032115041679411070.txt'
    File='Fig7_NoZip_Single_2023032115181679411895.txt'
    File='Fig7_NoZip_Single_2023032115271679412461.txt' #0.00125, 0.002
    #File='Fig7_NoZip_Single_2023032115561679414207.txt'
    #File='Fig7_NoZip_Single_2023032116181679415494.txt'
    #File='Fig7_NoZip_Single_2023032116441679417055.txt'
    #File='Fig7_NoZip_Single_2023032117291679419743.txt'
    #File='Fig7_NoZip_Single_2023032118331679423615.txt'
    File='Fig7_NoZip_Single_2023032210441679481872.txt' #0.0025, 0.001
    File='Fig7_NoZip_Single_2023032214161679494564.txt' #0.0005, 0.03
    File='Fig7_NoZip_Single_2023032213541679493293.txt' #0.00025, 0.008
    File='Fig7_NoZip_Single_2023032215101679497818.txt'
    File='Fig7_NoZip__2023032716021679929334.txt'
    #File='Fig7_NoZip__2023032716181679930325.txt'
    #File='DensitySphere_SingleA_2023032715531679928819.txt'
    File='DensitySphere_SingleA_2023032717321679934741.txt'
    File='DensitySphere_SingleA_2023032715531679928819.txt'
    File='Density_SmallSphere_Single_2023032917081680106099.txt'
    File='Density_SmallSphere_Single_2023032918131680109981.txt'
    File='Density_SmallSphere_Single_2023032919101680113413.txt'
    File='Fig7_NoZip_SphereSmall_2023032917161680106619.txt'

    #File='NoZip_Den_2023041917461681922764.txt'  #0.0005 0.042
    #File='NoZip_Den_2023041917451681922758.txt'  #0.000125 0.018
    File='NoZip_Den_2023041917461681922761.txt'   #0.00006 0.0145

    reps=1 #Number of repitions

    File=FileLocation+File
    f=open(File,"r")
    lines=f.readlines()

    Count=0

    val_aniso_1=[]
    val_aniso_2=[]
    val_aniso_3=[]
    Summary=[]
    Scalar=[]
    Time=[]
    repeat=[]
    Count=[]
    Avord=0
    for x in lines:
        x=x.split(";")
        repeat.append([float(x[4])])
        val_aniso_1.append([float(x[0])])
        val_aniso_2.append([float(x[1])])
        val_aniso_3.append([float(x[2])])
        Summary.append([float(x[12])])
        Scalar.append([abs(float(x[13]))])
        Time.append([float(x[6])])
        Count.append(float(x[13]))

    Count=np.array(Count)
    Time=np.array(Time)
    t=int(len(Count)/reps)
    data = np.zeros((t,reps))
    datah=np.zeros((t,reps))
    for i in range(t):
        for j in range(reps):
            i=int(i)
            j=int(j)
            a=int(j*t+i)
            data[i,j]=Count[a]
            datah[i,j]=Time[a]

    fig, ax = plt.subplots()
    plt.figure(1)
    hello=plt.plot(datah[0:t,0:5],data[0:t,0:5])
    plt.xlabel('Time')
    plt.ylabel('Total Microtubule Segments')
    plt.show()


if __name__ == '__main__':
    main()
