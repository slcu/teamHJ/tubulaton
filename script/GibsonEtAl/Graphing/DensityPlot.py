#!/usr/bin/python
# -*- coding: utf-8 -*- 
# NOTE: This code is written in Python 3. 
import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
import math
from mayavi import mlab
import matplotlib.pyplot as plt
import seaborn as sns


def Inputs():
    """ Output: Two lists Inp2 and Inp of file names output by tubulaton
    
    In the function ADD the names of the text files that density data should be read in for. There is one density file per (rc, rn) pair
    Inp and Inp2 are two Python lists (see examples below). Each list should contain all the files in order corresponding to the
    lower density first and the higher density. The boxplot will plot in the order of the files so it is advised to go from lowest to 
    highest rc value. x-labels of rc values are plotted based in Inp rc values so it is recommened to have the same rc values for both 
    sets of data. """
    
    


    # Fig 7 no-zip and zip, small sphere
    #Inp=['Zip_Den_2023041909491681894158.txt',
    #'Zip_Den_2023041909491681894167.txt',
    #'Zip_Den_2023041822301681853426.txt',
    #'Zip_Den_2023041822301681853424.txt',
    #'Zip_Den_2023041822301681853420.txt',
    #'Zip_Den_2023041822301681853416.txt',
    #'Zip_Den_2023041822301681853413.txt',
    #'Zip_Den_2023041822301681853410.txt']
    ##'Zip_Den_2023041909491681894163.txt', #0.00006
    ##'Zip_Den_2023041909491681894161.txt', #0.0000125
    
    #Inp2=['NoZip_Den_2023041909371681893441.txt',
    #'Density_SmallSphere_Den_2023041111361681209374.txt',
    #'Density_SmallSphere_Den_2023041111361681209372.txt',
    #'Density_SmallSphere_Den_2023041111361681209367.txt',
    #'Density_SmallSphere_Den_2023041111361681209364.txt',
    #'Density_SmallSphere_Den_2023041111361681209362.txt',
    #'Density_SmallSphere_Den_2023041111351681209359.txt',
    #'Density_SmallSphere_Den_2023041111351681209357.txt']   
    ##'NoZip_Den_2023041909371681893447.txt', #0.00006
    ##'NoZip_Den_2023041909371681893444.txt', #0.0000125

    ##############################################################################
    #Fig 7 no-zip and zip, large sphere
    #Inp=['Fig7_NoZip__2023032616471679845660.txt',
    #'Fig7_NoZip__2023032605081679803711.txt',
    #'Fig7_NoZip__2023032517241679765059.txt',
    #'Fig7_NoZip__2023032507481679730482.txt',
    #'Fig7_NoZip__2023032423081679699289.txt',
    #'Fig7_NoZip__2023032416031679673796.txt',
    #'Fig7_NoZip__2023032413001679662853.txt',
    #'Fig7_NoZip__2023032410451679654757.txt',
    #'Fig7_NoZip__2023032409471679651242.txt']

    #Inp2=['Fig7_Zip__2023032616321679844751.txt',
    #'Fig7_Zip__2023032604531679802813.txt',
    #'Fig7_Zip__2023032517121679764343.txt',
    #'Fig7_Zip__2023032507331679729586.txt',
    #'Fig7_Zip__2023032423011679698885.txt',
    #'Fig7_Zip__2023032415571679673464.txt',
    #'Fig7_Zip__2023032412551679662512.txt',
    #'Fig7_Zip__2023032410431679654626.txt',
    #'Fig7_Zip__2023032409471679651238.txt']
 
    #Large sphere, low and high density
    
    #Inp2=['Density_b_2023031517101678900240.txt',
    #'Density_b_2023031607061678950396.txt',
    #'Density_b_2023031708511679043075.txt',
    #'Density_b_2023031821081679173718.txt',
    #'Density_b_2023032019131679339622.txt',
    #'Density_b_2023032306431679553819.txt',
    #'Density_b1_2023032013441679319872.txt']
       
    #Inp=['Density_2023031517101678900205.txt',
    #'Density_2023031523581678924711.txt',
    #'Density_2023031611581678967928.txt',
    #'Density_2023031705241679030693.txt',
    #'Density_2023031803231679109809.txt',
    #'Density_2023031907451679211913.txt',
    #'Density_2023032015451679327147.txt']
    
    ##################################################################################
    
    ######Small Sphere, low and high density 
    
    #Higher density - origional values (need adapting for smaller sphere)
    #will keep the last one and replace the others with new simulations currently being run
    Inp=['NoZip_2023042809541682672068.txt',
    'Density_SmallSphere_Den_2023041211081681294107.txt',
    'Density_SmallSphere_Den_2023041311511681383119.txt',
    'Density_SmallSphere_Den_2023041311521681383125.txt',
    'Density_SmallSphere_Den_2023041412371681472256.txt',
    'Density_SmallSphere_Den_2023041118291681234189.txt',
    'Density_SmallSphere_Den_2023040611151680776116.txt',
    'Density_SmallSphere_Den_2023041809381681807110.txt']
    #'NoZip_2023042714071682600820.txt',
    #'NoZip_2023042615211682518888.txt',
    #'NoZip_2023042714071682600837.txt',
    #'NoZip_2023042714071682600852.txt',
    #Low density -no zippering Cameron values
    Inp2=['NoZip_Den_2023041909371681893441.txt',
    'Density_SmallSphere_Den_2023041111361681209374.txt',
    'Density_SmallSphere_Den_2023041111361681209372.txt',
    'Density_SmallSphere_Den_2023041111361681209367.txt',
    'Density_SmallSphere_Den_2023041111361681209364.txt',
    'Density_SmallSphere_Den_2023041111361681209362.txt',
    'Density_SmallSphere_Den_2023041111351681209359.txt',
    'Density_SmallSphere_Den_2023041111351681209357.txt',]
    
    ###########################################################
    ##### Small Sphere Severing
    #Severing varying density
    #Inp=[
    #'Sev_Den_2023042222241682198671.txt',
    #'Sev_Den_2023042222241682198675.txt',
    #'Sev_Den_2023042222241682198655.txt',
    #'Sev_Den_2023042222241682198657.txt',
    #'Sev_Den_2023042222241682198659.txt',
    #'Sev_Den_2023042222241682198660.txt',
    #'Sev_Den_2023042222241682198662.txt',
    #'Sev_Den_2023042222241682198666.txt']
    
    #Inp2=['Sev_ConDen_2023042312231682249029.txt',
    #'Sev_ConDen_2023042312231682249030.txt',
    #'Sev_ConDen_2023042312231682249019.txt',
    #'Sev_ConDen_2023042312231682249020.txt',
    #'Sev_ConDen_2023042312231682249022.txt',
    #'Sev_ConDen_2023042312231682249023.txt',
    #'Sev_ConDen_2023042312231682249025.txt',
    #'Sev_ConDen_2023042312231682249027.txt',]
    
    InpAll=[Inp2,Inp]

    return(InpAll)


def main():
    """Plot the density of microtubules from two sets of data Inp and Inp2 as a boxplot
    """
    #Real in variables
    FileLocation='../../../Output/PythonGeneratedOutput/AnisotropyTextFiles/'  #Location of text files
    Inp_All=Inputs()  #Text files

    fig, ax = plt.subplots()

    #For loop over all lines in each text file to extract density, rc and rn
    Count_Dataset=0
    for Inp in Inp_All:
        Density=[[] for _ in Inp]
        Density_Average=[]
        Anisotropy=[[] for _ in Inp]
        Anisotropy_Average=[]
        rc=[]
        rn=[]
        for i,j in enumerate(Inp):
            File=FileLocation+j
            f=open(File,"r")
            lines=f.readlines()

            Count=0
            for x in lines:
                x=x.split(";")
                if Count==0:         
                    rc.append(round(float(x[9])*10**3,5))
                    rn.append(float(x[10]))
                    Count=Count+1
                if abs(float(x[6])-10000)<0.00001:
                    Density[i].append(float(x[13]))
                    Anisotropy[i].append(abs(float(x[14])))

            Density_Average.append(np.mean(Density[i]))
            Anisotropy_Average.append(np.mean(Anisotropy[i]))
        sublist=[max(i) for i in Density]
        MaxY=max(sublist)+1000
 
        #Boxplot
        sns.set_theme(style='whitegrid')
        if Count_Dataset==0:
            col2=(0.98, 0.5, 0.5)
            col="red"
            Count_Dataset=Count_Dataset+1
        else:
            col2=(0.6, 1, 1)
            col="blue"
        pal2 = {col2 for _ in Density}
        pal = {col for _ in Density}
        hello=sns.boxenplot(Density,showfliers=False , palette=pal2)
        hello=sns.stripplot(Density, size=4, palette=pal)

        print("Density:", Density_Average)
        print("Average:",np.mean(Density_Average))
        print("Anisotropy:",Anisotropy_Average)
        print("rn: ",rn)
        print("rc: ",rc)
 
    ## Plotting formating (commented lines can be changed for better format in other case) 
    plt.ylabel('Total Microtubule Segments', fontsize=24)
    ax.set_xticklabels(rc, fontsize=24)
    plt.xlim([-1,8])
    #plt.xlim([-1,10])
    ax.set_yticks([0,20000,40000,60000])
    ax.set_yticklabels([0,20000,40000,60000], fontsize=24)
    plt.xlabel(r"$r_{c}$", fontsize=24)
    plt.ylim([0,MaxY])
    ax.set_yticks([0,10000,20000,30000])
    ax.set_yticklabels([0,10000,20000,30000], fontsize=24)
    #plt.ylim([0,40000]) 
    
    plt.show()
    

    


if __name__ == '__main__':
    main()
