#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
import math
from mayavi import mlab
#import matplotlib.pyplot as plt

def lecture(rep,nom):
    """Read in data from the .vtk file output from the microtubule simulations  
       Inputs: nom is the name of the file. rep is the file location, if none provided assume in current directory
    Output: p the read in data   """
    r=tvtk.PolyDataReader()
    r.file_name=rep+nom
    #print (r.file_name)
    r.update()
    p=r.output
    return p

def extraction(p,verbose=0):
    """
	This function extracts data from p. It creates a numpy array with the following data in each position:
    0, 1, 2, 3 , 4,  5,  6,        7
    X, Y, Z, VX, VY, VZ, identite, type
    
    c'est ici qu'il faut que je rajoute la coque si elle n'y est pas spontanément.
    """
    x=np.array([X[0] for X in p.points])
    y=np.array([X[1] for X in p.points])
    z=np.array([X[2] for X in p.points])
    vx=np.array([X[0] for X in p.point_data.get_array(0)])
    vy=np.array([X[1] for X in p.point_data.get_array(0)])
    vz=np.array([X[2] for X in p.point_data.get_array(0)])
    identite=np.array([i for i in p.point_data.get_array(1)])
    #if verbose:print(p.point_data.get_array_name(1))
    typ=np.array([i for i in p.point_data.get_array(2)])
    d=np.array((x,y,z,vx,vy,vz,identite,typ)).T
    return d

def filtreBundle(d):
    """
    This function aims to remove microtubules which have bundled so that bundles are only counted once. It identifies the direction of each microtubule and any which are heading in the same direction to five decimal places are assumed to be bundled. Microtubules going in the opposite direction with the same gradient are then updated to have the same key with repeats removed. Bundles are then averaged over to give one entry. Note: microtubules will be counted as bundled if they are going in the same direction so need small variation in microtubule direction otherwise each microtubule/bundle may be only counted once. 
    Inputs: d data on the microtubules
    Outputs: df data on the microtubules with bundles removed and without the identite and type fields (position and velocity only). 
    """
    toto={}
    tata={}
    for r in d:
        toto.setdefault(tuple(np.round(r[3:6],10)),[]).append(r)
        tata.setdefault(tuple(-np.round(r[3:6],10)),[]).append(r)

    Repeats=[]
    Repeats_other=[]
    #add bundle elements going in the opposite direction
    for i,k in enumerate(toto.keys()):
        if k in tata:
            toto[k].extend(tata[k]) # microtubules can be moving either way along a bundle
            if not(k in Repeats):
                k_tem=list(k)
                k_tem = [ -x for x in k_tem]
                k_tem=tuple(k_tem)
                if not(k_tem in Repeats):
                    Repeats.append(k_tem)
            elif not (k in Repeats_other):
                Repeats_other.append(k)
    if (not(len(set(Repeats_other).intersection(set(Repeats)))==len(set(Repeats)))):
        raise ValueError("Error in filtreBundle - Repeats and Repeats_other should be the same size")      
    #Remove bundled elements counted twice  
    for i in Repeats:
        del toto[i]
    #Take bundle posititon as the mean of all microtubules in that bundle but the velocities come from the key of the microtubule   
    Microtubules=[]
    average_matrix=[]
    avmat=[]
    Velocities=[]
    for i in toto:
        average_matrix=(np.mean(toto[i],axis=0))
        Velocities=list(i)
        temp1=np.array([average_matrix[0],average_matrix[1],average_matrix[2],Velocities[0],Velocities[1],Velocities[2]])
        Microtubules.append(temp1)

        #Check to see how far apart the bundled tubulin is. If too far then print a warning message. May indicate bundled microtubules are being underrepresented.         
        average_matrix=np.array(toto[i])
        origsize=np.size(average_matrix)
        average_matrix=average_matrix[:,0:3]
        average_matrix=average_matrix-np.mean(average_matrix,axis=0)
        average_matrix=np.linalg.norm(average_matrix,axis=1)
        if not(np.max(average_matrix)<10):
            print("Warning: Bundle filtering may be removing too much tubulin. Max_average_matrix=",np.max(average_matrix), "matsiz=", np.size(average_matrix), "OrigmatSize=",origsize )
    df=np.array(Microtubules)
    return df
    
def FilterPosition(df):
    """ Filter microtubules so only including microtubules on one/two faces of the shape and not too close to the edge. Distances are determined relative to the most extreme position of microtubules in each dimension. Since we are dealing with cuboids in fact we are considering two sides: the two directly opposite each other.
    Inputs: df the microtubules data  
    Output: df Reduced list of microtubules"""
    All_X=[]
    All_Y=[]
    All_Z=[]
    for i in df:
        All_X.append(i[0])
        All_Y.append(i[1])
        All_Z.append(i[2])

    WallGap=170; #Assuming 8nm per space apprxoimately 1.3 micrometers on every side so stops microtubules going over the edges being included
    WallGap1=170# Only including microtubules close to the wall. Distance microtubules are allowed to be from the wall. 
    MaxX=np.max(All_X)-WallGap
    MinX=np.min(All_X)+WallGap
    MaxZ=np.max(All_Z)-WallGap
    MinZ=np.min(All_Z)+WallGap
    YBound1=np.max(All_Y)-WallGap1
    YBound2=np.min(All_Y)+WallGap1
    
    deletelist=[]
    #print(np.shape(df))
    for k,i in enumerate(df):
        if not(i[0]>MinX and i[0]<MaxX and i[2]<MaxZ and i[2]>MinZ and (i[1]>YBound1 or i[1]<YBound2)):
            deletelist.append(k)
    df=np.delete(df,deletelist,axis=0)
    return(df)

def AngleUpdateDirec(AnisoAngle):
    """Changes reference direction of the directional anisotropy calculation. Used if axes on the structure is not as desired. Currently rotates reference direction by 90 degrees so in -y direction"""
    AnisoAngle=AnisoAngle+math.pi/2
    if AnisoAngle>math.pi/2:
        AnisoAngle=AnisoAngle-math.pi
    return(AnisoAngle)

def AngleAnisotropy(df,Centre,Radius,Shape):
    """Calculate anisotropy as sqrt(cos(2*theta)^2+sin(2*theta)^2) where I have assumed all microtubule lenghths are the same size (which should be the case using this code). Theta is the angle direction vector creates with a reference direction. I use reference direction of x-axis (1,0,0). This is calculated in 2D so we assume all the microtubules are rougly in the x-z plane. FilterPosition has reduced the list of microtubules to those where this is approximatley true.
     Input: df filtered miccrotubule data
     Output: Anis the anisostropy on that side of the shape """

    Aniso1=0
    Aniso2=0
    Aniso3=0
    Sadd=np.matrix([[0,0,0],[0,0,0],[0,0,0]], dtype=float)
    S=np.matrix([[0,0,0],[0,0,0],[0,0,0]], dtype=float)
    Count=0
 
    MinBound=Centre-Radius
    MaxBound=Centre+Radius
    print(Centre)
    print(Radius)
    print(Shape)
    #Sadd=0
    #S=np.matrix([[0,0],[0,0]])
    #Sadd=np.matrix([[0,0],[0,0]])
    for i in df:
        #Norm2D=math.sqrt(i[3]**2+i[3]**2)
        Norm3D=math.sqrt(i[3]**2+i[4]**2+i[5]**2)
        RadPos=math.sqrt((i[0]-Centre)**2+(i[1]-Centre)**2+(i[2]-Centre)**2)
        if (math.isnan(Norm3D) or abs(Norm3D)<0.000000001) or (((i[0]<MinBound) or (i[1]<MinBound) or (i[2]<MinBound) or (i[0]>MaxBound) or (i[1]>MaxBound) or (i[2]>MaxBound)) and Shape=='Cube') or (Shape=='Sphere' and RadPos>Radius):
            pass
        else :
            Direc1=i[3]/Norm3D
            Direc2=i[4]/Norm3D
            Direc3=i[5]/Norm3D
            #S[0,0]=2*Direc1*Direc1-1
            #S[0,1]=2*Direc1*Direc2
            #S[1,0]=2*Direc2*Direc1
            #S[1,1]=2*Direc2*Direc2-1
            Direc3=i[5]/Norm3D
            S[0,0]=3/2*Direc1*Direc1-1/2
            S[1,1]=3/2*Direc2*Direc2-1/2
            S[2,2]=3/2*Direc3*Direc3-1/2
            S[0,1]=3/2*Direc1*Direc2
            S[0,2]=3/2*Direc1*Direc3
            S[1,2]=3/2*Direc2*Direc3
            S[1,0]=3/2*Direc2*Direc1
            S[2,0]=3/2*Direc3*Direc1
            S[2,1]=3/2*Direc3*Direc2
            #if abs(SinThet**2+CosThet**2-1)>0.0000001: 
                #print (abs(SinThet**2+CosThet**2-1))
                #raise ValueError("Error in AngleAnisotropy - Value should always be approximately 1")
            Sadd=Sadd+S
            Count=Count+1
    if Count==0:
        print("ERROR:No Microtubules")
        Summary=-1
        Aniso2=-1
        Aniso3=-1
        Scalar=-1
    else :
        Sadd=Sadd/Count
        L=np.linalg.eig(Sadd)
        Aniso1=(L[0][0])
        Summary=0
        Scalar=Aniso1
        Aniso2=L[0][1]
        Aniso3=L[0][2]
        if ((abs(Aniso1)+abs(Aniso2)+abs(Aniso3))<0.2):
            Summary=0
        else:
            Summary=1
    return Aniso1, Aniso2, Aniso3, Summary, Scalar, Count

def WriteAniso(Aniso1,Aniso2,Aniso3,FileName,OutputFile,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit,Summary,Scalar,Count):
    """Anisotropy data is stored in the text file AnisotropyOutputAll. """
    #print(OutputFile)
    #print(FileName)
    f2=open(OutputFile,"a+")   
    count=0; 
    f2.write(str(Aniso1))
    f2.write("; ")
    f2.write(str(Aniso2))
    f2.write("; ")
    f2.write(str(Aniso3))
    f2.write(";")
    f2.write(str(CutNum))
    f2.write(";")
    f2.write(str(Repitition))
    f2.write(";")
    f2.write(str(TotSteps))
    f2.write(";")
    f2.write(str(Vtk_num))
    f2.write("; ")
    f2.write(str(FileName))
    f2.write("; ")
    f2.write(str(Bund))
    f2.write("; ")
    f2.write(str(cat))
    f2.write("; ")
    f2.write(str(shrink))
    f2.write("; ")
    f2.write(str(NucInit))
    f2.write("; ")
    f2.write(str(Summary))
    f2.write("; ")
    f2.write(str(Count))
    f2.write("; ")
    f2.write(str(Scalar))
    f2.write("\n")
    
def main():
    """
    Reads in inputs provided on command line then calculates the anisotropy. 
    """
    #print (sys.argv)
    args = sys.argv[1:]
    FileLocation=args[0]
    VtkSteps=int(args[2])
    TotSteps=int(args[3])
    CutNum=float(args[4].replace("_","."))
    Repitition=int(args[5])
    #if len(sys.argv)==8:
    OutputFile=args[6]
    #OutputFile="/home/cameron/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/AnisotropyOutputAll.txt"
    if len(sys.argv)>=9:
        Bund=float(args[7].replace("_",".")) #The size of bundling used. 
    else:
        Bund=float('NaN');
    if len(sys.argv)>=10:
        cat=float(args[8].replace("_",".")) #Chance of an induced catastrophe. 
    else:
        cat=float('NaN');  
    if len(sys.argv)>=11:
        shrink=float(args[9].replace("_",".")) #Chance of an shrinking per section of microtubule. 
    else:
        shrink=float('NaN');   
    if len(sys.argv)>=12:
        NucInit=float(args[10].replace("_",".")) #Chance of an shrinking per section of microtubule. 
    else:
        NucInit=float('NaN');
    OutputFileCopy=args[11]
    Centre=float(args[12])  
    Radius=float(args[13])
    Shape=args[14]
  
    

    Vtk_num=0;
    Vtk_num=Vtk_num+VtkSteps

    OutputFileBund=OutputFile[:-4]+"_bundles.txt"
    while Vtk_num<=TotSteps:
        FullFileName=args[1]+str(Vtk_num)+".vtk"
        p=lecture(FileLocation,FullFileName)
        

	# CODE IF YOU WANT TO NOT BUNDLE FILTER THE DATA
        dprovi=extraction(p) #Extract data from vtk file
        #df=filtreBundle(dprovi) #Filtre the data so microtubule bundles only counted once
        #df=FilterPosition(dprovi) #Filter data for position of microtubules (so only include anisotropy on one face) 
        df=dprovi
        Aniso,Aniso2,Aniso3,Summary,Scalar, Count=AngleAnisotropy(df,Centre,Radius,Shape)
        WriteAniso(Aniso,Aniso2,Aniso3,FullFileName,OutputFile,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit,Summary,Scalar, Count) #Add anisotropy data to the text file
        WriteAniso(Aniso,Aniso2,Aniso3,FullFileName,OutputFileCopy,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit,Summary,Scalar, Count) #Copy anisotropy data for graphing

	# CODE IF YOU ALSO WANT TO BUNDLE FILTER THE DATA
        #dprovi=extraction(p) #Extract data from vtk file
        #df=filtreBundle(dprovi) #Filtre the data so microtubule bundles only counted once
        #df=FilterPosition(df) #Filter data for position of microtubules (so only include anisotropy on one face) 
        #Aniso,Aniso_direc=AngleAnisotropy(df)
        #WriteAniso(Aniso,Aniso_direc,FullFileName,OutputFileBund,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat) #Add anisotropy data to the text file
        Vtk_num=Vtk_num+VtkSteps

if __name__ == '__main__':
    main()

