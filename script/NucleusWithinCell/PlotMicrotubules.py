#!/usr/bin/python
# -*- coding: utf-8 -*-
import vtk
import math
import numpy as np
from tvtk.api import tvtk
import matplotlib.pyplot as plt
import os, sys
  
def lecture(rep,nom):
    """Read in data from the .vtk file output from the microtubule simulations. Copied from CalculateAnisotropy.
       Inputs: nom is the name of the file. rep is the file location, if none provided assume in current directory. 
    Output: p the read in data   """
    r=tvtk.PolyDataReader()
    r.file_name=rep+nom
    #print (r.file_name)
    r.update()
    p=r.output
    return p

def extraction(p,verbose=0):
    """Copied from CalculateAnisotropy
	This function extracts data from p. It creates a numpy array with the following data in each position:
    0, 1, 2, 3 , 4,  5,  6,        7
    X, Y, Z, VX, VY, VZ, identite, type
    
    c'est ici qu'il faut que je rajoute la coque si elle n'y est pas spontanément.
    """
    x=np.array([X[0] for X in p.points])
    y=np.array([X[1] for X in p.points])
    z=np.array([X[2] for X in p.points])
    vx=np.array([X[0] for X in p.point_data.get_array(0)])
    vy=np.array([X[1] for X in p.point_data.get_array(0)])
    vz=np.array([X[2] for X in p.point_data.get_array(0)])
    identite=np.array([i for i in p.point_data.get_array(1)])
    #if verbose:print(p.point_data.get_array_name(1))
    typ=np.array([i for i in p.point_data.get_array(2)])
    d=np.array((x,y,z,vx,vy,vz,identite,typ)).T
    return d

def CalculateDistribution(Reps, FileLocation, Date,LenBar,WidthBar,VTK_Num, File='',FileFormat=1):
        """ Calculates the distribution of MTs along the cell (bars and barsprop) and the proportion of those MTs which are right moving (BarsDirecX)"""
        #BarXLabel=[-(i+0.5) for i in range(400)] #to plot the opposite way around with end of root hair at other end. 
        TotalBars=math.ceil(LenBar/WidthBar);
        BarXLabel=[(WidthBar*(i+0.5)) for i in range(TotalBars)]
        Bars=[0]*TotalBars #Total of MTs in each section of a 1 micrometer region
        BarsDirecX=[0]*TotalBars #Similar to Bars but proportion of MTs in that region which are right moving
        BarsVeryX=[0]*TotalBars
        BarsVeryMX=[0]*TotalBars
        for k in range(Reps):

            #File format
            if FileFormat==1: #Standard format file names
                File1=File+str(k+1)+'_'+str(VTK_Num)+'.vtk'
            else: #Have exact File name here if not in the standard format
                File1='Test4_8300.vtk'

            FullFileName=FileLocation+File1
            p=lecture(FileLocation,File1)
            dprovi=extraction(p)
 
            All_X=[]
            for i in dprovi:
                Temp=i[0]*8/1000/WidthBar  #Positoin in Microtmeters divided by bar width to put it in the right bar
                Bars[math.floor(Temp)]=Bars[math.floor(Temp)]+1
                if i[3]>0:
                   BarsDirecX[math.floor(Temp)]=BarsDirecX[math.floor(Temp)]+1
                TempDirec=math.atan(np.sqrt(i[4]**2+i[5]**2)/i[3])
                if TempDirec>0:#0.785:
                    BarsVeryX[math.floor(Temp)]=BarsVeryX[math.floor(Temp)]+1
                if TempDirec<0:#-0.785:
                    BarsVeryMX[math.floor(Temp)]=BarsVeryMX[math.floor(Temp)]+1
                    
        for i in range(len(BarsDirecX)):
            if Bars[i]!=0:
                BarsDirecX[i]=BarsDirecX[i]/Bars[i]
                BarsVeryX[i]=BarsVeryX[i]/Bars[i]
                BarsVeryMX[i]=BarsVeryMX[i]/Bars[i]         
            

        Tot=np.sum(Bars)
        BarsProp=[i/Tot for i in Bars]
        return(BarsProp,BarXLabel,BarsDirecX,BarsVeryX,BarsVeryMX,Bars)

def CalcTipWidth(Bars,BarXLabel):
    """Calcualte the width of the tip region by the width of area  """
    Bars=np.array(Bars)
    Count=0
    for ele in Bars:
        if ele!=0:
            Count=Count+1
    AverSig=sum(Bars)/Count
    print(AverSig)
    RootEnd=247.5
    #RootEnd=BarXLabel[np.max(np.nonzero(Bars>(AverSig/2)))] #Position of tip assuming signal above 1/2 average sig mag
    LowSig=Bars<(AverSig*5/4)
    PosLowSig=[BarXLabel[m] for m in range(0,len(LowSig)) if LowSig[m]==True]
    RootSig=[RootEnd-m for m in PosLowSig]
    RootSig = [x for x in RootSig if x >= 0] #Assumes root tip to the right
    Width=min(RootSig)
    print("Width of tip region: ",Width)
    print("Position of root end: ",RootEnd)
    return(Width,RootEnd)

def IntensityBetweenNucMT(Bars,BarXLabel,Xpos):
    """Calculate mean intensity between the nucleus and the end of the root hair """
    RootEnd=np.max(np.nonzero(Bars)) #Root end measured as first nonzero entry (not true for all data). 
    between=[]
    for i,j in zip(BarXLabel,Bars):
        if i>(Xpos+20) and i<(BarXLabel[RootEnd]-20): #Average which is 20 micrometers from nuc and 20 from tip.
            between.append(j)
    bx=np.mean(between)
    print("Mean between: ", bx)

def PlotHistogram(BarXLabel,Bars,ylabel='MTs',Plotting=2,Xinit=22425,MovSpeed=0.3,IntBet=1):
    """Plot a histogram of the MT intensity data"""
    if Plotting<=1:
        fig,ax=plt.subplots()
        plt.bar(BarXLabel,Bars,width=1)
        plt.xlabel('Distance (micromemters)')
        plt.ylabel(ylabel)
        if Plotting==0: #If zero look at nucleus position. 
            Xpos=(Xinit+MovSpeed*10000)*8/1000; #Estimate of nucleus position
            plt.plot([Xpos,Xpos],[0,max(Bars)],'--r') #Pot nucleus position
            if IntBet==0:
                IntensityBetweenNucMT(Bars,BarXLabel,Xpos) #Calcaulte estimate of MT intensity between nucleus and Tip.
        plt.show()

def PlotBarsRightLeft(BarXLabel,BarsVeryX,Plotting=2):
    """Plot proportion of MTs which are right moving as bar chart"""
    if Plotting==1:
        fig2,ax2=plt.subplots()
        plt.bar(BarXLabel,BarsVeryX,width=5)
        plt.xlabel('Distance (micromemters)')
        plt.ylabel('Proportion of Right Moving MT segments')
        plt.show()

def PlotLinePropEachDirect(BarXLabel,BarsVeryX,BarsVeryMX,Plotting=2):  
    """Plot proportion of right and left moving MTs."""
    if Plotting==1:
        fig3,ax3=plt.subplots()
        plt.plot(BarXLabel,BarsVeryX,'-b',label='right')
        plt.plot(BarXLabel,BarsVeryMX,'-r',label='left')    
        plt.xlabel('Distance(micrometers)')
        plt.ylabel('Proportion of MTs')    
        plt.legend()
        plt.show()

def PlotOverVariables(Date,File,VTK_Num,LenBar,WidthBar,LenAll,FileLocation,Reps,TenList,ShrinkList):
    TenListNum=[math.log10(float(x.replace('_','.'))) for x in TenList]
    ShrinkListNum=[math.log10(float(x.replace('_','.'))) for x in ShrinkList]
    for Tr in TenList:
        WidRan=[]
        for sh in ShrinkList:
            File=str(Date)+'_Tread'+Tr+'_Shrink'+sh+'_repeat_'        
            [Bars,BarXLabel,BarsDirecX,BarsVeryX,BarsVeryMX,BarsTotals]=CalculateDistribution(Reps, FileLocation,Date,LenBar,WidthBar,VTK_Num,File,1)
            [Width,Tip]=CalcTipWidth(BarsTotals,BarXLabel)
            WidRan.append(Width)
        lab='Treadmill='+Tr
        plt.plot(ShrinkListNum,WidRan,'-+',label=lab)
    plt.legend()
    plt.xlabel('log10(Shrink Probability)')
    plt.ylabel('Rough estimate of Tip Peak Width')
    plt.show()

def main():
    """
    Calculates the distribution of microtubules in the x direction. 
    """

    #Xinit=22425;
    #MovSpeed=0.3;
    TenList=['0_001','0_0005']
    ShrinkList=['0_1','0_01','0_001','0_0001','0_00001']

    VTK_Num=10000;
    LenBar=400; #Micrometer total length of region MT density considered within 
    WidthBar=1; #Micrometer width of region considered
    Date='2021081912011629370910' #Date of data beign considered
    LenAll=[[1800,300,300]] #List of nuclei sizes considered   
    FileLocation='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/'+str(Date)+'/' #folder of data 
    Reps=1

    for ListElem in LenAll:
        #File=str(Date)+'_Sphere_Rad375_375_375_Xpos'+str(ListElem)+'_Mesh2000_repeat_' #Varying position
        #File=str(Date)+'_Sphere_Rad'+str(ListElem)+'_'+str(ListElem)+'_'+str(ListElem)+'_'+'Xpos9425_Mesh2000_repeat_'
        #File=str(Date)+'_Sphere_Rad'+str(ListElem[0])+'_'+str(ListElem[1])+'_'+str(ListElem[2])+'_'+'Xpos5425_Mesh2000_repeat_'
        #File=str(Date)+'_Aniso_CylinderCortical__Cut_Bund_Cat_repeat_'
        #File=str(Date)+'_Cylinder_10000_EqualNucleation_repeat_'
        File=str(Date)+'_Sphere_Rad1800_300_300_Xpos22425_Mesh2000_repeat_'        

        [Bars,BarXLabel,BarsDirecX,BarsVeryX,BarsVeryMX,BarsTotals]=CalculateDistribution(Reps, FileLocation,Date,LenBar,WidthBar,VTK_Num,File,1) #If 1 use repeat format otherwise you filename in function
    
        Bars=[i/Reps for i in Bars]
        [Width,Tip]=CalcTipWidth(BarsTotals,BarXLabel)

        PlotHistogram(BarXLabel,BarsTotals,'All MTs',1) #Plot histogram of total detections. if Plotting second input<=1
        PlotHistogram(BarXLabel,Bars,'Proportion of MTs',2) #Plot histogram of proportion of detections. if Plotting second input<=1
              
        PlotBarsRightLeft(BarXLabel,BarsVeryX,2) #Bar chart right moving MTs 
        PlotLinePropEachDirect(BarXLabel,BarsVeryX,BarsVeryMX,2) #Line chart left and right moving MTs

    #PlotOverVariables(Date,File,VTK_Num,LenBar,WidthBar,LenAll,FileLocation,Reps,TenList,ShrinkList)
    plt.show()
        
        
if __name__ == '__main__':
    main()
