#!/usr/bin/python
# -*- coding: utf-8 -*-
import vtk
import math
import numpy as np
from tvtk.api import tvtk
import matplotlib.pyplot as plt
import os, sys
import PlotMicrotubules as PM
  
def PlotHistogram():
    """Plot the histogram of the mean length and the average mean """
    #file='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2021010711461610020013/2021010711461610020013_Sphere_Rad600_300_300_Xpos22425_Mesh2000_lengths.txt'
    #file='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/MaudComparison/DifBound2_MaudComparison_Sphere_Rad600_300_300_Xpos22425_Mesh2000_lengths.txt' #direct comparison to Maud
    #file='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/MaudComparison_LargeBends/DifBound2_MaudComparison_SmallCat_Sphere_Rad600_300_300_Xpos22425_Mesh2000_lengths.txt' #Allow large bends
    #file='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/MaudComparison_LargeBends/DifBound2_MaudComparison_Sphere_Rad600_300_300_Xpos22425_Mesh2000_lengths.txt' #Allow large bends with a cat rate equal to spontaneous catastrophe
    file='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2021081109281628670498/2021081109281628670498_Sphere_Rad1800_300_300_Xpos22425_Mesh2000_lengths.txt'
    MeanLength=[]
    with open(file, "r") as OpenFile:
         for line in OpenFile:
             line=line.strip()
             ml=line.split(";")
             ml=ml[:-1]
             ml=[int(x) for x in ml]
             MeanLength.append(np.mean(ml))
    plt.hist(ml)
    #plt.xlabel('Time')
    plt.xlabel('MT length')
    plt.ylabel('Number of MT')
    #plt.plot(MeanLength)
    print(np.mean(MeanLength[50:]))
    #plt.savefig('Maud_MTLengthDistribution')
    plt.show()

def PlotTotalSegments():
    file='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2021082216121629645177/2021082216121629645177_Sphere_Rad1800_300_300_Xpos22425_Mesh2000_lengths.txt'
    FrameGap=100
    TotalLength=[]
    NumberOfMT=[]
    with open(file, "r") as OpenFile:
         for line in OpenFile:
             line=line.strip()
             ml=line.split(";")
             ml=ml[:-1]
             ml=[int(x) for x in ml]
             TotalLength.append(np.sum(ml))
             NumberOfMT.append(len(ml))
    x=[FrameGap*i for i in range(len(TotalLength))]
    fig1,ax1=plt.subplots()
    ax1.plot(x,TotalLength,'+-')
    ax1.set_xlabel('Simulation Step')
    ax1.set_ylabel('Total segments')
    fig2,ax2=plt.subplots()
    ax2.plot(x,NumberOfMT,'+-')
    ax2.set_xlabel('Simulation Step')
    ax2.set_ylabel('Number of Microtubules')
    plt.show()
    
def PlotNumMTs():
    file='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/2021082118321629567140/2021082118321629567140_Sphere_Rad1800_300_300_Xpos22425_Mesh2000.txt'
    NumMts=[]
    with open(file, "r") as OpenFile:
         for line in OpenFile:
             line=line.strip()
             ml=line.split(";")
             NumMts.append(float(ml[11])) 
    plt.plot(NumMts)
    plt.xlabel('New Step')
    plt.ylabel('Number of MTs')
    plt.show()

def PlotMaximum():
    """Maximum at the tip and across the whole root hair."""
    #Tip at 247
    FrameGap=1000 #Gap between frames to look at data
    date='2021082216121629645177' #Date identification of data
    MaxReg=[] #Maximum across the data set 
    MaxEnd=[] #Maximum in tip region
    for j in range(89):
        VTK_Num=j*FrameGap;
        fileTot='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/'+date+'/'+date+'_Sphere_Rad1800_300_300_Xpos22425_Mesh2000_repeat_' 
        #fileTot='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/Data/VTK/'+date+'/'+date+'_Tread0_001_Shrink0_0005_repeat_' 
        [_,_,_,_,_,BarsTotals]=PM.CalculateDistribution(1, fileTot, date,400,1,VTK_Num)
        #print([list((i, BarsTotals[i])) for i in range(len(BarsTotals))])
        MaxReg.append(max(BarsTotals))
        MaxEnd.append(max(BarsTotals[240:260])) #when using len 30000 cylinder
        #MaxEnd.append(max(BarsTotals[80:90])) #when using len 10000 cylinder
    x=[FrameGap*i for i in range(len(MaxReg))]
    fig1,ax1=plt.subplots()
    ax1.plot(x,MaxReg,'+-')
    ax1.plot(x,MaxEnd,'+-')
    ax1.legend(['Maximum','Tip Maximum'])
    ax1.set_xlabel('Simulation Step')
    ax1.set_ylabel('Maximum of Signal')
    plt.show()
    
def main():
    #PlotHistogram() #Plot histogram of microtubule lengths at final time step and simulation in the lsit
    #PlotNumMTs() #Plot number of MTs for increasing simulation steps
    #PlotTotalSegments() #lot total number of segments and number of MTs over simulation steps
    PlotMaximum() #Use histogram to find maximum detections in tip and across whole root hair
        
        
if __name__ == '__main__':
    main()
