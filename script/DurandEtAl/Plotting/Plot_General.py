#!/usr/bin/python
# -*- coding: utf-8 -*- 
# NOTE: This code is written in Python 3. 
import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
import math
from mayavi import mlab
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

# function for setting the colors of the box plots pairs
def setBoxColors(bp,num):
    #Set colours of the boxplots
    plt.setp(bp['boxes'][0], color='blue')
    plt.setp(bp['caps'][0], color='blue')
    plt.setp(bp['caps'][1], color='blue')
    plt.setp(bp['whiskers'][0], color='blue')
    plt.setp(bp['whiskers'][1], color='blue')
    plt.setp(bp['fliers'][0], color='blue')
    plt.setp(bp['medians'][0], color='blue')
    
    if num>=2:
        plt.setp(bp['boxes'][1], color='red')
        plt.setp(bp['caps'][2], color='red')
        plt.setp(bp['caps'][3], color='red')
        plt.setp(bp['whiskers'][2], color='red')
        plt.setp(bp['whiskers'][3], color='red')
        plt.setp(bp['fliers'][1], color='red')
        plt.setp(bp['medians'][1], color='red')
    
    if num>=3:
        plt.setp(bp['boxes'][2], color='green')
        plt.setp(bp['caps'][3], color='green')
        plt.setp(bp['caps'][4], color='green')
        plt.setp(bp['whiskers'][4], color='green')
        plt.setp(bp['whiskers'][5], color='green')
        plt.setp(bp['fliers'][2], color='green')
        plt.setp(bp['medians'][2], color='green')

    if num>4:
        plt.setp(bp['boxes'][3], color='yellow')
        plt.setp(bp['caps'][4], color='yellow')
        plt.setp(bp['caps'][5], color='yellow')
        plt.setp(bp['whiskers'][6], color='yellow')
        plt.setp(bp['whiskers'][7], color='yellow')
        plt.setp(bp['fliers'][3], color='yellow')
        plt.setp(bp['medians'][3], color='yellow')

def main():
    """Plot anisotropy and anisotropic direction from tubulaton CalculateAnisotropy output. Compare two data sets provided by Aniso1 and Aniso2 that needs to be listed on following lines.
    """
    #Read in variables
    #Aniso1='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/CuttingMultiRepetions_addition_2021031318201615659639_1.txt'
    #Aniso2='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/CuttingMultiRepetions_addition_2021031318211615659710_1.txt'

    Aniso1='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/CuttingMultiRepetions_addition_2021032118121616350375_1.txt'
    Aniso2='/home/tamsin/Active/Documents/tubulaton/Output/PythonGeneratedOutput/AnisotropyTextFiles/CuttingMultiRepetions_addition_2021032118131616350431_1.txt'
#------------------------- Graph Aniso1 --------------------------------------------------

    f=open(Aniso1,"r")
    lines=f.readlines()
    val_aniso1=[]
    val_anisodirec1=[]
    for x in lines:
        x=x.split(";")
        val_aniso1.append(float(x[0]))
        val_anisodirec1.append(float(x[1]))
    f=open(Aniso2,"r")
    lines=f.readlines()
    val_aniso2=[]
    val_anisodirec2=[]
    for x in lines:
        x=x.split(";")
        val_aniso2.append(float(x[0]))
        val_anisodirec2.append(float(x[1]))

    fig1,ax1=plt.subplots()
    ax1.boxplot([val_aniso1,val_aniso2])
    ax1.set_ylabel('Anisotropy')
    ax1.set_xticklabels(['perpendicular','parallel'])
 
    fig2,ax2=plt.subplots()
    val_anisodirec1.sort()    
    total_num=len(val_anisodirec1)
    k=1/total_num;
    for value in val_anisodirec1:
        if k<2/total_num-0.00001:
            ax2.plot(value,k,'+b',label='perpendicular')
        else :
            ax2.plot(value,k,'+b')
        k+=1/total_num
    val_anisodirec2.sort()    
    total_num=len(val_anisodirec2)
    k=1/total_num;
    for value in val_anisodirec2:
        if k<2/total_num-0.00001:
            ax2.plot(value,k,'+r',label='parallel')
        else :
            ax2.plot(value,k,'+r')
        k+=1/total_num
    ax2.legend()
    ax2.set_xlim([-1.5,1.5])
    ax2.set_xlabel(r"$\Theta_{S_2}$")
    ax2.set_ylabel('CDF')
    plt.show()

   
#------------------------- Graph for Rectangle -----------------------------

    FileLocation=args[1]
    f=open(FileLocation,"r")
    lines=f.readlines()

    num_bund=np.size(bund_list)
    num_cut=np.size(cut_list)
    plt.figure(1)
    ax = plt.axes()
    pos_box+=1
    for cut in cut_list:
        val_aniso=[]
        cat=0.001
        for x in lines:
            x=x.split(";")
            if int(x[5])==VTK:
                if ((float(x[2])==cut) and (float(x[7]) in bund_list) and (float(x[8])==cat)):
                    val_aniso.append(float(x[0]))
        bp=plt.boxplot(val_aniso, positions = [pos_box], widths = 0.6)
        pos_box+=1

        if cut==0.005:
            plt.figure(2)
            ax2=plt.axes()
            bp=plt.boxplot(val_aniso, positions = [3], widths = 0.6)

    #plt.figure(1)
    #ax = plt.axes()
    #Xlab=[str(cut) for cut in cut_list]
    #TicPoint=[(num_bund+1)/2+x*(num_bund+1) for x in range (0,num_cut)]
    #ax.set_xticklabels(Xlab)
    #ax.set_xticks(TicPoint)

#----------------------- Graph Labels ---------------------------
    plt.figure(1)
    ax = plt.axes()
    Xlab=["$P_{cross}$="+str(cut) for cut in cut_list]+['\n \n Square']+["$P_{cross}$="+str(cut) for cut in cut_list]+['\n \n Rectangle']
    TicPoint=[1,2,1.5,4,5,4.5]
    ax.set_xticklabels(Xlab)
    ax.set_xticks(TicPoint)
    ax.tick_params(axis=u'both', which=u'both',length=0)
    plt.ylabel('Anisotropy')
    plt.xlim([0,pos_box])
    plt.ylabel(r"$S_2$")

    plt.figure(2)
    ax2 = plt.axes()
    TicPoint=[1,3]
    ax2.set_xticklabels(['Square','Rectangle'])
    ax2.set_xticks(TicPoint)
    plt.xlim([0,4])
    plt.ylabel(r"$S_2$")

#------------------------ Save Graph --------------------------------
    FileLoc1=args[1].split("/")
    FileName1=FileLoc1[-1]
    FileLoc2=args[0].split("/")
    FileName2=FileLoc2[-1]
    Direc=FileLocation.replace(FileName1,'')
    Direc=Direc+"Figures/"
    FileName1=FileName1.replace(".txt",'')
    FileName2=FileName2.replace(".txt",'')

    plt.figure(1)
    ax = plt.axes()
    fname=Direc+"GraphPaper_Anisotropy_VTK"+str(VTK)+"__"+FileName1+"__"+FileName2+".png"
    print(fname)
    plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format=None, transparent=False, bbox_inches=None, pad_inches=0.1, frameon=None, metadata=None)

    plt.figure(2)
    ax2 = plt.axes()
    fname=Direc+"GraphPaper_WildTypeAnisotropy_VTK"+str(VTK)+"__"+FileName1+"__"+FileName2+".png"
    print(fname)
    plt.savefig(fname, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format=None, transparent=False, bbox_inches=None, pad_inches=0.1, frameon=None, metadata=None)
    plt.show()


if __name__ == '__main__':
    main()
