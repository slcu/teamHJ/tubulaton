#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
import math
from mayavi import mlab
#import matplotlib.pyplot as plt

def lecture(rep,nom):
    """Read in data from the .vtk file output from the microtubule simulations  
       Inputs: nom is the name of the file. rep is the file location, if none provided assume in current directory
    Output: p the read in data   """
    r=tvtk.PolyDataReader()
    r.file_name=rep+nom
    #print (r.file_name)
    r.update()
    p=r.output
    return p

def extraction(p,verbose=0):
    """
	This function extracts data from p. It creates a numpy array with the following data in each position:
    0, 1, 2, 3 , 4,  5,  6,        7
    X, Y, Z, VX, VY, VZ, identite, type
    
    c'est ici qu'il faut que je rajoute la coque si elle n'y est pas spontanément.
    """
    x=np.array([X[0] for X in p.points])
    y=np.array([X[1] for X in p.points])
    z=np.array([X[2] for X in p.points])
    vx=np.array([X[0] for X in p.point_data.get_array(0)])
    vy=np.array([X[1] for X in p.point_data.get_array(0)])
    vz=np.array([X[2] for X in p.point_data.get_array(0)])
    identite=np.array([i for i in p.point_data.get_array(1)])
    #if verbose:print(p.point_data.get_array_name(1))
    typ=np.array([i for i in p.point_data.get_array(2)])
    d=np.array((x,y,z,vx,vy,vz,identite,typ)).T
    return d

def filtreBundle(d):
    """
    This function aims to remove microtubules which have bundled so that bundles are only counted once. It identifies the direction of each microtubule and any which are heading in the same direction to five decimal places are assumed to be bundled. Microtubules going in the opposite direction with the same gradient are then updated to have the same key with repeats removed. Bundles are then averaged over to give one entry. Note: microtubules will be counted as bundled if they are going in the same direction so need small variation in microtubule direction otherwise each microtubule/bundle may be only counted once. 
    Inputs: d data on the microtubules
    Outputs: df data on the microtubules with bundles removed and without the identite and type fields (position and velocity only). 
    """
    toto={}
    tata={}
    for r in d:
        toto.setdefault(tuple(np.round(r[3:6],10)),[]).append(r)
        tata.setdefault(tuple(-np.round(r[3:6],10)),[]).append(r)

    Repeats=[]
    Repeats_other=[]
    #add bundle elements going in the opposite direction
    for i,k in enumerate(toto.keys()):
        if k in tata:
            toto[k].extend(tata[k]) # microtubules can be moving either way along a bundle
            if not(k in Repeats):
                k_tem=list(k)
                k_tem = [ -x for x in k_tem]
                k_tem=tuple(k_tem)
                if not(k_tem in Repeats):
                    Repeats.append(k_tem)
            elif not (k in Repeats_other):
                Repeats_other.append(k)
    if (not(len(set(Repeats_other).intersection(set(Repeats)))==len(set(Repeats)))):
        raise ValueError("Error in filtreBundle - Repeats and Repeats_other should be the same size")      
    #Remove bundled elements counted twice  
    for i in Repeats:
        del toto[i]
    #Take bundle posititon as the mean of all microtubules in that bundle but the velocities come from the key of the microtubule   
    Microtubules=[]
    average_matrix=[]
    avmat=[]
    Velocities=[]
    for i in toto:
        average_matrix=(np.mean(toto[i],axis=0))
        Velocities=list(i)
        temp1=np.array([average_matrix[0],average_matrix[1],average_matrix[2],Velocities[0],Velocities[1],Velocities[2]])
        Microtubules.append(temp1)

        #Check to see how far apart the bundled tubulin is. If too far then print a warning message. May indicate bundled microtubules are being underrepresented.         
        average_matrix=np.array(toto[i])
        origsize=np.size(average_matrix)
        average_matrix=average_matrix[:,0:3]
        average_matrix=average_matrix-np.mean(average_matrix,axis=0)
        average_matrix=np.linalg.norm(average_matrix,axis=1)
        if not(np.max(average_matrix)<10):
            print("Warning: Bundle filtering may be removing too much tubulin. Max_average_matrix=",np.max(average_matrix), "matsiz=", np.size(average_matrix), "OrigmatSize=",origsize )
    df=np.array(Microtubules)
    return df
    
def FilterPosition(df):
    """ Filter microtubules so only including microtubules on one/two faces of the shape and not too close to the edge. Distances are determined relative to the most extreme position of microtubules in each dimension. Since we are dealing with cuboids in fact we are considering two sides: the two directly opposite each other.
    Inputs: df the microtubules data  
    Output: df Reduced list of microtubules"""
    All_X=[]
    All_Y=[]
    All_Z=[]
    for i in df:
        All_X.append(i[0])
        All_Y.append(i[1])
        All_Z.append(i[2])

    WallGap=170; #Assuming 8nm per space apprxoimately 1.3 micrometers on every side so stops microtubules going over the edges being included
    WallGap1=170# Only including microtubules close to the wall. Distance microtubules are allowed to be from the wall. 
    MaxX=np.max(All_X)-WallGap
    MinX=np.min(All_X)+WallGap
    MaxZ=np.max(All_Z)-WallGap
    MinZ=np.min(All_Z)+WallGap
    YBound1=np.max(All_Y)-WallGap1
    YBound2=np.min(All_Y)+WallGap1
    
    deletelist=[]
    #print(np.shape(df))
    for k,i in enumerate(df):
        if not(i[0]>MinX and i[0]<MaxX and i[2]<MaxZ and i[2]>MinZ and (i[1]>YBound1 or i[1]<YBound2)):
            deletelist.append(k)
    df=np.delete(df,deletelist,axis=0)
    return(df)
    
def FilterPosition_NucleusClose(df):
    """ Filter microtubules so only including microtubules in the region around the nucleus which is manually prescribed.
    Inputs: df the microtubules data  
    Output: df Reduced list of microtubules"""

    WallGap=1400; #How close to the nucleus in the long direction
    WallGap1=3000 #How close to be to the nucleus 
    MaxX=5425+WallGap1
    MinX=5425-WallGap1
    MaxZ=1400+WallGap
    MinZ=1400-WallGap
    MinY=1400-WallGap
    MaxY=1400+WallGap
    
    deletelist=[]
    for k,i in enumerate(df):
        if not(i[0]>MinX and i[0]<MaxX and i[2]<MaxZ and i[2]>MinZ and (i[1]>MinY or i[1]<MaxY)):
            deletelist.append(k)
    df=np.delete(df,deletelist,axis=0)
    return(df)

def FilterPosition_Edges(df):
    """ Filter microtubules so only including microtubules near the cortex or more central. Prescribing values below determine which region to look at.
    Inputs: df the microtubules data  
    Output: df_mid df_end Reduced list of microtubules near nucleus and central or near nucleus and cortex"""

    WallGap=60000 #How close to the nucleus in the long direction - set large to lok everywhere
    WallGap1=0 #How close to be to the nucleus in Z
    WallGap2=312.5 #How close to be to the nucleus in Y 
    MaxX=17425+800+WallGap
    MinX=17425+800-WallGap
    MaxZ=800+2*625-WallGap1
    MinZ=800+WallGap1
    MinY=800+WallGap2
    MaxY=800+2*625-WallGap2
    
    deletelist_cortex=[]
    deletelist_mid=[]
    deletelist_nucaway=[]
    for k,i in enumerate(df):
        if (i[0]<MinX or i[0]>MaxX):
            deletelist_nucaway.append(k) #Away from the nucleus along cylinder
        elif (i[2]<MaxZ and i[2]>MinZ and i[1]>MinY and i[1]<MaxY):
            deletelist_mid.append(k) #Near the centre root hair 
        else : 
            deletelist_cortex.append(k)  #Near cortex of root hair

    df_end=np.delete(df,deletelist_mid,axis=0)
    df_mid=np.delete(df,deletelist_cortex,axis=0)
    df_end=np.delete(df_end,deletelist_nucaway,axis=0)
    df_mid=np.delete(df_mid,deletelist_nucaway,axis=0)
    
    ratio=len(df_mid)/len(df)

    return(df_mid,df_end,ratio)

def AngleUpdateDirec(AnisoAngle):
    """Changes reference direction of the directional anisotropy calculation. Used if axes on the structure is not as desired. Currently rotates reference direction by 90 degrees so in -y direction"""
    AnisoAngle=AnisoAngle+math.pi/2
    if AnisoAngle>math.pi/2:
        AnisoAngle=AnisoAngle-math.pi
    return(AnisoAngle)

def AngleAnisotropy(df):
    """Calculate anisotropy as sqrt(cos(2*theta)^2+sin(2*theta)^2) where I have assumed all microtubule lenghths are the same size (which should be the case using this code). Theta is the angle direction vector creates with a reference direction. I use reference direction of x-axis (1,0,0). This is calculated in 2D so we assume all the microtubules are rougly in the x-z plane. FilterPosition has reduced the list of microtubules to those where this is approximatley true.
     Input: df filtered miccrotubule data
     Output: Anis the anisostropy on that side of the shape """

    Aniso=0
    Aniso_direc=float('NaN')
    SinAdd=0
    CosAdd=0
    RefDirec=[1,0]
    Count=0
    Count_Total=0
    for i in df:
        Count_Total=Count_Total+1
        Direc=[0,0]
        Norm2D=math.sqrt(i[3]**2+i[5]**2)
        if (math.isnan(Norm2D) or abs(Norm2D)<0.000000001):
            print("Warning: Norm2D nan or close to 0, microtubule ignored")
        else :
            Direc[0]=i[3]/Norm2D #
            Direc[1]=i[5]/Norm2D #normalise vector
            SinThet=Direc[1]*RefDirec[0]-Direc[0]*RefDirec[1]
            CosThet=Direc[0]*RefDirec[0]+Direc[1]*RefDirec[1] #cos and sin calculated by dot and cross product
            if abs(SinThet**2+CosThet**2-1)>0.0000001: 
                print (abs(SinThet**2+CosThet**2-1))
                raise ValueError("Error in AngleAnisotropy - Value should always be approximately 1")
            SinAdd=SinAdd+2*SinThet*CosThet  #double angle added 
            CosAdd=CosAdd+1-2*SinThet*SinThet
            Count=Count+1 
    if Count==0:
        print("ERROR:No Microtubules")
    Aniso=math.sqrt(CosAdd**2+SinAdd**2)
    Aniso_direc=np.arctan2(SinAdd,(CosAdd+Aniso)) #Note: when arctan(0,0) unlikely to reach right value but only occurs if everything aligned perpendicular - unlikely. 
    while Aniso_direc>math.pi/2:
        Aniso_direc=Aniso_direc-math.pi
    while Aniso_direc<-math.pi/2:
        Aniso_direc=Aniso_direc+math.pi
    if Count>0:
        Aniso=Aniso/Count
    else :
        Aniso=float('NaN')
    #Aniso_direc=AngleUpdateDirec(Aniso_direc) #Change direction if want to calculate relative to a different axis
    #print(Aniso, Aniso_direc)
    return Aniso, Aniso_direc, Count_Total

def WriteAniso(Aniso,Aniso_direc,FileName,OutputFile,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit,ratio=float('NaN'),Total=float('NaN')):
    """Anisotropy data is stored in the text file AnisotropyOutputAll. """
    #print(OutputFile)
    #print(FileName)
    f2=open(OutputFile,"a+")   
    count=0; 
    f2.write(str(Aniso))
    f2.write("; ")
    f2.write(str(Aniso_direc))
    f2.write("; ")
    f2.write(str(CutNum))
    f2.write(";")
    f2.write(str(Repitition))
    f2.write(";")
    f2.write(str(TotSteps))
    f2.write(";")
    f2.write(str(Vtk_num))
    f2.write("; ")
    f2.write(str(FileName))
    f2.write("; ")
    f2.write(str(Bund))
    f2.write("; ")
    f2.write(str(cat))
    f2.write("; ")
    f2.write(str(shrink))
    f2.write("; ")
    f2.write(str(NucInit))
    if ~np.isnan(ratio):
        f2.write(";")
        f2.write(str(ratio))       
    if ~np.isnan(Total):
        f2.write(";")
        f2.write(str(Total))    
    f2.write("\n")

def GeneralAnisotropy(p,FullFileName,OutputFile,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit):
    """Calculate anisotropy and direction anisotropy throughout. Standard Anisotropy Function!!!"""
    dprovi=extraction(p) #Extract data from vtk file
    #df=filtreBundle(dprovi) #Filtre the data so microtubule bundles only counted once
    #df=FilterPosition(dprovi) #Filter data for position of microtubules (so only include anisotropy on one face) 
    #df=FilterPosition_NucleusClose(dprovi) #Filter data to only include data near the nucleus
    df=dprovi;
    Aniso,Aniso_direc, count=AngleAnisotropy(df)
    WriteAniso(Aniso,Aniso_direc,FullFileName,OutputFile,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit,count) #Add anisotropy data to the text file

def NucleusAnisotropy(p,FullFileName,Output,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit):
    dprovi=extraction(p) #Extract data from vtk file     

    WallGap=3750 #How close to the nucleus in the long direction - set large to lok everywhere
    NucPos=24050;
    MaxX=NucPos+WallGap
    MinX=NucPos-WallGap
    
    #print("Max X: ",MaxX)
    #print("Nuc mid: ",NucPos)
    #print("Min X: ", MinX)

    MinZ=800+(625*2)*(1/3)
    MaxZ=800+(2*625)*(2/3)
    deletelist_NotCortex=[]
    deletelist_NotMid=[]
    deletelist_NotFront=[]
    deletelist_NotBack=[]   
    for k,i in enumerate(dprovi):
        #print(i[0])
        if (i[0]>MaxX or i[0]<NucPos):
            deletelist_NotFront.append(k) #Not near nucleus front
        elif (i[0]>NucPos or i[0]<MinX):
            deletelist_NotBack.append(k) #Not near nucleus front

        if (i[2]<MinZ or i[2]>MaxZ):
            deletelist_NotMid.append(k) #Near the centre root hair 
        else : 
            deletelist_NotCortex.append(k)  #Near cortex of root hair

    Temp_dp=dprovi
    df_mid=np.delete(Temp_dp,deletelist_NotMid,axis=0)
    Temp_dp=dprovi
    df_cortex=np.delete(Temp_dp,deletelist_NotCortex,axis=0)
    Temp_dp=dprovi
    df_front=np.delete(Temp_dp,deletelist_NotFront,axis=0)
    Temp_dp=dprovi
    df_back=np.delete(Temp_dp,deletelist_NotBack,axis=0)
    #print("back", len(df_back))
    #print("front",len(df_front))
    #Add anisotropy calculations to correct files
    if len(df_mid)>0:
        Aniso_mid,Aniso_direc_mid,count=AngleAnisotropy(df_mid)
        OutputFile=Output+'_MidAll.txt'
        WriteAniso(Aniso_mid,Aniso_direc_mid,FullFileName,OutputFile,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit,count) 
    if len(df_cortex)>0:
        Aniso_cortex,Aniso_direc_cortex,count=AngleAnisotropy(df_cortex)
        OutputFile=Output+'_CortexAll.txt'
        WriteAniso(Aniso_cortex,Aniso_direc_cortex,FullFileName,OutputFile,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit,count)
    if len(df_front)>0:
        Aniso_front,Aniso_direc_front,count=AngleAnisotropy(df_front)
        #print("Front:", Aniso_front)
        OutputFile=Output+'_FrontNuc.txt'
        WriteAniso(Aniso_front,Aniso_direc_front,FullFileName,OutputFile,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit,count) 
    if len(df_back)>0:
        Aniso_back,Aniso_direc_back,count=AngleAnisotropy(df_back)
        #print("Back:", Aniso_back)
        OutputFile=Output+'_BackNuc.txt'
        WriteAniso(Aniso_back,Aniso_direc_back,FullFileName,OutputFile,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit,count)
   
def PositionAnisotropy(p,FullFileName,OutputFile_Mid,OutputFile_Cortex,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit):
    """Calculate anisotropy and direction anisotropy throughout, but only using MTs in prescribed section of the cell"""
    dprovi=extraction(p) #Extract data from vtk file
    #df=filtreBundle(dprovi) #Filtre the data so microtubule bundles only counted once
    #df=FilterPosition(dprovi) #Filter data for position of microtubules (so only include anisotropy on one face) 
    df_mid,df_cortex,ratio=FilterPosition_Edges(dprovi) #Filter data to only include data near the nucleus
    #df=dprovi;
    if len(df_mid)>=0:
        Aniso_mid,Aniso_direc_mid,count=AngleAnisotropy(df_mid)
    else : 
        Aniso_mid=float('NaN')
        Aniso_direc_mid=float('NaN')
    if len(df_cortex)>=0:
        Aniso_cortex,Aniso_direc_cortex,count=AngleAnisotropy(df_cortex)
    else : 
        Aniso_cortex=float('NaN')
        Aniso_direc_cortex=float('NaN')       
    WriteAniso(Aniso_mid,Aniso_direc_mid,FullFileName,OutputFile_Mid,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit,ratio,count) #Add anisotropy data to the text file
    WriteAniso(Aniso_cortex,Aniso_direc_cortex,FullFileName,OutputFile_Cortex,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit,ratio,count) #Add anisotropy data to the text file
    
def main():
    """
    Reads in inputs provided on command line then calculates the anisotropy. 
    """
    print("Inputs to Calculate Anisotropy:")
    print(sys.argv)
    args = sys.argv[1:]
    FileLocation=args[0]
    VtkSteps=int(args[2])
    TotSteps=int(args[3])
    CutNum=float(args[4].replace("_","."))
    Repitition=int(args[5])
    #if len(sys.argv)==8:
    OutputFile=args[6]
    print(Repitition)
    #else :
    #    OutputFile="/home/tamsin/Documents/microtubule_simulations/Output/PythonGeneratedOutput/AnisotropyTextFiles/AnisotropyOutputAll.txt"
    if len(sys.argv)>=9:
        Bund=float(args[7].replace("_",".")) #The size of bundling used. 
    else:
        Bund=float('NaN');
    if len(sys.argv)>=10:
        cat=float(args[8].replace("_",".")) #Chance of an induced catastrophe. 
    else:
        cat=float('NaN');  
    if len(sys.argv)>=11:
        shrink=float(args[9].replace("_",".")) #Chance of an shrinking per section of microtubule. 
    else:
        shrink=float('NaN');   
    if len(sys.argv)>=12:
        NucInit=float(args[10].replace("_",".")) #Chance of an shrinking per section of microtubule. 
    else:
        NucInit=float('NaN');   

    Vtk_num=0;
    Vtk_num=Vtk_num+VtkSteps
    OutputFileBund=OutputFile[:-4]+"_bundles.txt"
    OutputFile_Mid=OutputFile[:-4]+"_Mid.txt"
    OutputFile_Cortex=OutputFile[:-4]+"_Cortex.txt"
    #print(OutputFile_Cortex)
    while Vtk_num<=TotSteps:
        FullFileName=args[1]+str(Vtk_num)+".vtk"
        p=lecture(FileLocation,FullFileName)
        
       	# CODE IF YOU WANT TO NOT BUNDLE FILTER THE DATA
        GeneralAnisotropy(p,FullFileName,OutputFile,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit)

        #CODE TO CALCULATE ANISOTROPY NEAR BOUNDARIES OR ENDOPLASMIC
        #PositionAnisotropy(p,FullFileName,OutputFile_Mid,OutputFile_Cortex,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit)

        #CODE TO CALCULATE ANISOTROPY FOR PAPER NEAR NUCLEUS AND BOUNDARIES
        #NucleusAnisotropy(p,FullFileName,OutputFile[:-4],Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,shrink,NucInit)
 

	      # CODE IF YOU ALSO WANT TO BUNDLE FILTER THE DATA
        #dprovi=extraction(p) #Extract data from vtk file
        #df=filtreBundle(dprovi) #Filtre the data so microtubule bundles only counted once
        #df=FilterPosition(df) #Filter data for position of microtubules (so only include anisotropy on one face) 
        #Aniso,Aniso_direc,count=AngleAnisotropy(df)
        #WriteAniso(Aniso,Aniso_direc,FullFileName,OutputFileBund,Vtk_num,CutNum,TotSteps,Repitition,Bund,cat,count) #Add anisotropy data to the text file
        
        
        Vtk_num=Vtk_num+VtkSteps
        

if __name__ == '__main__':
    main()
