These files are setup initially to run tubulaton and produce some output images which could be used for training

The ini file NucleusInsideCylinder.ini in this folder runs a simulation with some default parameters of a cylinder representing the root hair with an 
ellipse contained within representing the nucleus. Microtubules nucleate off both the cylinder and the nuclues. 

To run the script go into the bin folder and run the following command (i.e run programme on the input file NucleusInsideCylinder.ini )

./programme ../init/2024_Karan/NucleusInsideCylinder.ini 

This generates output files in vtk format which are placed in : tubulaton/Output/PythonGeneratedOutput/Data/VTK/NucInsideCylinder
You may need to create this folder for the output to be produced or change the output folder set by the variable nom_folder_output_vtk
on line 27 of NucleusInsideCylinder.ini . VTK is a file format of the output and you can read more here: https://docs.vtk.org/en/latest/design_documents/VTKFileFormats.html

To view the vtk files we use the programme paraview

https://www.paraview.org/

I can show you how to view this in paraview but essentially you need to open the vtk files in paraview and then view them in glyph format. The coloring and viewing style can be changed here. These images can then be saved as a png using File/Save Animation which will then save each frame of the video individually as a png. We may want to consider if this is the best pipeline for generating training images e.g. a more automated approach particularly if we want lots of simulations with different parameters.

Possible things to consider which may/may not effect the training accuracy:

How zoomed do we want the image?

Do we want microtubules to not be able to enter the tip of the root hair (which is closer to experimental observations)?

Is colour important? 

Do we want to ignore early images of the simulations as these match experiments least well?

We should ensure the model can detect when there are no microtubules present? Does this effect the images we want to train with? 





