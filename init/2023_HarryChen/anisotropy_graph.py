import sys
import matplotlib.pyplot as plt

#could reduce to one function get_values
def get_anisotropy(filePath):
    # takes the file and returns list of anisotropy values
    with open(filePath, 'r') as f:
        lines = f.readlines()

    anisotropy =[]
    for line in lines:
        values = line.split(';')
        anisotropy.append(float(values[0]))

    return anisotropy

def get_angles(filePath):
    # takes the file and returns a list of absolute value of angles
    with open(filePath, 'r') as f:
        lines = f.readlines()

    angles =[]
    for line in lines:
        values = line.split(';')
        angles.append(abs(float(values[1])))

    return angles

def main():
    # takes names of simulation and plot the anisotropy values on the same graph
    args = sys.argv[1:]

    anisotropies = {}
    angles = {}
    for fileName in args:
        filePath = f'/home/harry/Documents/tubulaton/Output/{fileName}/{fileName}_CortexAll.txt'
        anisotropies[fileName] = get_anisotropy(filePath)
        angles[fileName] = get_angles(filePath)
    
    figure, axis = plt.subplots(2, 1)

    # anisotropy plot
    for name, anisotropy in anisotropies.items():
        axis[0].plot(anisotropy, label=name)

    #TODO get time step from somewhere

    axis[0].set_ylabel('anisotropy')
    axis[0].legend()
    axis[0].set_title('Anisotropy over time')

    # angle plot
    for name, angle in angles.items():
        axis[1].plot(angle, label=name)

    axis[1].set_ylabel('|angle|')
    axis[1].legend()
    axis[1].set_title('Unsigned angle over time')

    plt.show()

if __name__ == '__main__':
    main()
