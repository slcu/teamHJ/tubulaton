//classe Update
// Functions which change the contours (boundaries) during the simualations
#include <vector>
#include "utility.hpp"
#include "Contour.hpp"
#include "ElementPool.hpp"
#include "MicrotubulePool.hpp"
#include "math.h"
#include <sstream> 
#include <fstream>
#include <algorithm>

using namespace std;

// Function to update the position of an element e which has moved by PosChange. ContourHash also updated. Reutrns new position
vector<double> UpdateElement(Contour *c, Element *e, vector<double> PosChange)
{
//cout<<"In UpdateElement"<<endl;
//cout<<"pos_old: "<<pos_old<<endl;
Anchor *an = e->getAnchor();
vector<double> pos;
pos.push_back(*an->getX());
pos.push_back(*an->getY());
pos.push_back(*an->getZ());
pos[0]=pos[0]+PosChange[0];
pos[1]=pos[1]+PosChange[1];
pos[2]=pos[2]+PosChange[2];
an->setPosition(pos);
e->setAnchor(*an);
return pos;
} 

//Same as Update Element above but first set the normal data too
vector<double> UpdateElement(Contour *c, Element *e, vector<double> PosChange, vector<double> New_normals)
{Shape s = e->getShape();
s.setDirection(New_normals);
e->setShape(s);
vector<double> pos=UpdateElement(c, e, PosChange);
return pos;
}

vector<long> Update_LocalHash(Contour *c, Element *e,vector<long> AllHashMT,vector<double> pos){
// long pos_new=c->GetContourElementHash(e); #commented as value not used
for (int i=-1; i<2; i++)
	{
		for (int j=-1; j<2; j++)
		{
			for (int k=-1; k<2; k++)
			{
				double x2 = pos[0]+i;
				double y2 = pos[1]+j;
				double z2 = pos[2]+k;                
				if ((x2>=0) & (y2>=0) & (z2>=0)){
            long h=c->GetMTNearElementHash(x2,y2,z2);
            if (std::find(AllHashMT.begin(), AllHashMT.end(), h)==AllHashMT.end()){AllHashMT.push_back(h);}
            }
      }
    }
  }
return AllHashMT;
}

//Function to  move the contour at a steady pace to the right
void MoveValues(Contour *c,Parametres *params, MicrotubulePool *Mpool)
{
//cout << "In move values"<<endl;
std::deque<Element *> AllElements=c->GetContourElements(0);
int LenEle = AllElements.size();
vector<long> AllHashMT;
for (int i=0; i<LenEle; i++){
Element *e = AllElements.at(i);
vector<double> PosChange;
PosChange.push_back(params->getProprieteD("Movement_Speed"));
PosChange.push_back(0);
PosChange.push_back(0);
vector<double> pos_new;
if (not(PosChange[0]==0 and PosChange[1]==0 and PosChange[2]==0)){
long pos_old=c->GetContourElementHash(e);
pos_new=UpdateElement(c,e,PosChange);
c->UpdateSpaceContor(e,pos_old);
e->setPropriete("ChangeX",PosChange[0]);
e->setPropriete("ChangeY",PosChange[1]);
e->setPropriete("ChangeZ",PosChange[2]);
if (e->getShape().getDirection()[0]*PosChange[0]+e->getShape().getDirection()[1]*PosChange[1]+e->getShape().getDirection()[2]*PosChange[2]<0) {
AllHashMT=Update_LocalHash(c,e,AllHashMT,pos_new);}}
}
Mpool->UpdateMTWithin(AllHashMT);
}

//Function moves the given surface at stated speed to the right. Moves the microtubules effects too then checks if any tubulin contained within the shape and if now outside the membrane removes that element. 
void MoveNucleus(Contour *c,Parametres *params, MicrotubulePool *Mpool, int ContNum)
{
vector<double> PosChange;
PosChange.push_back(params->getProprieteD("Movement_Speed_2"));
PosChange.push_back(0);
PosChange.push_back(0);
std::deque<Element *> AllElements=c->GetContourElements(ContNum);
int LenEle = AllElements.size();
vector<long> AllHashMT;
for (int i=0; i<LenEle; i++){
Element *e = AllElements.at(i);
vector<double> pos;
if (not(PosChange[0]==0 and PosChange[1]==0 and PosChange[2]==0)){
long pos_old=c->GetContourElementHash(e);
pos=UpdateElement(c,e,PosChange); //Update element to new position and get its new position 
c->UpdateSpaceContor(e,pos_old);
e->setPropriete("ChangeX",PosChange[0]);
e->setPropriete("ChangeY",PosChange[1]);
e->setPropriete("ChangeZ",PosChange[2]);
}
else {Anchor *an = e->getAnchor();
pos.push_back(*an->getX());
pos.push_back(*an->getY());
pos.push_back(*an->getZ());}

//Find which contour has are around where the contour is moving. 
AllHashMT=Update_LocalHash(c,e,AllHashMT,pos);
}
//Mpool->UpdateMTWithin(AllHashMT); 
Mpool->UpdateMoveMT(AllHashMT,PosChange);
//In the hash regions identifed above check if any MT elements are now outside the domain. 
}

//Function to update contour elements based on read in text file
void Update_ReadContour(Contour *c,Parametres *params, MicrotubulePool *Mpool){
//cout<<"In ReadContour"<<endl;
std::deque<Element *> AllElements=c->GetContourElements(0); //AllElements in the Contour
vector<long> AllHashMT;
int LenEle = AllElements.size();
string FileName=params->getProprieteS("nom_folder_input_vtk")+params->getProprieteS("Update_file"); //Full directory of the text file with update instructions
cout<<FileName<<endl;
fstream newfile;
newfile.open(FileName,ios::in);
if (newfile.is_open()){   //checking whether the file is open
    string tp; //Each line of the text file
    int count=0;
    while(getline(newfile, tp)){  //read data from file object and put it into string.
        if (count==0){count=1;}  //code to ignore first line of the file
        else{
        vector<float> points;
        istringstream x (tp);
        string sub;
        while (getline(x,sub,',')){
        
        points.push_back(stof(sub));}
        //int cur_ind;
        float min1=100000;
        Element *cur;
        vector<long> AllHashMT;
        for (int i=0; i<LenEle; i++){
        Element *e = AllElements.at(i);
        float dist=sqrt(pow(points[0]-*e->getAnchor()->getX(),2)+pow(points[1]-*e->getAnchor()->getY(),2)+pow(points[2]-*e->getAnchor()->getZ(),2));
        if (dist<min1){
        min1=dist;
        cur=e;
        //cur_ind=i;
        }
        }
        vector<double> PosChange;
        PosChange.push_back(points[3]);
        PosChange.push_back(points[4]);
        PosChange.push_back(points[5]);   
        vector<double> NewNormal;
        NewNormal.push_back(points[9]);
        NewNormal.push_back(points[10]);
        NewNormal.push_back(points[11]); 
        cur->setPropriete("ChangeX",PosChange[0]);
        cur->setPropriete("ChangeY",PosChange[1]);
        cur->setPropriete("ChangeZ",PosChange[2]);     
        long pos_old=c->GetContourElementHash(cur);
        vector<double> pos_new=UpdateElement(c,cur,PosChange,NewNormal);
        c->UpdateSpaceContor(cur,pos_old);   
    }
    }
  }
  newfile.close();   //close the file object.
  cout<<"UpdateReadContour:" <<endl;
  Mpool->UpdateMoveMT(*c);  
}

//Update proba_shrink_beyond in params if being used in this simulation
void Update_ShrinkBeyond(Parametres *params){
//Only if proba_shrink_beyond is a parameter being used in this model should we change it 
if (params->getProprieteD("proba_shrink_beyond")>=0){ 
double New_ShrinkBeyond;
New_ShrinkBeyond=params->getProprieteD("Xpos_beyond")+params->getProprieteD("Movement_Speed");
params->setProprieteD("Xpos_beyond",New_ShrinkBeyond);}

}
