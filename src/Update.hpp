//classe Update
//For updating contours during the simulations

#include <vector>

class MicrotubulePool;
class ElementPool;
class Parametres;
class Element;
class Contour;

std::vector<double> UpdateElement(Contour *c, Element *e, std::vector<double> PosChange); //Update an element to its new position
std::vector<double> UpdateElement(Contour *c, Element *e, std::vector<double> PosChange, std::vector<double> New_normals); //Update normals then position of element
std::vector<long> Update_LocalHash(Contour *c, Element *e, std::vector<long> AllHashMT, std::vector<double> pos);

void Update_ReadContour(Contour *c, Parametres *params, MicrotubulePool *Mpool);

//std::vector<double> UpdateElementOnly(Contour *c, Element *e, std::vector<double> PosChange); 

void  MoveValues(Contour *c, Parametres *params, MicrotubulePool *Mpool);

void  MoveNucleus(Contour *c, Parametres *params, MicrotubulePool *Mpool, int ContNum);

void Update_ShrinkBeyond(Parametres *params); //Updates parameter proba_shrink_beyond if it is being used and we move the contour
