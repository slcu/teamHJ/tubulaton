//classe ElementPool
//non si element.h deja importe ailleurs
#ifndef __ELEMENTPOOL_H_INCLUDED__
#define __ELEMENTPOOL_H_INCLUDED__
#include <iostream>
#include "Element.hpp"
#include "Contour.hpp"
#include <unordered_map>
#include <vector>
#include <list>

typedef int Id;
class Parametres;


typedef std::unordered_map<long, std::vector<Id> > block;
class ElementPool
{
    friend std::ostream& operator<<(std::ostream& os, ElementPool& p);
    public:
        ElementPool(Parametres *params);
        ~ElementPool();
        Element* giveElement();
        Element* giveElement(Structure* id);
        Element* getElement(Id id, std::string s);
        void erase(Element*  id);
        void spaceContourRegister(Element* e);
        void spaceContourRegister(Element* e, long pos); 
        void RemoveSpaceContourRegister(Element* e, long pos_old);
        void RemoveSpaceMtRegister(Element* e, long pos_old);
        void spaceMtRegister(Element* e);
        
        
        std::list<Element *> getListElements();
        std::list<Element *> getListSpace2Id();
        std::list<Element *> getListId2Space();
        
        long getPositionContour(Element * id);
        long getPositionMt(Element * id);
        
        Id maxId();
        
        int testRecursif(Element *candidat, Element *e, int degre);
        
        //la fonction gÃ©rant la recherche de plus proches voisins, pour l'instant cette version est celle qui renvoie l'Ã©lÃ©ment voisin
        std::pair<double, Element*>  NN_Mt(Element* plus);
        std::pair<double, Element*>  NN_Contour(Element* plus);
        void NN_All_Contour(Element* plus, int change); //Calculate all nearest neighbours on the contour to see if forces needs updating for output
        std::vector< std::pair<double, Element*>> NN_AllContourOutput(Element* plus); //Calculate the nearest neighbour in each contour to point plus
        std::vector<std::pair<double, Element*>> NN_AllContourOutput(std::vector<double> pos_plus,std::vector<Element*> content); //Calculates nearest neighbour in 'content' to 'pos_plus'
        std::vector<std::pair<double, Element*>> NN_AllContourOutput(std::vector<double> pos_plus); //Calculate the nearest neighbour in each contour to point pos_plus (assuming in neighbourhood)
        std::vector<std::pair<double, Element*>> NN_EnireContourOutput(std::vector<double> pos_plus,Contour c); //Calcluate nearest neighbour by checking all elements on the contour
	      double CalcGap(std::vector<double> pos, Element *e_mb); //Calculate gap between element as pos and the contour element e_mb
             
        // function to check if neighbour still at large angle for crossover severing
      	int N_Check(Element* plus);
        //rajouter d'autre possibilitÃ©s : pondÃ©ration de tous les Ã©lÃ©ments prÃ©sents dans le coin par exemple
        
        //a placer peut etre plus tard dans une classe espace dont elementpool pourrait hÃ©riter
        std::vector<Element*>* getSpaceContour();
        std::vector<Element*>* getSpaceMt();
        void setContour(Contour *c);
        Contour * getContour();

        std::vector<Element*> getSpaceContourContent(double x, double y, double z);
        std::vector<Element*> getSpaceMtContent(double x, double y, double z);
        void spaceContourRegisterLimits(double bounds[6]); // change resolutions to reflect surface shape
        
        long hashContour(Element* id) ;
        long hashContour(Anchor a);//transforms a position into the hash code
        long hashContour(double x, double y, double z);
        long hashMt(Element* id) ;
        long hashMt(Anchor a);//transforms a position into the hash code
        long hashMt(double x, double y, double z);
        
        void enlist_tocut(Element *e);
        void unlist_tocut(Element *e);
        std::list<Element *> getlist_tocut();


	//functions for microtubule cutting at crossovers
        void enlist_crossover_tocut(Element *e);
        void unlist_crossover_tocut(Element *e);
        std::list<Element *> getlist_crossover_tocut();
        
        void resetDead();
        void garbageCollector();
        
        int rencontre_bundle_grow;//au voisin
        int rencontre_bundle_shrink;//au voisin
        int rencontre_mb_grow;//Ã  la membrane
        int rencontre_mb_shrink;//Ã  la membrane
        int cross;//situation Ã  la membrane oÃ¹ l'on crosse
        int quitte_mb;
        
        int scission;//Ã©vÃ©nement de scission

	int E_check; // used for debugging
	void ErrorChecking(int temp1);
 
        std::vector<Element*> UpdateElementPool_Outside(std::vector<long> AllHashMT);
        void UpdateElementPool_Move(std::deque<Element *> MTElements, std::vector<double> PosMove);
        std::vector<double> UpdateElementPool_MTMovement( std::deque<Element *> MTElements,Contour c);
        std::vector<Element *> UpdateElementPool_MoveDefinitely( std::deque<Element *> MTElements, std::vector<double> PosMove, Contour c);
        
    private:
        //std::unordered_map<Id, Element> m_elements;
        std::vector<Element*> m_elements;
        int m_maxId;
        //parent
        int m_structure;
        //liste des vivants
        //std::vector<Element*>  m_vivants;
        Contour *m_contour;
        Parametres *m_params;
        std::vector<int> taille_pow;
        std::vector<int> taille_val;
        
        //partie espace
        //TODO : changer le type de m_id2space, VOIR SI NECESSAIRE ou si STOCKAGE DANS ELEMENT
        std::unordered_map<Element*, long > m_id2spaceContour;//the basic int -> element structure
        std::unordered_map<Element*, long > m_id2spaceMt;//the basic int -> element structure
        std::vector<Element*> *m_spaceContour2id;//the basic int -> element structure
        std::vector<Element*> *m_spaceMt2id;//the basic int -> element structure
        
        
        //TODO : cette categorie sera inutile dans le contexte d'un tableau en memoire
        std::list<int> m_populated;//list of the coordinates that have data
        int m_nbdeads;//nb of deads
        std::vector<Element *> m_deads;//nb of deads
        std::list<Element *> m_tocut;//nb of elements concerned by an angle that can increase the probability to be cut. 
	std::list<Element *> m_crossover_tocut;//nb of elements which have crossover over another microtubule and thus has a probability to be cut
        
        int m_hashContour_resolution;//the resolution used to convert coordinates into hash
        int m_hashMt_resolution;//the resolution used to convert coordinates into hash
        int Limit_Each_Res; //size of x,y,z when conveting to has (maximum value of each coordinate)
        
        int CalculatePow2(int n); // calculate the smallest power of 2 larger than n. Necessary to set grid size as small as possible  
        
        
        
        
        
};
#endif
