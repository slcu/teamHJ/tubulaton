//elementPool
#include "MicrotubulePool.hpp"
#include "Microtubule.hpp"
#include "ElementPool.hpp"
#include <vector>
#include <deque>
#include <unordered_set>
#include <list>
#include <utility>
#include "utility.hpp"
#include "Contour.hpp"
#include <fstream>
#include <math.h>
#define verbose 0
#define deb(x) if (verbose>=1){cout << x << endl;}
#define deb2(x) if (verbose>=2){cout << x << endl;}
#define deb3(x) if (verbose>=3){cout << x << endl;}
#define deb4(x) if (verbose>=4){cout << x << endl;}


using namespace std;


    std::ostream& operator<<(std::ostream& os, MicrotubulePool& p)
    {
        cout << p.maxId()<<endl;
        for (unordered_map<Id, Microtubule>::iterator it = p.getMicrotubules()->begin();it != p.getMicrotubules()->end();++it)
        {
            os << "id "<< it->first <<" microtubule "<<it->second<<endl;
        }
        return os;
    }
 
     std::ofstream& operator<<(std::ofstream& os, MicrotubulePool& p)
    {
        cout << p.maxId()<<endl;
        for (unordered_map<Id, Microtubule>::iterator it = p.getMicrotubules()->begin();it != p.getMicrotubules()->end();++it)
        {
            os << "id "<< it->first <<" microtubule "<<it->second<<endl;
        }
        return os;
    }
    
    MicrotubulePool::MicrotubulePool()
    {
        if (verbose>=1) {cout<<"constructeur de MicrotubulePool version () :"<<endl;}
        m_maxId=1;
    }
    
    MicrotubulePool::MicrotubulePool(Parametres *params, int nb_gamma_tub, ElementPool *ep)
    {
        if (verbose>=1) {cout<<"constructeur de MicrotubulePool version (int nb_gamma_tub, ElementPool *ep):"<<endl;}
//         int m_influence = 0;
        m_maxId=1;
        m_nb_gamma_tub=nb_gamma_tub;
        m_params = params;
        
        destruction_microtubule = 0;
        creation_microtubule = 0;
        cut_microtubule = 0;
	override_cut_microtubule=0;
        detachements=0;
	shrinkages=0;
        
        for (int i = 0; i < nb_gamma_tub;i++)
        {
            //vector<double> v = pointOnSphere(150, 3000,3000,3000);
            vector<double> v = pointOnContour(ep->getContour());
            Id current=maxId()+1;
            m_maxId++;
            Microtubule e(m_params, ep,this, current, v,1); // Function seems unused but if used assume initating on contour 1
            m_microtubules[current]=e;
            e.setId(current);
            pair<Id,Microtubule*> p;
            p.first=current;
            p.second=&(m_microtubules[current]);
            m_gamma_tub.push_back(p);
        }
    }
    
    //MicrotubulePool::MicrotubulePool(Parametres *params, int nb_gamma_tub, double proba_detachement, double proba_initiation, ElementPool *ep)
    //{
        //if (verbose>=1) {cout<<"constructeur de MicrotubulePool version (int nb_gamma_tub, double proba_detachement, double proba_initiation, ElementPool *ep):"<<endl;}
        //int m_influence = 0;
        //m_maxId=1;
        //m_nb_gamma_tub=nb_gamma_tub;
        //m_proba_detachement = proba_detachement;
        //m_proba_initiation = proba_initiation;
        //m_ep=ep;
        //m_params = params;
        //
    //};
    
    
    MicrotubulePool::MicrotubulePool(Parametres *params, ElementPool *ep)
    {
        m_influence = params->getProprieteD("part_influence_normale");
        if (verbose>=1) {cout<<"constructeur de MicrotubulePool version (int nb_gamma_tub, double proba_detachement, double proba_initiation, ElementPool *ep):"<<endl;}
        m_maxId=1;
        m_nb_gamma_tub=params->getProprieteI("nb_microtubules_init");
        m_proba_detachement = params->getProprieteD("proba_detachement_par_step_par_microtubule");
        //m_proba_initiation = params->getProprieteD("proba_initialisation_par_step");
	m_proba_shrink = params->getProprieteD("proba_shrink");
	m_proba_crossmicro_cut = params->getProprieteD("proba_crossmicro_cut");
        m_proba_tocut = params->getProprieteD("proba_tocut");
        m_proba_rescue = params->getProprieteD("proba_rescue");
        m_obj_num= params->getProprieteI("num_input_vtk");
        m_ep=ep;
        m_params = params;
        
        destruction_microtubule = 0;
        creation_microtubule = 0;
        cut_microtubule = 0;
	override_cut_microtubule=0;
        detachements=0;
        shrinkages=0;

    }
    
    MicrotubulePool::~MicrotubulePool()
    {       
           if (verbose>=1) {cout<<"destructeur de MicrotubulePool :"<<endl;}
    }
    
    void MicrotubulePool::initiate()
    {
    double cen_unif=m_params->getProprieteD("Initialisation_Volume_Radius");
    int nb_mic=0;
    if (m_params->getProprieteI("Sep_Nucleations")==1)
        {   int Num_in;
            int Everywhere_Num_in;
            for (int j=1; j<=m_obj_num; j++)
            {
                //extract the correct value for microtubule initialisation on this surface
                if (j==1)
                { 
                 Num_in=m_params->getProprieteI("nb_microtubules_init");
                 Everywhere_Num_in=m_params->getProprieteI("Everywhere_nb_microtubules_init");
                }
                else
                { 
                 string na_temp_nb_mic="nb_microtubules_init_"+std::to_string(j);
                 Num_in=m_params->getProprieteI(na_temp_nb_mic);
                 string na_temp_nb_mic_every="Everywhere_nb_microtubules_init_"+std::to_string(j);
                 Everywhere_Num_in=m_params->getProprieteI(na_temp_nb_mic_every);
                }
                cout << "Num_in=" << Num_in << endl;
                for (int k=0; k<Num_in; k++)
                {
                    deb4("Separate surfaces: boucle ngamma 1")
                    //vector<double> v = pointOnSphere(150, 3000,3000,3000);
                    vector<double> v=ChooseNewMT(m_ep,j,0,cen_unif);
                    //vector<double> v = pointNucleateOnContour(m_ep->getContour(),j-1);
                    //Id current=maxId()+1;
                    Id current = this->giveMicrotubule(m_ep, v, j);
                    //m_maxId++;
                    deb4("Separate surfaces: boucle ngamma 2")
                    //Microtubule e(m_params, m_ep,this, current, v, j);
                    //m_microtubules[current]=e;
                    //e.setId(current);
                    deb4("Separate surfaces: boucle ngamma 3")
                    pair<Id,Microtubule*> p;
                    p.first=current;
                    p.second=&(m_microtubules[current]);
                    deb4("Separate surfaces: boucle ngamma 4")
                    m_gamma_tub.push_back(p);
                    //e.getPlus()->setStructure(&m_microtubules[current]);
                    nb_mic++;
                }
                for (int k=0; k<Everywhere_Num_in; k++)
                {
                    deb4("Separate surfaces: boucle ngamma 1")
                    //vector<double> v = pointOnSphere(150, 3000,3000,3000);
                    vector<double> v=ChooseNewMT(m_ep,j,1,cen_unif);
                    //vector<double> v = pointNucleateOnContour(m_ep->getContour(),j-1);
                    Id current = this->giveMicrotubule(m_ep, v, j);
                    //Id current=maxId()+1;
                    //#m_maxId++;
                    deb4("Separate surfaces: boucle ngamma 2")
                    //Microtubule e(m_params, m_ep,this, current, v, j);
                    //m_microtubules[current]=e;
                    //e.setId(current);
                    deb4("Separate surfaces: boucle ngamma 3")
                    pair<Id,Microtubule*> p;
                    p.first=current;
                    p.second=&(m_microtubules[current]);
                    deb4("Separate surfaces: boucle ngamma 4")
                    m_gamma_tub.push_back(p);
                    //e.getPlus()->setStructure(&m_microtubules[current]);
                    nb_mic++;
                }
            }
        }
    else
        {   double cen_unif=m_params->getProprieteD("Initialisation_Volume_Radius");
            int cont_num;
            if (m_obj_num==1){cont_num=1;}
            else if (cen_unif>0){cont_num=m_obj_num+1;}
            else {cont_num=-1;}
            int Num_in=m_params->getProprieteI("nb_microtubules_init");
            int Everywhere_Num_in=m_params->getProprieteI("Everywhere_nb_microtubules_init");
            for (int k = 0; k < Num_in;k++)
            {   
                deb4("boucle ngamma 1")
                vector<double> v=ChooseNewMT(m_ep,cont_num,0,cen_unif);
                //vector<double> v = pointOnSphere(150, 3000,3000,3000);
                //if (cen_unif>0){ string st_volume=m_params->getProprieteS("Nucleation_Volume_Shape");
                //     if (st_volume=="sphere"){v=GenerateRandomMTDirecSphere(cen_unif);}
                //     else {v=GenerateRandomMTDirec(cen_unif);}}
                //else{v = pointNucleateOnContour(m_ep->getContour(),0);}

                Id current = this->giveMicrotubule(m_ep, v, cont_num);
                //Id current=maxId()+1;
                //m_maxId++;
                deb4("boucle ngamma 2")
                //Microtubule e(m_params, m_ep,this, current, v,cont_num); 
                //m_microtubules[current]=e;
                //e.setId(current);
                deb4("boucle ngamma 3")
                pair<Id,Microtubule*> p;
                p.first=current;
                p.second=&(m_microtubules[current]);
                deb4("boucle ngamma 4")
                m_gamma_tub.push_back(p);
                //e.getPlus()->setStructure(&m_microtubules[current]);
                nb_mic++;
            }
            for (int k = 0; k < Everywhere_Num_in;k++)
            {
                deb4("boucle ngamma 1")
                vector<double> v=ChooseNewMT(m_ep,cont_num,1,cen_unif);
                //vector<double> v = pointOnSphere(150, 3000,3000,3000);
                //if (cen_unif>0){ string st_volume=m_params->getProprieteS("Nucleation_Volume_Shape");
                //     if (st_volume=="sphere"){v=GenerateRandomMTDirecSphere(cen_unif);}
                //     else {v=GenerateRandomMTDirec(cen_unif);}}
                //else{v = pointNucleateOnContour(m_ep->getContour(),0);}

                //Id current=maxId()+1;
                //m_maxId++;
                Id current = this->giveMicrotubule(m_ep, v, cont_num);
                deb4("boucle ngamma 2")
                //Microtubule e(m_params, m_ep,this, current, v,cont_num); 
                //m_microtubules[current]=e;
                //e.setId(current);
                deb4("boucle ngamma 3")
                pair<Id,Microtubule*> p;
                p.first=current;
                p.second=&(m_microtubules[current]);
                deb4("boucle ngamma 4")
                m_gamma_tub.push_back(p);
                //e.getPlus()->setStructure(&m_microtubules[current]);
            

                nb_mic++;
            }
        }
    cout<<"Number of MTs created:"<<nb_mic<<endl;
    }

   
    
    Id MicrotubulePool::giveMicrotubule(ElementPool *ep)
    {
        
        Id current=maxId()+1;
        m_maxId++;
        Microtubule e(m_params, ep,this, current);
        m_microtubules[current]=e;
        e.setId(current);
        e.getPlus()->setStructure(&m_microtubules[current]);
        //cout<<*e.getPlus()<<endl;
        return current;
    }

// "giveMicrotubule" Function used when you have a whole microtubule and want to add it to the microtubule pool. This happens for example after a microtubule has been cut. Then the first microtubule just takes the place of the origional microtubule and the second gets added to the "microtubule pool" using this function. The input *ep is the element pool of elements which makes up the microtubule. 
    Id MicrotubulePool::giveMicrotubule(ElementPool *ep, deque<Element *> body)
    {
        Id current=maxId()+1;
        m_maxId++;
        if (verbose==2) {cout<<"giveMicrotubule :"<<endl;}
        Microtubule e(m_params,ep,this, current, body);
        m_microtubules[current]=e;
        e.setId(current);
        e.getPlus()->setStructure(&m_microtubules[current]); //Slightly unclear what the e.getPlus is used for on this line. 
        //cout<<*e.getPlus()<<endl;
        return current;
    }
    
    
// Generates a new microtubule. Similar to above. Used when a new microtubule appears in initiation. Unclear whether this function and the above Microtubulue::giveMicrotubule both needed. 	
     Id MicrotubulePool::giveMicrotubule(ElementPool *ep, vector<double> vec, int cont_num)    
	{
        Id current=maxId()+1;
        m_maxId++;
        if (verbose==2) {cout<<"giveMicrotubule :"<<endl;}
        Microtubule e(m_params,ep,this, current, vec, cont_num);
        m_microtubules[current]=e;
        e.setId(current);
        e.getPlus()->setStructure(&m_microtubules[current]);
        //cout<<*e.getPlus()<<endl;
        return current;
    }
    
       
    
        
    
    Microtubule* MicrotubulePool::getMicrotubule(Id id)
    {
        //ici ajouter des tests de coherence
        return &m_microtubules[id];
    }
    
    unordered_map<Id, Microtubule>* MicrotubulePool::getMicrotubules()
    {
        //ici ajouter des tests de coherence
        return &m_microtubules;
    }
    

// Gets key of every microtubule in the m_microtubules
    list<Id> MicrotubulePool::getListMicrotubules()
    {
        list<Id> lm;
        for (unordered_map<Id,Microtubule>::iterator it=m_microtubules.begin();it!=m_microtubules.end();it++)
        {
            lm.push_back(it->first); // adds key of the microtubule to the list lm
        }
        return lm;
    }
    
    Id MicrotubulePool::maxId()
    {
        return m_maxId;
    }
    
    int MicrotubulePool::getInfluence()
    {
        return m_influence;
    }
    
    
    
    void MicrotubulePool::erase(Id id)
    {
		deb("MicrotubulePool::erase(Id id)");
		destruction_microtubule+=1;
		//cout << "erase microtubule : "<< m_microtubules[id]  << endl;
		m_microtubules.erase(id);		
		deb("MicrotubulePool::erase(Id id) OVER");
	}

    int MicrotubulePool::write(string s_filename)
    {
        ofstream myfile;
        char *filename = (char*)s_filename.c_str();
        myfile.open(filename);
        myfile << *this;
        myfile.close();
        return 0;
    }

    std::vector<int> MicrotubulePool::getMicrotubuleLengths()
    {   std::vector<int> MicroSizes;
	list<Id> lp = getListMicrotubules();
        for (list<Id>::iterator it=lp.begin();it!=lp.end();it++)
        {   
        	int Temp1=getMicrotubule(*it)->getSize();
        	MicroSizes.push_back(Temp1);
	}
        return MicroSizes;
    }

// Move the microtubules on one timestep. First coupure "cut" : every microtubule in the list "getlist_tocut" is cut with a probability "m_proba_tocut". The cut produces two microtubules, one of which replaces the origional microtubule the other which is added to the MicrotubulePool as a new microtubule. The second microtubule which contains the free end continues to grow at this end but recedded from the cut. The first microtubule still attached to its base only decays away from the cut. Secondly: detachment. Every microtubule has  a probability m_proba_detachment of detaching from its base. If it does then it starts to decay from its minus end. Thirdly: initiation. the integar part of proba_initiation indicates the number of microtubules which have to be created. These are created and added to the list microtubules. The decimal part of proba_initation details the probability of a single new microtubule being created, which is created after the above number of required microtubules are produced. Finally it sends each microtubule individually to m.evolve() presumably to progress them all forward in time - note this seems to evolve microtubules initialised in this timestep too. 
//Added an additiona option of shrinkage - chance that the microtubules start to shrink based on a probability
    void MicrotubulePool::evolve(ElementPool *ep)
    {   //version avec trois probas : initiation, separation et coupure
        deb("MicrotubulePool::evolve(ElementPool *ep)");        
        //srand(time(0)+getpid());
        //cut
        list<Element *> l_tocut = m_ep->getlist_tocut();        
        int k=0;
        for (list<Element *>::iterator it=l_tocut.begin();it!=l_tocut.end();it++)
        { 
            //deb3("test...");
            double proba = (float)rand()/(float)RAND_MAX;
            if ((proba < m_proba_tocut))
            {
                deb3("ici...");
                //cout << **it <<  endl;
                Structure *s=(*it)->getStructure();
                deb3("... ici"); 
                s->scission(*it);
                //cut_microtubule+=1;
                deb3("... ou lÃ "); 
            }
	//cout << "k = "<< k << endl;
        }

	//cutting microtubules which have crossed over each other
        list<Element *> m_cross_tocut = m_ep->getlist_crossover_tocut();  
        for (list<Element *>::iterator it=m_cross_tocut.begin();it!=m_cross_tocut.end();it++)
        {   double proba = (float)rand()/(float)RAND_MAX;
	    k++;
            if ((proba < m_proba_crossmicro_cut))
            {   //cout << "Probability=" << proba << " Microtubule cut=" << m_proba_crossmicro_cut << endl;  	
                Element * Cross_element=(*it)->getCrossMicro();
                if (Cross_element->isAlive()==1) //check it is still crossing a microtubule
		{ 
                	deb3("ici...");
                	//cout << **it <<  endl;
                	Structure *s=(*it)->getStructure();
                	deb3("... ici"); 
                	s->scission(*it);
			ep->unlist_crossover_tocut(*it);
                        (*it)->setCrossMicro(NULL);
                	cut_microtubule+=1;

		}
		else   //if it is not crossing a microtubule just delete it from the list. Keep and eye on how many cutting this is stopping.
		{	//ep->ErrorChecking(1);
			//ep->N_Check(*it);	
			//ep->ErrorChecking(0)
			// delete from list if decided nolonger a crossover
			ep->unlist_crossover_tocut(*it);
                        (*it)->setCrossMicro(NULL);
			override_cut_microtubule+=1;
		}
		
            }
        }
        //cout << "Total micro considered for cutting=" << k; 
        //cout <<" Overide=" <<override_cut_microtubule;
        //cout << " Cut=" << cut_microtubule << endl;

        
        //detachement - microtubule starts decaying from minus end
        list<Id> lp = getListMicrotubules();
        for (list<Id>::iterator it=lp.begin();it!=lp.end();it++)
        {   
            Microtubule *m=getMicrotubule(*it);
            double proba = (float)rand()/(float)RAND_MAX;
            if ((proba < m_proba_detachement))
                {
                    deb3("testDetach...");
                    if (verbose==3){cout<<*m<<endl;}
                    //voir si je rajoute une condition sur l'etat du microtubule
                    if (m->getProprieteI("gr_st_moins")!=-1){
                    m->setPropriete("gr_st_moins", -1);
                    m->setPropriete("TreadmillingStart",m_params->getProprieteI("Time"));
                    detachements+=1;}
                }
        }

	// microtubules have a probability of decaying from the positive end suddenly
        double proba_shrink_bey=m_params->getProprieteD("proba_shrink_beyond");
        lp = getListMicrotubules();
        if (proba_shrink_bey<0){
        for (list<Id>::iterator it=lp.begin();it!=lp.end();it++)
        {   
            Microtubule *m=getMicrotubule(*it);
            double proba = (float)rand()/(float)RAND_MAX;
            if ((proba < m_proba_shrink) and (m->getProprieteI("gr_st_plus")==1))
                {
                    deb3("testshrink...");
                    if (verbose==3){cout<<*m<<endl;}
                    m->setPropriete("gr_st_plus", -1);
                    shrinkages+=1;
                }
            if ((proba < m_proba_rescue) and (m->getProprieteI("gr_st_plus")==-1))
                {
                    m->setPropriete("gr_st_plus", +1);
                }
        }
        }
        else {
        for (list<Id>::iterator it=lp.begin();it!=lp.end();it++)
           {double temp_m_shrink;
            Microtubule *m=getMicrotubule(*it);
            double proba = (float)rand()/(float)RAND_MAX;
            if (m->getProprieteI("ShrinkBeyond")==1){temp_m_shrink=proba_shrink_bey;}
            else {temp_m_shrink=m_proba_shrink;}
            if ((proba < temp_m_shrink))
                {  deb3("testshrink_maybeAreaMoreStable...");
                    if (verbose==3){cout<<*m<<endl;}
                    m->setPropriete("gr_st_plus", -1);
                    shrinkages+=1;
                }     
            }   
        }
       
        //initiation
//         double pos = (float)rand()/(float)RAND_MAX;
    double cen_unif=m_params->getProprieteD("Initialisation_Volume_Radius");
    if (m_params->getProprieteI("Sep_Nucleations")==1)
        {   double proba_initiation;
            double proba_initiation_every;
            for (int j=1; j<=m_obj_num; j++)
            {
                //extract the correct value for microtubule initialisation on this surface
                if (j==1)
                { 
                 proba_initiation=m_params->getProprieteD("proba_initialisation_par_step");
                }
                else
                { 
                 string na_temp_nb_mic="proba_initialisation_par_step_"+std::to_string(j);
                 proba_initiation=m_params->getProprieteD(na_temp_nb_mic);
                }
                int cont_num=j; // surface number
                //cout<< "Initiating on multiple surfaces begin" << " Cont_num=" << cont_num <<endl;
                InitiateMidway(ep, proba_initiation, cont_num,0,-10);
 
                 //Initiate everywhere
                 if (j==1)
                { 
                 proba_initiation_every=m_params->getProprieteD("Everywhere_proba_initialisation_par_step");
                }
                else
                { 
                 string na_temp_nb_mic="Everywhere_proba_initialisation_par_step_"+std::to_string(j);
                 proba_initiation_every=m_params->getProprieteD(na_temp_nb_mic);
                }        
                if (proba_initiation_every>0){InitiateMidway(ep, proba_initiation_every, cont_num,1,-10);}       
                
                
                //double proba = (float)rand()/(float)RAND_MAX;
                //int p_proba_entier = proba_initiation;
                //double p_proba_decimal = proba_initiation - p_proba_entier;
                //cout << "test de la valeur entiÃ¨re/dÃ©cimale "<< p_proba_entier <<" "<<p_proba_decimal<<endl;
                //int nb_init=0;
                //for (int step=1;step<=p_proba_entier;step++)
                //{
                //   deb3("testInit...");
                //   vector<double> v = pointNucleateOnContour(ep->getContour(),cont_num-1);
                //   //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
                //   Id i = this->giveMicrotubule(ep, v,cont_num);
                //  Microtubule *m = this->getMicrotubule(i);
                //   creation_microtubule+=1;
                //   if (verbose==3){cout<<*m<<endl;}
                //   nb_init++;
               //}
               //if (proba < p_proba_decimal)
               //{   
               //   deb3("testInit decimal...");
               //   //vector<double> v = pointOnSphere(150, 3000,3000,3000)
               //   vector<double> v = pointNucleateOnContour(ep->getContour(),cont_num-1);
               //   //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
               //   
               //   Id i = this->giveMicrotubule(ep, v, cont_num);
               //   Microtubule *m = this->getMicrotubule(i);
               //   creation_microtubule+=1;
               //   if (verbose==3){cout<<*m<<endl;}
               //   nb_init++;
               // }
               //cout<< "Initiation on multiple surfaces end" <<endl;
            }
        }
    else
        {   
            


            int cout_num;
            if (m_obj_num==1){cout_num=1;} //normally here as one object not nucleating seperately
            else if (cen_unif>0){cout_num=m_obj_num+1;} //nucleating within highest number object (not well tested)
            else {cout_num=-1;}             //nucleate on all surfaces equally (not well tested)
            double proba_initiation = m_params->getProprieteD("proba_initialisation_par_step");
            InitiateMidway(ep, proba_initiation, cout_num,0,cen_unif);

            double proba_initiation_every=m_params->getProprieteD("Everywhere_proba_initialisation_par_step");
            if (proba_initiation_every>0){InitiateMidway(ep, proba_initiation_every, cout_num,1,cen_unif);}

            // //int cont_num=-1;
            // double proba = (float)rand()/(float)RAND_MAX;
            // int p_proba_entier = proba_initiation;
            // double p_proba_decimal = proba_initiation - p_proba_entier;
            // //cout << "test de la valeur entiÃ¨re/dÃ©cimale "<< p_proba_entier <<" "<<p_proba_decimal<<endl;
            // int nb_init=0;
            // for (int step=1;step<=p_proba_entier;step++)
            // {
            //     deb3("testInit...");
            //    //vector<double> v = pointOnSphere(150, 3000,3000,3000);
            //     vector<double> v;
            //     if (cen_unif>0){ string st_volume=m_params->getProprieteS("Nucleation_Volume_Shape");
            //         if (st_volume=="sphere"){v=GenerateRandomMTDirecSphere(cen_unif);}
            //         else {v=GenerateRandomMTDirec(cen_unif);}}
            //     else{ v = pointNucleateOnContour(ep->getContour(),0);}
            //     //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
            //     Id i = this->giveMicrotubule(ep, v, cout_num);
            //     Microtubule *m = this->getMicrotubule(i);
            //     creation_microtubule+=1;
            //     if (verbose==3){cout<<*m<<endl;}
            //     nb_init++;
            // }
            // if (proba < p_proba_decimal)
            // {   
            //     deb3("testInit decimal...");
            //     //vector<double> v = pointOnSphere(150, 3000,3000,3000);
            //     vector<double> v;
            //     if (cen_unif>0){ string st_volume=m_params->getProprieteS("Nucleation_Volume_Shape");
            //         if (st_volume=="sphere"){v=GenerateRandomMTDirecSphere(cen_unif);}
            //         else {v=GenerateRandomMTDirec(cen_unif);}}
            //     else{v = pointNucleateOnContour(ep->getContour(),0);}
            //     //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
            //     Id i = this->giveMicrotubule(ep, v, cout_num);
            //     Microtubule *m = this->getMicrotubule(i);
            //     creation_microtubule+=1;
            //     if (verbose==3){cout<<*m<<endl;}
            //     nb_init++;
            // }

        }
        //cout<<"nb d'initiations "<<nb_init<<endl;
        
        deb2("           evolve microtubulepool 2");
//         int a=0;int b=0;int c=0;int d=0;double tot=0;int lg=0;
        lp = getListMicrotubules();
        deb2("           evolve microtubulepool 2b");
        for (list<Id>::iterator it=lp.begin();it!=lp.end();it++)
        {   
                //deb3("testMicr...");
                Microtubule *m=getMicrotubule(*it);
                deb2("                   evolve microtubulepool 3a");
                if (verbose>=1){cout << "                       in evolve microtubule, microtubule : " << *m <<endl;}
                m->evolve();
                if (verbose>=1){cout << "                       evolve returned in the pool loop : " << *m <<endl;}
                deb2("                   evolve microtubulepool 3b");
  
        }
        deb("           evolve microtubulepool 4");


    }
 
    //Calculate position and direction to start off an MT nucleating in a central volume of the space.
    //cen_unif input is half the number of units to nucleate within, so box is 2*cen_unif on each size, cen_unif distance from the central point in each x,y,z direction
    std::vector<double> MicrotubulePool::GenerateRandomMTDirec( double cen_unif)
    {
    vector<double> v;
    double Xpos=m_params->getProprieteD("CentreX")-cen_unif+((float)rand()/(float)RAND_MAX)*2*cen_unif;
    v.push_back(Xpos);
    double Ypos=m_params->getProprieteD("CentreY")-cen_unif+((float)rand()/(float)RAND_MAX)*2*cen_unif;
    v.push_back(Ypos);
    double Zpos=m_params->getProprieteD("CentreZ")-cen_unif+((float)rand()/(float)RAND_MAX)*2*cen_unif;
    v.push_back(Zpos);
    double Xdirec=(((float)rand()/(float)RAND_MAX)-0.5)*2;
    double Ydirec=(((float)rand()/(float)RAND_MAX)-0.5)*2;
    double Zdirec=(((float)rand()/(float)RAND_MAX)-0.5)*2;
    double Norm=sqrt(pow(Xdirec,2)+pow(Ydirec,2)+pow(Zdirec,2));
    Xdirec=Xdirec/Norm;
    Ydirec=Ydirec/Norm;
    Zdirec=Zdirec/Norm;
    v.push_back(Xdirec);
    v.push_back(Ydirec);
    v.push_back(Zdirec);
    return v;}

    std::vector<double> MicrotubulePool::GenerateRandomMTDirecSphere( double cen_unif)
    {
    vector<double> v;
    double Theta=((float)rand()/(float)RAND_MAX)*2*M_PI;
    double Phi=acos(2*(float)rand()/(float)RAND_MAX-1);
    double R=cen_unif*cbrt((float)rand()/(float)RAND_MAX);
    v.push_back(m_params->getProprieteD("CentreX")+R*cos(Theta)*sin(Phi));
    v.push_back(m_params->getProprieteD("CentreY")+R*sin(Theta)*sin(Phi));
    v.push_back(m_params->getProprieteD("CentreZ")+R*cos(Phi));
    double Xdirec=(((float)rand()/(float)RAND_MAX)-0.5)*2;
    double Ydirec=(((float)rand()/(float)RAND_MAX)-0.5)*2;
    double Zdirec=(((float)rand()/(float)RAND_MAX)-0.5)*2;
    double Norm=sqrt(pow(Xdirec,2)+pow(Ydirec,2)+pow(Zdirec,2));
    Xdirec=Xdirec/Norm;
    Ydirec=Ydirec/Norm;
    Zdirec=Zdirec/Norm;
    v.push_back(Xdirec);
    v.push_back(Ydirec);
    v.push_back(Zdirec);
    return v;}
    
    //nucleaiton of microtubules after the initial step for 1 contour, multiple contours, spatial nulceation, and nucleating on entire surface as well as spatial 
    void MicrotubulePool::InitiateMidway(ElementPool *ep, double proba_initiation, int cont_num, int type_nuc,int cen_unif)
    {
         vector<double> v;
         double proba = (float)rand()/(float)RAND_MAX;
         int p_proba_entier = proba_initiation;
         double p_proba_decimal = proba_initiation - p_proba_entier;
         //cout << "test de la valeur entiÃ¨re/dÃ©cimale "<< p_proba_entier <<" "<<p_proba_decimal<<endl;
         int nb_init=0;

         //cout<<proba_initiation<<endl;
         //cout<<cen_unif<<endl;
         //cout<<cont_num<<endl;
         //cout<<type_nuc<<endl;

         for (int step=1;step<=p_proba_entier;step++)
         {
             deb3("testInit...");
             //vector<double> v = pointOnSphere(150, 3000,3000,3000);
             //choose point to nucleate on whole contour if type_nuc=1 or only on nucleation zoon if type_nuc==0. cont_num decides if one or multiple contours to nucleate on 
             v=ChooseNewMT(ep,cont_num,type_nuc,cen_unif);
             //cout << v[0]<<"/" <<v[1]<<"/" <<v[2]<<"/" <<v[3]<<"/" <<v[4]<<"/" <<v[5] << endl;
             Id i = this->giveMicrotubule(ep, v,cont_num);
             Microtubule *m = this->getMicrotubule(i);
             creation_microtubule+=1;
             if (verbose==3){cout<<*m<<endl;}
             nb_init++;
       }

       if (proba < p_proba_decimal)
            {   
            deb3("testInit decimal...");
            //vector<double> v = pointOnSphere(150, 3000,3000,3000)
            v=ChooseNewMT(ep,cont_num,type_nuc,cen_unif);
            Id i = this->giveMicrotubule(ep, v, cont_num);
            Microtubule *m = this->getMicrotubule(i);
            creation_microtubule+=1;
            if (verbose==3){cout<<*m<<endl;}
            nb_init++;
        }

    }

   //chooses a vector v from which to grow the new MT depending on the type of nulceation
    std::vector<double> MicrotubulePool::ChooseNewMT(ElementPool *ep,  int cont_num, int type_nuc,int cen_unif){
          std::vector<double> v;
          if (cen_unif>0){ string st_volume=m_params->getProprieteS("Nucleation_Volume_Shape");
                 if (st_volume=="sphere"){v=GenerateRandomMTDirecSphere(cen_unif);}
                 else {v=GenerateRandomMTDirec(cen_unif);}}
          else {
                if (cont_num<0)
                {
                     if (type_nuc==1){v = pointOnContour(ep->getContour());}
                     else {v = pointNucleateOnContour(ep->getContour());}
                }
                else
                {
                    if (type_nuc==1){v = pointOnContour(ep->getContour(),cont_num-1);}
                   else{v = pointNucleateOnContour(ep->getContour(),cont_num-1);}
                }
         }
    return v;
    }

//Finds MTs in given hash neighbourhoods which are outside the membrane and cuts them. 
    void MicrotubulePool::UpdateMTWithin(std::vector<long> AllHashMT){
    std::vector<Element *>  PosElem= m_ep->UpdateElementPool_Outside(AllHashMT); //find elements outside the membrane in these hash regions
    UpdateMTWithin(PosElem);
    }
     
    void MicrotubulePool::UpdateMTWithin(std::vector<Element *> PosElem){
    for (std::vector<Element *>::iterator it = PosElem.begin(); it!=PosElem.end(); ++it)
        {if ((*it)->isAlive()==1){
   	        //cout<<"Killing an element"<<endl;
             Structure *s=(*it)->getStructure(); 
       	     s->KillElement(*it);
             }
        }
     }

//Finds MTs in given hash neighbourhoods and move them when whole contour moving as a solid body motion. 
    void MicrotubulePool::UpdateMoveMT(std::vector<long> AllHashMT, std::vector<double> PosMove){ 
    //Find all elements outside the membrane 
    std::vector<Element *>  PosElem= m_ep->UpdateElementPool_Outside(AllHashMT); //find elements outside the membrane in these hash 
    //Find MT numbers which correspond to these elements
    std::unordered_set<Id> Mt_ids; //List of all MTs with element which has moved outside the boundary
    //cout<<"List of Mts"<<endl;
    //cout<<"MaxMT_ID: "<< maxId()<<endl;
    //cout<<"Length PosElem"<<PosElem.size()<<endl;
    //if (PosElem.size()>0){cout<<"First_ID: "<<PosElem[0]->getStructureId()<<endl;}
    for (std::vector<Element *>::iterator it = PosElem.begin(); it!=PosElem.end(); ++it)
        {Id Temp_MT_ID=(*it)->getStructureId(); //ID of microtubules with an element close to the surface
         //cout<<"Id of MT: " << Temp_MT_ID << endl;
         //cout<<"Is alive: "<<(*it)->isAlive()<<endl;
         if ((*it)->isAlive()) {Mt_ids.insert(Temp_MT_ID);}
        }
   //Get Elements in Microtubule then move or kill it
   for (const Id& x: Mt_ids)
   { std::deque<Element *> MT_Elements;
   MT_Elements=getMicrotubule(x)->getBody();  
   m_ep->UpdateElementPool_Move(MT_Elements, PosMove); }  
   //Kill any elements which are still outside the contour
   UpdateMTWithin(AllHashMT);
   }  

//Move MTs close to boundayr of contour when they are not all moving the same amount 
    void MicrotubulePool::UpdateMoveMT(Contour c){ 
   //  //Find all elements outside the membrane 
    std::list<Id> Mt_ids=getListMicrotubules();
   //  //std::vector<Element *>  PosElem= m_ep->UpdateElementPool_Outside(AllHashMT); //find elements outside the membrane in these hash 
   //  //Find MT numbers which correspond to these elements
   //  //std::unordered_set<Id> Mt_ids; //List of all MTs with element which has moved outside the boundary
   //  //if (PosElem.size()>0){cout<<"First_ID: "<<PosElem[0]->getStructureId()<<endl;}
   //  for (std::vector<Element *>::iterator it = PosElem.begin(); it!=PosElem.end(); ++it)
   //      {Id Temp_MT_ID=(*it)->getStructureId(); //ID of microtubules with an element close to the surface
   //       //cout<<"Id of MT: " << Temp_MT_ID << endl;
   //       //cout<<"Is alive: "<<(*it)->isAlive()<<endl;
   //       if ((*it)->isAlive()) {Mt_ids.insert(Temp_MT_ID);}
   //      }
   cout<<"Number of Microtubules:"<<Mt_ids.size()<<endl;
   //Get Elements in Microtubule then move or kill it
   for (const Id& x: Mt_ids)
   { std::deque<Element *> MT_Elements;
   MT_Elements=getMicrotubule(x)->getBody();  
   cout<<"Number of elements:"<<MT_Elements.size()<<endl;
   std::vector<double> PosMove=m_ep->UpdateElementPool_MTMovement(MT_Elements,c);
   cout<<"Pos Move all: "<<PosMove[0]<<" "<<PosMove[1]<<" "<<PosMove[2]<<endl;
   std::vector<Element *> DelElem;
   DelElem=m_ep->UpdateElementPool_MoveDefinitely(MT_Elements, PosMove,c); 
   cout<<"number of elements to delete:"<<DelElem.size()<<endl;
   UpdateMTWithin(DelElem);  
   //Kill any elements which are still outside the contour
   //UpdateMTWithin(AllHashMT);
        }
   }

    void MicrotubulePool::UpdateParameters(Parametres *params)
    {
     m_proba_detachement = params->getProprieteD("proba_detachement_par_step_par_microtubule");
     m_proba_shrink = params->getProprieteD("proba_shrink");
     m_proba_crossmicro_cut = params->getProprieteD("proba_crossmicro_cut");
     m_proba_tocut = params->getProprieteD("proba_tocut");
     m_params = params;
    }
     
