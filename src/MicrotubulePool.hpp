//classe MicrotubulePool
//non si element.h deja importe ailleurs
#ifndef __MICROTUBULEPOOL_H_INCLUDED__
#define __MICROTUBULEPOOL_H_INCLUDED__
#include <iostream>
#include "Microtubule.hpp"
#include "Contour.hpp"
#include <unordered_map>
#include <deque>
#include <list>
#include <vector>
#include <utility>

typedef int Id;
class Parametres;


class MicrotubulePool
{
    friend std::ostream& operator<<(std::ostream& os, MicrotubulePool& p);
    friend std::ofstream& operator<<(std::ofstream& os, MicrotubulePool& p);
    public:
        MicrotubulePool();
        MicrotubulePool(Parametres *params, int nb_gamma_tub, ElementPool *ep);
        //MicrotubulePool(Parametres *params, int nb_gamma_tub, double proba_detachement, double proba_initiation, ElementPool *ep);
        MicrotubulePool(Parametres *params, ElementPool *ep);
        ~MicrotubulePool();
        void initiate();
        Id giveMicrotubule(ElementPool *ep);        
        Id giveMicrotubule(ElementPool *ep, std::deque<Element *> body);
        Id giveMicrotubule(ElementPool *ep, std::vector<double> vec, int cont_num);
        Microtubule* getMicrotubule(Id id);
        int getInfluence();
        void erase(Id id);
        std::list<Id> getListMicrotubules();
        int write(std::string filename);
        std::vector<int> getMicrotubuleLengths();

        void evolve(ElementPool *ep);
        
        //a ne pas utiliser
        std::unordered_map<Id, Microtubule> *getMicrotubules();
        
        int destruction_microtubule;
        int creation_microtubule;
        int cut_microtubule;
	      int override_cut_microtubule;    
        int detachements;
	      int shrinkages;
        void UpdateMTWithin(std::vector<long> AllHashMT);
        void UpdateMTWithin(std::vector<Element *> PosElem);
        void UpdateMoveMT(std::vector<long> AllHashMT, std::vector<double> PosMove);
        void UpdateMoveMT(Contour c);
        void UpdateParameters(Parametres *params); //Updates the relevent private member objects (when parameters are changed midway through simulation)
        
    private:
        void InitiateMidway(ElementPool *ep, double proba_initation, int cont_num,int type_nuc,int cen_unif); //initiate microtubules in different ways depending on initiation criteria
        std::vector<double> ChooseNewMT(ElementPool *ep, int cont_num, int type_nuc,int cen_unif); //chooses vector v to initiate a new microtubule depending on nucleation parameters
       std::vector<double> GenerateRandomMTDirec( double cen_unif); //Generate random position and direction in stated smaller cuboidal volume to nucleate
        std::vector<double> GenerateRandomMTDirecSphere( double cen_unif); //Generate random position and direction in stated smaller spherical volume to nucleate
        std::unordered_map<Id, Microtubule> m_microtubules;
        Id maxId();
        Id m_maxId;
        int m_nb_gamma_tub;
        int m_obj_num;
        double m_proba_detachement;
	double m_proba_shrink;
	double m_proba_crossmicro_cut;
        double m_proba_rescue;

        ElementPool *m_ep;

        //double m_proba_initiation;
        double m_proba_tocut;
        int m_influence;
        std::list< std::pair<Id,Microtubule*> > m_gamma_tub;
        Parametres *m_params;
               
};
#endif
