//classe Contour
//non si Contour.h deja importe ailleurs
#ifndef __CONTOUR_H_INCLUDED__
#define __CONTOUR_H_INCLUDED__
#include <iostream>
#include "vtkPolyDataReader.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkTransform.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"
#include "vtkPolyDataWriter.h"
#include <unordered_map>
#include <string>
#include "Element.hpp"
#include "Structure.hpp"
#include <deque>
#include <vector>

class ElementPool;
class Parametres;

class Contour:public Structure
{
	friend std::ostream& operator<<(std::ostream& os, Contour& m);
	public:
		Contour(Parametres *params, ElementPool *ep, Id r);
		void open(); //open new contour and look for bounds for has
                void open(int bound_ind); //open but don't change bounds
		virtual std::ostream& Print( std::ostream &os) const;
		std::vector<double> getElementRandom();
		std::vector<double> getElementRandom(int i); //get random element from contour i
		std::vector<double> getElementRandomNucleation(int i); //get random elment from all possible nucleation sites of contour i (may not be whole contour).
 		std::vector<double> getElementRandomNucleation(); //get random elment from all possible nucleation sites of contour i (may not be whole contour).
                std::vector<double> AdaptElement(Element *e, int ContNum);
                std::deque<Element *> GetContourElements(int i);
                std::deque<Element *> GetAllContourElements();
                std::deque<Element *> GetContourNucleationElements(int i); // get all points you can nucleate at on contour i .
		
	void UpdateSpaceContor(Element *e, long pos_old);
  long GetContourElementHash(Element *e);
  long GetMTNearElementHash(double pos1, double pos2,  double pos3);

                void UpdateUnconnected(); //deletes old contour and reads in another
   
	private:
		ElementPool *m_elementPool;
                Parametres *m_params;
		std::deque<Element *> m_body; //Holds all objects on a surface
		std::vector<std::deque<Element *>> m_surf; //Holds all objects on each surface seperately
		std::vector<std::deque<Element *>> m_nucleation; //Holds points on nucleation area for each object, one for each surface	


};
#endif

